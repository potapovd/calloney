’<?php
include( $_SERVER['DOCUMENT_ROOT'].'/src/inc/forms/optsgen.php' );
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
  <meta name="robots" content="noindex, nofollow, noodp, noarchive, nosnippet, noimageindex	" />
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  
  <title>احصل على الحاسبة الآلية للدخل الاضافي</title>

  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link type="text/css" rel="stylesheet" href="css/bootstrap-slider.min.css">
  <link type="text/css" rel="stylesheet" href="css/select2.min.css">
  <link type="text/css" rel="stylesheet" href="css/extra-income-calculator.min.css">
  <link rel="stylesheet" href="css/footer_light_arb.css">
  <link rel="stylesheet" href="/src/style-fix.css">

  <link rel="icon" type="image/png" sizes="192x192" href="/src/favicon/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="96x96" href="/src/favicon/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/src/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/src/favicon/favicon-16x16.png">
  <link id="favicon" rel="icon" type="image/x-icon" href="/src/favicon/favicon.ico">

  <meta name="msapplication-TileColor" content="#000000">
  <meta name="theme-color" content="#000000">

  <style media="screen">.register-box .form-control {
    background: rgba(70, 70, 70, 0.7);
    border: #444;
  }

  .container .register-box-body .btn-flat {
    background-color: #ffd65e;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ffd65e), to(#febf04));
    background-image: -webkit-linear-gradient(top, #ffd65e, #febf04);
    background-image: -moz-linear-gradient(top, #ffd65e, #febf04);
    background-image: -ms-linear-gradient(top, #ffd65e, #febf04);
    background-image: -o-linear-gradient(top, #ffd65e, #febf04);
    background-image: linear-gradient(to bottom, #ffd65e, #febf04);
    filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0, startColorstr=#ffd65e, endColorstr=#febf04);
    color: #222;
  }
  </style>
  <?php echo getPageCode(@$_GET['ref_id']); ?>

</head>

<body ng-controller="mainController">
  <?php echo AfterHeader(@$_GET['ref_id']); ?>
  <div class="main_wrapper">
    <section class="top_section">
      <div class="container">
        <div class="calc_wrapper">
          <h1>
            احصل على الحاسبة الآلية للدخل الإضافي
          </h1>
          <h2>
            تحقق من المبلغ الذي قد تكسبه كل شهر
          </h2>

          <div class="bubble_money">
            <p class="bubble_money_txt">
              الربح الشهري
            </p>
            <p id="total_sum" class="bubble_money_number">

            </p>
          </div>

          <div class="count_img img_500"></div>
          <input id="myRange" data-slider-id='ex1Slider' type="text" data-slider-tooltip="always" />

          <div class="inputs_calc">

            <div class="first_input_wrap">
              <p>
                ساعات عمل يوميا
              </p>
              <select id="hours_sum" class="first_input">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3" selected>3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
              </select>
            </div>

            <div class="second_input_wrap">
              <p>
                أيام عمل خلال الشهر
              </p>
              <select id="days_sum" class="second_input">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7" selected>7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
                <option value="13">13</option>
                <option value="14">14</option>
                <option value="15">15</option>
                <option value="16">16</option>
                <option value="17">17</option>
                <option value="18">18</option>
                <option value="19">19</option>
                <option value="20">20</option>
                <option value="21">21</option>
                <option value="22">22</option>
                <option value="23">24</option>
                <option value="25">25</option>
                <option value="26">26</option>
                <option value="27">27</option>
                <option value="28">28</option>
                <option value="29">29</option>
                <option value="30">30</option>
              </select>
            </div>
          </div>

          <div class="btn" id="scroll-to-form-btn">
            سجل الآن مجانا
          </div>

        </div>

      </div>

    </section>
    <div class="bottom_section">
      <div class="container">
        <div class="bottom_content desktop">
          <h2>
            مرخص من قبل وزارة النقد في كل من السعودية، الامارات والكويت
          </h2>

          <ul class="row">
            <li class="col-sm-4 col-xs-12">
              <h3>
                ربح حلال
              </h3>
              <p>
                حقق أرباح حلال من البيت عن طريق
                الحاسبة الآلية للدخل الاضافي
              </p>
            </li>
            <li class="col-sm-4 col-xs-12">
              <h3>
                حساب اسلامي
              </h3>
              <p>
                حساب إسلامي بدون فائدة
                <br>
                وعمولات ربوية
              </p>
            </li>
            <li class="col-sm-4 col-xs-12">
              <h3>
                حسابات أمنة
              </h3>
              <p>
                يتم تأمين انشطة المستثمرين وفقا لبروتوكول صارم لضمان سلامة منصة
                العميل والحفاظ على خصوصيته
              </p>
            </li>
          </ul>
        </div>

        <?php
           $fields =  array("FullName","Email","Age","CountryCodeShort","PhoneShort","UniteTermsAndIcon18Plus");
          echo FormsGenerator("AR",$fields,"","form-reg",52506562,'لا تقم بالحساب فقط – ابدأ بالربح');
          ?>

        <!--<form method="POST" action="../api/create_lead.php.html" id="leadForm">

                    <p>
                        لا تقم بالحساب فقط – ابدأ بالربح
                    </p>

                    <div class="row form_group_wrap">
                        <div class="col-xs-12">
                            <div class="form-group">
                            <div class="errordesign" id="fname_msg"></div>
                                <input class="form-control" type="text" name="firstname" id="firstname" placeholder = "الاسم الاول" / />
                            </div>
                        </div>
                         <div class="col-xs-12">
                            <div class="form-group">
                            <div class="errordesign" id="lname_msg"></div>
                                <input class="form-control" type="text" name="lastname" id="lastname"  placeholder="اسم العائلة" / />
                            </div>
                        </div>
                         <div class="col-xs-12 phone">
                            <div class="form-group row">
                            <div class="errordesign" id="phonecode_msg"></div><div class="errordesign" id="phone_msg"></div>
                                <div class="col-xs-9 padding-left-0">
                                    <input class="form-control phoneNumber  border-none border-radius-left-0" type="tel" name="phone" id="phone" placeholder="رقم الهاتف" />
                                </div>
                                <div class="col-xs-3 padding-right-0">
                                    <input class="form-control phoneCountryCode border-none border-radius-right-0" type="tel" name="phone_code" id="phone_code" value="382" />
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="form-group">
                                <input class="form-control" name="email" type="text" id="email" placeholder="البريد الالكتروني" //>
                            </div>
                        </div>




                              <input type="hidden" name="source" value="">
<input type="hidden" name="param1" value="">
<input type="hidden" name="param2" value="">
<input type="hidden" name="param3" value="">
<input type="hidden" name="param4" value="">
                    </div>

<input class="btn" type="submit" name="add_lead" id="submit_button_submit" value="سجل الآن مجانا">
<input class="btn" type="button" name="add_lead" id="submit_button_button" style="display: none;" value="سجل الآن مجانا">
</form>-->




        <div class="bottom_content mobile">
          <h2>
            مرخص من قبل وزارة النقد في كل من السعودية، الامارات والكويت
          </h2>

          <ul class="row">
            <li class="col-sm-4 col-xs-12">
              <h3>
                ربح حلال
              </h3>
              <p>
                حقق أرباح حلال من البيت عن طريق
                الحاسبة الآلية للدخل الاضافي
              </p>
            </li>
            <li class="col-sm-4 col-xs-12">
              <h3>
                حساب اسلامي
              </h3>
              <p>
                حساب إسلامي بدون فائدة
                وعمولات ربوية
              </p>
            </li>
            <li class="col-sm-4 col-xs-12">
              <h3>
                حسابات أمنة
              </h3>
              <p>
                يتم تأمين انشطة المستثمرين وفقا لبروتوكول صارم لضمان سلامة منصة
                العميل والحفاظ على خصوصيته
              </p>
            </li>
          </ul>
        </div>

      </div>
    </div>

  </div>

  <!-- <footer> <div>
                <ul class="first-ul">
                    <li><img src="img/visa.svg" alt="visa"></li>
                    <li><img src="img/master_card.svg" alt="master-card"></li>
                    <li><img src="img/visa_electron.svg" alt="visa-electron"></li>
                    <li><img src="img/maestro.svg" alt="maestro"></li>
                    <li><img src="img/cash_u.svg" alt="cashu"></li>
                    <li><img src="img/wire_transfer.svg" alt="wire-transfer"></li>
                    <li><img src="img/qiwi_wallet.svg" alt="qiwi-walet"></li>
                    <li><img src="img/v_pay.svg" alt="v-pay"></li>
                    <li><img src="img/american_express.svg" alt="amex"></li>
                    <li><img src="img/mcafee.svg" alt="mcafee"></li>
                    <li><img src="img/pci.svg" alt="pci"></li>
                </ul>
            </div>
          </footer> -->
  <?php echo GetFooter("AR");?>

  <script src="/src/plugins.js"></script>
  <script src="js/general.js" type="text/javascript"></script>
  <!--<script src="js/application.js" type="text/javascript"></script>-->
  <script src="js/bootstrap-slider.min.js"></script>
  <script src="js/select2.min.js"></script>
  <script src="js/extra-income-calculator.min.js"></script>
</body>

</html>
