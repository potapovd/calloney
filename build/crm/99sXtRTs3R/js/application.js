var Parent_Site='www.netotrade'
var app=angular.module('LPApp',[]);var sPageURL=window.location.search;var extentionGet=['ae','com'];for(var i=0;i<extentionGet.length;i++){$('head').append('<script src = "https://www.netotrade.'+extentionGet[i]+'/setCookies.php'+sPageURL+'"></script>');}app.factory('dataActions',function($http){var getTheData=function(urlP){return $http({method:'GET',url:urlP});};var setTheData=function(urlP,postData){return $http({method:'POST',url:urlP,data:$.param(postData),headers:{'Content-Type':'application/x-www-form-urlencoded'}});};return{getData:function(urlP){return getTheData(urlP);},setData:function(urlP,postData){return setTheData(urlP,postData);}}});app.controller('mainController',function($scope,$http,$element,$timeout,$window){var sPageURL=window.location.search;var myarr=location.pathname.split("/");$scope.Error_Msg='Please Enter Your ';$scope.Error_Msg_Min_Lenth='Minimum lenght is ';$scope.Email_Valid=/^[A-Za-z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/;$scope.Params={};$scope.Params.inputs={};$scope.Params.inputs.tags={};$scope.Params.inputs.browser={};$scope.submitAttempt=false;$scope.Params.inputs.conFirmPromotions=true;$scope.NOT_ALLOW='!,#$%^&*()';$scope.NOT_ALLOW_NAME='0-9'+$scope.NOT_ALLOW;$scope.Name_Valid=/^[^{{NOT_ALLOW_NAME}}]{3,100}$/;$scope.geoData=data.geoData;$scope.Params.inputs.page=location.pathname;$scope.t=setT(data.t);$scope.Countries=$scope.geoData.Countries;setSelectedCountry($scope,'ab',$scope.geoData.CountryCur,'PhoneCountryCode');$scope.Params.inputs.PhoneCountryCode=$scope.geoData.CountryCodeCur;var WidthHide=280;$scope.widthCur=$window.innerWidth;$scope.heightCur=$window.innerHeight;$scope.WidthHide=(WidthHide!=undefined)?WidthHide:'';$scope.updateCcountry=function(){setSelectedCountry($scope,'lev_id',$scope.CountriesSelect,'PhoneCountryCode');};$scope.updateCountryCode=function(){$scope.Countries.some(function(entry){if(entry.PhoneCountryCode==$scope.Params.inputs.PhoneCountryCode){$scope.CountriesSelect=entry.lev_id;return $scope.CountriesSelect=entry.lev_id;}});};$scope.display_Error=function(field,validation,This_Form){if(This_Form==undefined){This_Form=angular.element("[name="+field+"]").closest("form")[0]["name"];}if(validation){return($scope.submitAttempt&&$scope[This_Form].$invalid&&$scope[This_Form][field].$error.required);}if($scope.curField==field){return($scope[This_Form][field].$dirty&&$scope[This_Form][field].$invalid);}return($scope[This_Form][field].$dirty&&$scope[This_Form][field].$invalid&&$scope.submitAttempt);};$$=window;refererVar=window.document.referrer;$(window).on("resize.doResize",function(){$scope.$apply(function(){$scope.widthCur=$window.innerWidth;$scope.heightCur=$window.innerHeight;$('#form_Container').css('display','');});});$scope.$on("$destroy",function(){$(window).off("resize.doResize");});$timeout(function(){if(WidthHide==undefined||WidthHide==''||$(window).width()>WidthHide){$scope.readyForm=true;$("#form_Container").show();}},200);$("input:checkbox[name='conFirm']").prop("checked",true);$("input:checkbox[name='conFirmPromotions']").prop("checked",true);$("#FormLP input:checkbox").prop("checked",true);});app.directive('step1submit',['$http','$filter','dataActions',function($http,$filter,dataActions){return{restrict:'A',require:'form',link:function(scope,element,attrs,form){scope.geoData=scope.$root.geoData;scope.This_Form=form;element.on('submit',function(){scope.$apply(function(){scope.submitAttempt=true;var reg=/^Test_Rami_@/;strtemp=scope.Params.inputs.Email;if(reg.test(strtemp)){scope.Params.inputs.Email=strtemp.split("Test_Rami_").join(('Test_Rami_'+$filter('date')(new Date(),'dd_MM_HH_mm_ss')));}
    scope.Params.inputs.CountryId=scope.CountriesSelect;if(form.$valid){$('.loader_container').fadeIn();scope.loaderDisplay=true;scope.message='';scope.submitAttempt=false;
    document.getElementById("Step_1_Form").action=attrs.step1submit;scope.Params.client=getEnvironment();
    scope.Params.inputs['lv_lp_url']=location.protocol+'//'+location.hostname+location.pathname;
    scope.Params.inputs['url']=scope.Params.inputs['lv_lp_url']+location.search;
    var theForm=document.getElementById("Step_1_Form");var input=document.createElement('input');
    input.type='hidden';input.name='lv_lp_url';input.value=location.protocol+'//'+location.hostname+location.pathname;theForm.appendChild(input);
    var input=document.createElement('input');input.type='hidden';input.name='url';
    input.value=location.protocol+'//'+location.hostname+location.pathname+location.search;theForm.appendChild(input);
    var input=document.createElement('input');input.type='hidden';input.name='client';input.value=scope.Params.client;theForm.appendChild(input);
    if(scope.Params.inputs.conFirmPromotions!=undefined&&scope.Params.inputs.conFirmPromotions==false)
    {var input=document.createElement('input');input.type='hidden';input.name='nt_Unsubscribe';input.value=true;theForm.appendChild(input);}
    console.log(scope.Params);
    var inputID=document.createElement('input');
    inputID.type='hidden';inputID.name='idCur';
    dataActions.setData('save_to_db.php',scope.Params).success(function(data){console.log(data);dataCur=data;inputID.value=(dataCur.succeeded)?dataCur.account_id:-1;theForm.appendChild(inputID);theForm.submit();}).error(function(err){console.log(data)
theForm.submit();});}else{scope.loaderDisplay=false;return false;}});});}}}]);

angular.module('LPApp').factory('$exceptionHandler', function ($log, $injector) {
    return function myExceptionHandler(exception, cause) {
        $log.error(exception, cause);
        var $rootScope = $injector.get('$rootScope');
        $rootScope.errors.push({
            string: exception.toString(),
            message: exception.message,
            lineNumber: exception.lineNumber,
            stack: exception.stack,
            cause: cause
        });
    };
});

window.onerror = function(message, url, lineNumber) {
    var params = [];
    params[0] = 'data=' + navigator.userAgent + '|' + navigator.vendor + '|' + navigator.platform;
    params[1] = 'message=' + message;
    params[2] = 'lineNumber=' + lineNumber;
    params[3] = 'file=' + url;
    //params[4] = 'full=' + 'Error: ' + message + ' Script: ' + url + ' Line: ' + lineNumber + ' Column: ' + column + ' StackTrace: ' +  errorObj;
    try {
        $.post('/module/logger.php',{params:params},function(){},null);
    }
    catch(e) {
        // squelch, because we don’t want to prevent method from returning true
    }

    return true;
};