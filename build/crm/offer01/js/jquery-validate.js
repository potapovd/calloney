/*!
 * Survey Validate v1
 * www.santakrouz.com/portfolio
 *
 * Copyright (c) Said Asebbane
 */

$().ready(function () {
	"use strict";
	// validate signup form on keyup and submit
	$("#subscribe-form").validate({

		rules: {
			Firstname: "required",
			Email: {
			  required: true,
			  email: true
			},
			phone: {
				required: true,
				digits: true,
			},
			Age:"required",
			country: "required",
		},
		messages: {
			Firstname: "المرجو ادخال الإسم والنسب",
			Email: {
				required: "المرجو ادخال بريدك الإلكتروني",
				digits: "المرجو ادخال البريد الإلكتروني بشكل صحيح",
			},
			phone: {
				required: "المرجو ادخال رقم الجوال",
				digits: "المرجو ادخال أرقام فقط",
			},
			Age: "المرجو إدخال عمرك",
			country: "المرجو اختيار بلدك",


		}
	});



});
