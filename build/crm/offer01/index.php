<!DOCTYPE html>
<html >
<head>
  <meta charset="utf-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
  <meta name="robots" content="noindex, nofollow, noodp, noarchive, nosnippet, noimageindex"/>
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>هل سأمت من الخسارة بالتداول</title>
  <link rel="stylesheet" href="css/bootstrap.min.css" />
  <link rel="stylesheet" href="css/landing.css" />


  <link rel="icon" type="image/png" sizes="192x192" href="/src/favicon/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="96x96" href="/src/favicon/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/src/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/src/favicon/favicon-16x16.png">
  <link id="favicon" rel="icon" type="image/x-icon" href="/src/favicon/favicon.ico">
  <meta name="msapplication-TileColor" content="#0a182c">
  <meta name="theme-color" content="#0a182c">

  <style>
      @font-face {
      font-family: 'Droid Arabic Kufi';
      font-display: swap; /* Read next point */
      unicode-range:  U+0600-06FF, U+200C-200E, U+2010-2011, U+204F, U+2E41, U+FB50-FDFF, U+FE80-FEFC, U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
      src:  url(https://fonts.gstatic.com/ea/droidarabickufi/v6/DroidKufi-Regular.woff2) format('woff2'),
            url(https://fonts.gstatic.com/ea/droidarabickufi/v6/DroidKufi-Regular.woff) format('woff');
  }

  @font-face {
      font-family: 'Droid Arabic Kufi Bold';
      font-display: swap; /* Read next point */
      unicode-range:  U+0600-06FF, U+200C-200E, U+2010-2011, U+204F, U+2E41, U+FB50-FDFF, U+FE80-FEFC, U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
      src:  url(https://fonts.gstatic.com/ea/droidarabickufi/v6/DroidKufi-Bold.woff2) format('woff2'),
            url(https://fonts.gstatic.com/ea/droidarabickufi/v6/DroidKufi-Bold.woff) format('woff');
  }
  .advantages h2{
    padding:20px;
  }
  .btn-flat{
    background: #fea85d !important;
    font-size: 18px !important;
    color: #222 !important;
  }
  .form-hold{
    max-width: 640px !important;
  }
  .intro h1{
    text-align: center;
    font-family: 'Droid Arabic Kufi Bold';
    margin-bottom: 20px;
  }
  body{
    font-family: "Droid Arabic Kufi";
  }
  </style>
  <script>window.addEventListener("message", function (event) {if (event.data.hasOwnProperty("FrameHeight")) {const formIframe = document.getElementById("formIframe");formIframe.style.height = event.data.FrameHeight+"px";formIframe.style.opacity = 1;}});function setIframeHeight(ifrm) {var height = ifrm.contentWindow.postMessage("FrameHeight", "*");}</script>
</head>

<body class="rtl-page">
    <div id="wrapper">
        <main>
            <div class="intro">
                <div class="main-holder">
                    <h1>هل سأمت من الخسارة بتداول النفط والعملات؟ </h1>
                    <h2> طريقة حصرية ستتيح لك الفرصة لكسب ثلاثة
                        أضعاف من رأس مالك في الاستثمار والتداول
                        عن طريق الانترنت بدون خبرة سابقة</h2>
                    <br>
                    <div class="video-wrap"> <img src="images/8b36c305-80405171c.jpg" alt="" /> </div>
                    <br>
                    <br>
                    <div class="form-hold">
                         
                      <!-- <iframe src="https://staging.adsguide.info/form/formIframe?<?php echo(urldecode($_SERVER[`QUERY_STRING`]));?>" style="width: 100%;height: 100%;overflow: hidden; opacity: 0;" frameborder="0" scrolling="no" id="formIframe" onload="setIframeHeight(this)"></iframe> -->

                      <iframe 
                        src="http://crm2.local:8080/form/formIframe?<?php echo(urldecode($_SERVER['QUERY_STRING']));?>" 
                        style="width: 100%;height: 100%;overflow: hidden; opacity: 0;" 
                        frameborder="0" scrolling="no" id="formIframe" onload="setIframeHeight(this)"></iframe>

                    </div>
                </div>
            </div>
            <div class="advantages">
                <div class="main-holder">
                    <h2>لقد انتهى فريق المحللين البريطاني من تجربة استراتيجية اويل كيلر والتى اثبتت نجاحها على مدى سنه ونصف من المتابعه الدقيقه <br><br>
                        سوف نزود الاستراتيجيه مجانا لاول 20 مشترك لكي يشاركونا النجاح <br>
                        اسرع بالتسجيل قبل اغلاق باب التسجيل </h2>
                    <div class="coslgrid">
                        <div class="col-lg-offset-1 col-lg-10">
                            <div class="col-md-4">
                                <div class="item"> <i class="ico01">&nbsp;</i>
                                    <p>توصيات بنسبة نجاح ٩٠% من خبراء التداول مجانا</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="item"> <i class="ico02">&nbsp;</i>
                                    <p>مدير حساب لخدمتك</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="item"> <i class="ico03">&nbsp;</i>
                                    <p>حساب اسلامي من دون فوائد</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="marquee">
                    <script>
                    var mydate = new Date()
                    var year = mydate.getYear()
                    if (year < 1000)
                        year += 1900
                    var day = mydate.getDay()
                    var month = mydate.getMonth()
                    var daym = mydate.getDate()
                    if (daym < 10)
                        daym = "0" + daym

                    var dayarray = new Array("الأحد", "الإثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة", "السبت")
                    var montharray = new Array("يناير", "فبراير", "مارس", "أبريل", "مايو", "يونيو", "يوليو", "أغسطس", "سبتمبر", "أكتوبر", "تشرين الثاني", "ديسمبر")

                    document.write("<font>" + dayarray[day] + ", " + montharray[month] + " " + daym + ", " + year + "</font>")
                    </script>
                    <marquee width="auto" direction="right" behavior="scroll" onmouseover="this.setAttribute('scrollamount', 0, 0);" onmouseout="this.setAttribute('scrollamount', 4, 0);" scrollamount="4" style="width: auto;">
                        <span class="live-script"> ﻗﺎﻡ عبد الله ﻣﻦ جدة ﺑﺮﺑﺢ 4500 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ فيصل ﻣﻦ الرياض ﺑﺮﺑﺢ 10635 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ حسين ﻣﻦ نجران ﺑﺮﺑﺢ 20610 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ سيف ﻣﻦ الخبر ﺑﺮﺑﺢ 8040 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ حامد ﻣﻦ الجوف ﺑﺮﺑﺢ 14300 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ قاسم ﻣﻦ رفحاء ﺑﺮﺑﺢ 24201 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ علي ﻣﻦ بقعاء ﺑﺮﺑﺢ 17846 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ أشرف ﻣﻦ جيزان ﺑﺮﺑﺢ 12687 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ حسن ﻣﻦ تبوك ﺑﺮﺑﺢ 18723 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ صالح ﻣﻦ الدمام ﺑﺮﺑﺢ 23102 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ طلال ﻣﻦ العقير ﺑﺮﺑﺢ 16789 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ سعدون ﻣﻦ أبها ﺑﺮﺑﺢ 19003 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ سليم ﻣﻦ القطيف ﺑﺮﺑﺢ 20172 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ عبد الرحمن ﻣﻦ حفر الباطن ﺑﺮﺑﺢ 8104 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ فهد ﻣﻦ الأفلاج ﺑﺮﺑﺢ 9269 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ خليل ﻣﻦ بارق ﺑﺮﺑﺢ 16011 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ راشد ﻣﻦ الباحة ﺑﺮﺑﺢ 13538 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ وجيه ﻣﻦ حائل ﺑﺮﺑﺢ 15791 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ ماجد ﻣﻦ الدرعية ﺑﺮﺑﺢ 10679 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ رشدي ﻣﻦ بريدة ﺑﺮﺑﺢ 20611 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ أسامة ﻣﻦ سكاكا ﺑﺮﺑﺢ 10040 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ عبد المجيد ﻣﻦ القيصومة ﺑﺮﺑﺢ 14100 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ عاصم ﻣﻦ القريات ﺑﺮﺑﺢ 24201 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ قاسم ﻣﻦ رأس تنورة ﺑﺮﺑﺢ 21302 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ ناصر ﻣﻦ عنيزة ﺑﺮﺑﺢ 17846 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ عدنان ﻣﻦ الصويدرة ﺑﺮﺑﺢ 12687 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ حسن ﻣﻦ الأفلاج ﺑﺮﺑﺢ 18723 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ غازي ﻣﻦ القريات ﺑﺮﺑﺢ 23102 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ حمد ﻣﻦ الجبيل ﺑﺮﺑﺢ 16789 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ حمزة ﻣﻦ ميسان ﺑﺮﺑﺢ 19003 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ جمعان ﻣﻦ خميس مشيط ﺑﺮﺑﺢ 20172 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ جمال ﻣﻦ الظهران ﺑﺮﺑﺢ 8104 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ حسام ﻣﻦ الجوف ﺑﺮﺑﺢ 9269 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ عمر ﻣﻦ القصيم ﺑﺮﺑﺢ 16011 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ عثمان ﻣﻦ بقعاء ﺑﺮﺑﺢ 10040 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ عبد الرحمن ﻣﻦ الحوية ﺑﺮﺑﺢ 9507 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ بدر ﻣﻦ الإحساء ﺑﺮﺑﺢ 10635 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | ﻗﺎﻡ بكر ﻣﻦ النعيرية ﺑﺮﺑﺢ 20610 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية | | ﻗﺎﻡ راضي ﻣﻦ بحرة ﺑﺮﺑﺢ 10431 ﺭﻳﺎﻝ ﻋﻦ ﻃﺮﻳﻖ النخبة السعودية </span>
                    </marquee>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
      <?php //echo GetFooter("AR"); ?>
    </div>
    <!--[if lt IE 9]>
      <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <script src="//css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
    <!-- <script src="/src/plugins.js"></script> -->
</body>

</html>