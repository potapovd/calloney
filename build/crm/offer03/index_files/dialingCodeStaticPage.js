﻿var base = 'https://www.bdifx.com';
$(document).ready(function () {
    getCountryByIp();
});


function getCountryByIp() {
    $.ajax({
        type: "POST",
        url: base + '/Tools/GetCountryByIp',
        dataType: "json",
        async: false,
        success: function (response) {
            if (response != '') {
                $('#Country').val(response);
                getDialingCode();
            }
        }
    });
}

function getDialingCode() {
    $('#PhoneCountryCode').val('');
    $.ajax({
        type: "POST",
        url: base + '/Tools/GetDialingCode',
        dataType: "json",
        data: '&country=' + $('#Country').val(),
        async: false,
        success: function (response) {
            if ($('#Country').val() != '' && response != '' && response != null) {
                $('#PhoneCountryCode').val(response);
            }
        }
    });
}

