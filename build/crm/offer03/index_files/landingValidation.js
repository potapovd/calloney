﻿var isFirstClick = true;
function submitLandingActionForm() {
    if (!isLandingValid()) {
        $("#Error").show();
    }
    else if (isFirstClick) {
        isFirstClick = false;
        $('#actionForm').submit();
    }
}

$(document).ready(function () {
    $("#PhoneNumber");
    $('[type=text],[type=tel], select').change(function () {
        $(this).removeClass('input-validation-error');
        $(this).parent().removeClass('input-validation-error');
        $("#Error").hide();
    });
});

function isLandingValid() {
    var isValid = true;
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

    if ($('#FirstName').val() == '') {
        $('#FirstName').addClass('input-validation-error');
        isValid = false;
    }
    if ($('#LastName').val() == '') {
        $('#LastName').addClass('input-validation-error');
        isValid = false;
    }
    if ($('#Email').val() == '') {
        $('#Email').addClass('input-validation-error');
        isValid = false;
    } else {
        if (!emailReg.test($('#Email').val())) {
            $('#Email').addClass('input-validation-error');
            $("#ErrorEmail").show();
            isValid = false;
        }
    }
    if ($('#PhoneNumber').val() == '') {
        $('#PhoneNumber').addClass('input-validation-error');
        isValid = false;
    }
    if ($('#Country').val() == '') {
        $('#Country').parent().addClass('input-validation-error');
        isValid = false;
    }

    return isValid;
}

var numbersArray = "0123456789٠١٢٣٤٥٦٧٨٩";

function isNumberKey(evt) {
    return isNumber(getEventValue(evt));
}
function isNoNumber(evt) {
    return isNotNumber(getEventValue(evt));
}

function isNumber(char) {
    return $.inArray(char, numbersArray) !== -1;
}

function isNotNumber(char) {
    return $.inArray(char, numbersArray) === -1;
}

function getEventValue(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    return String.fromCharCode(charCode);
}

function validateNumericField(fieldId) {
    var value = $('#' + fieldId).val();
    var filteredValue = "";
    for (i = 0; i < value.length; i++) {
        if (isNumber(value[i])) {
            filteredValue = filteredValue + value[i];
        }
    }
    $('#' + fieldId).val(filteredValue);
}

function validateNonNumericField(fieldId) {
    var value = $('#' + fieldId).val();
    var filteredValue = "";
    for (i = 0; i < value.length; i++) {
        if (isNotNumber(value[i])) {
            filteredValue = filteredValue + value[i];
        }
    }
    $('#' + fieldId).val(filteredValue);
}