$(function() {
    "use strict";

    // Counter Up
    // jQuery(document).ready(function($) {
    //     $('.counterup').counterUp({
    //         delay: 10,
    //         time: 1000
    //     });
    // });

    //Smooth Scroll
    $(document).ready(function(){
      $("a").on('click', function(event) {
        if (this.hash !== "") {
          event.preventDefault();
          var hash = this.hash;

          $('html, body').animate({
            scrollTop: $(hash).offset().top - 80
          }, 800);
          return false;
        }
      });
    });


    // On Scroll bg color Script
    $(document).ready(function(){
      $(window).scroll(function(){
        var scroll = $(window).scrollTop();
          if (scroll > 50) {
            $(".navbar").addClass("nav-bg");
          }

          else{
              $(".navbar").removeClass("nav-bg");  	
          }
      })
    })


    // Navbar collapse on click
    $('.navbar-nav>li>a').on('click', function(){
        $('.navbar-collapse').collapse('hide');
    });


    // Testimonial Slider
    $('.owl2').owlCarousel({
        loop: true,
        autoplay: true,
        navs: true,
        items: 1
    });

    
});