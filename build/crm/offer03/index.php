<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
    <meta name="robots" content="noindex, nofollow, noodp, noarchive, nosnippet, noimageindex"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Queueads</title>

    <!-- Bootstrap CSS File -->
    <link href="./index_files/bootstrap.min.css" rel="stylesheet">
    <!-- Line Icon CSS File	-->
    <link href="./index_files/line-icon.css" rel="stylesheet">
    <!-- Font Awesome Icon CSS File -->
    <link href="./index_files/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="./index_files/owl.carousel.min.css">
    <link rel="stylesheet" href="./index_files/owl.theme.default.min.css">
    <link href="./index_files/font-awesome.min.css" rel="stylesheet">
    <!-- Custom CSS file -->
    <link href="./index_files/style.css" rel="stylesheet">

	<link rel="icon" type="image/png" sizes="192x192" href="/src/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/src/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/src/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/src/favicon/favicon-16x16.png">
    <link id="favicon" rel="icon" type="image/x-icon" href="/src/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#052B45">
    <meta name="theme-color" content="#052B45">
    <style>
        .btn-flat{
            background: #e81940 !important;
        }
    </style>
    <script>window.addEventListener("message", function (event) {if (event.data.hasOwnProperty("FrameHeight")) {const formIframe = document.getElementById("formIframe");formIframe.style.height = event.data.FrameHeight+"px";formIframe.style.opacity = 1;}});function setIframeHeight(ifrm) {var height = ifrm.contentWindow.postMessage("FrameHeight", "*");}</script>


</head>

<body cz-shortcut-listen="true">
    <section class="banner fix-background parallax" id="home">
        <div class="opacity-dark bg-black"></div>
        <div style="direction:rtl;" class="container position-relative">
            <div class="row align-items-center">
                <div class="col-lg-8 col-md-7">
                    <!-- <img src="./index_files/logo.png" alt="logo" class="logo"> -->
                    <div class="banner-text">
                        <center><img alt="" src="./index_files/ramadan.png"></center>
                        <h1 style="text-align:center;">رمضان كريم وانت بتستاهل</h1>
                        <h2 class="h2title" style="direction:rtl;color:#fff;">
                            ميزات فتح حساب استثماري إسلامي :
                        </h2>
                        <ul>
                            <li>دعم مالي حتى <strong>5000 دولار</strong></li>
                            <li>سحوبات اكسبرس <strong>(بطاقة السحب السريع)</strong></li>
                            <li>سحوبات وجوائز يوميه بقيمة<strong> 100 الف دولار</strong> طوال شهر رمضان الكريم</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-5">
                    <div class="signup-box">
                        <div class="signup-box-headling">
                            <h2>افتح حسابك اليوم </h2>
                            <h3>واستفيد من العروض الرمضانيه </h3>
                            <span class="arrow-down"></span>
                        </div>
                        <div class="signup-box-body">
                             
                             <!-- <iframe src="https://staging.adsguide.info/form/formIframe?<?php echo(urldecode($_SERVER[`QUERY_STRING`]));?>" style="width: 100%;height: 100%;overflow: hidden; opacity: 0;" frameborder="0" scrolling="no" id="formIframe" onload="setIframeHeight(this)"></iframe> -->
                             <iframe 
                            src="http://crm2.local:8080/form/formIframe?<?php echo(urldecode($_SERVER['QUERY_STRING']));?>" 
                            style="width: 100%;height: 100%;overflow: hidden; opacity: 0;" 
                            frameborder="0" scrolling="no" id="formIframe" onload="setIframeHeight(this)"></iframe>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- =====================
        Section 3 - Benefit
    ======================= -->
    <section class="bg-grey" id="benefits">
        <div class="container">
            <div class="row">
                <div class="col-md-4 benefit mb-4">
                    <div class="media">
                        <div class="media-body text-center">
                            <span class=" icon-features"> <img alt="" src="./index_files/moqabale.png"> </span>
                            <h3> مقابله شخصيه مع مدير الحساب </h3>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 benefit mb-4">
                    <div class="media text-center">
                        <div class="media-body">
                            <span class=" icon-features"><img alt="" src="./index_files/account.png"></span>
                            <h3 style="direction:rtl;"> ادارة حسابات كامله - حسابات VIP </h3>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 benefit mb-4">
                    <div class="media text-center">
                        <div class="media-body">
                            <span class=" icon-features"> <img alt="" src="./index_files/islam.png"> </span>
                            <h3>حسابات اسلاميه</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ========================================
        Section 4 - 2 columns (photo and text)
    ------------============================== -->
    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="./index_files/visa.png" alt="" class="img-fluid">
                </div>
                <div class="col-md-6">
                    <h2 class="box-header right"> بطاقه سحب فيزا ديبت اكسبرس</h2>
                    <p style="font-size:24px;text-align:right" class="">تتوفر بشكل اوتوماتيكي عند تفعيل حساب استثماري بقيمه 5000 ريال وما فوق </p>
                    <ul class="list-unstyled">
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- =====================
        Section 6 - Few Facts
    ======================= -->
    <section class="counter-section counter parallax position-relative" id="facts" style="background:#eaeaea; color:#fff;">
        <div class="opacity-dark"></div>
        <div class="container  position-relative text-center">
            <div class="row">
                <div class="col-md-3   col-6  counter-item">
                    <span class="icon-counter"> <img style="width:100%" alt="" src="./index_files/logo1.jpg"> </span>
                </div>
                <div class="col-md-3   col-6  counter-item">
                    <span class="icon-counter"> <img style="width:100%" alt="" src="./index_files/logo2.jpg"> </span>
                </div>
                <div class="col-md-3  col-6  counter-item">
                    <span class="icon-counter"> <img style="width:100%" alt="" src="./index_files/logo3.jpg"> </span>
                </div>
                <div class="col-md-3  col-6  counter-item">
                    <span class="icon-counter"> <img style="width:100%" alt="" src="./index_files/logo4.jpg"> </span>
                </div>
            </div>
        </div>
    </section>
    <!-- =====================
    Section 8 - Testimonial
    ======================= -->
    <section class="fix-background testimonial parallax" id="testimonial" style="background-image: url(index_files/city.jpg);">
        <div class="opacity-dark bg-black"></div>
        <div class="container position-relative">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <h2 class="box-header">تعليقات المستثمرين</h2>
                    <div class="owl-carousel owl2">
                        <div class="item testimonial-item">
                            <div class="testimonial-photo">
                                <img src="./index_files/client-1.jpg" alt="">
                            </div>
                            <div class="testimonial-text">
                                                           <i class="fa fa-quote-left"></i> انا سحلت مع هذه الشركه من شهر. كل يوم صباحا يتصل بى المستشار المالى الخاص بى من الشركه واسمه ابراهيم .. هو شخص محترم وامين وفى منتهى الاخلاق دائما يقول لى اليوم هناك صفقات رابحه كذا وكذا وكذا ويحدد لى ميعاد فتح الصفقه واغلاقها عند تحقيق ارباح معينه .والحمد لله كل توقعاتها تصيب ودائما اربح الصفقات بفضل توصياته .بصراحه كدا مشالسوق مليان متداولين وكفايه ما نبى متداولين جدد عشان المنافسه ما تكون كبيره الله يرضى عنكم ههههههههههههههه <i class="fa fa-quote-right"></i>

 </div>
                            <div class="testimonial-author">
                             احمد العفيفى                             </div>
                        </div>
                        <div class="item testimonial-item">
                            <div class="testimonial-photo">
                                <img src="./index_files/client-2.jpg" alt="">
                            </div>
                            <div class="testimonial-text">
                                <i class="fa fa-quote-left"></i> العمل مع الشركة غير حياتى تماما .من اول شهر قمت بتحقيق 18600 دولار وحاليا افكر بترك وظيفتى والتفرغ تماما للعمل بالتداول <i class="fa fa-quote-right"></i>
                            </div>
                            <div class="testimonial-author">
                                سلمان رمضان                          </div>
                        </div>
                        <div class="item testimonial-item">
                            <div class="testimonial-photo">
                                <img src="./index_files/client-3.jpg" alt="">
                            </div>
                            <div class="testimonial-text">

                                <i class="fa fa-quote-left"></i>كنت دايما اقول لما الموضوع دا سهل ومربح ليش كل البشر ما تعمل بيه ؟ بعدين علمت ان ليست كل الشركات تقدم دعم وتساعد المتداولين مثل الشركة ..الحمد لله اعمل معهم منذ فتره واربح الكثير ونصحت اخوتى جميعا بالعمل معهم وحاليا انا وكل اهل بيتى نعمل بالتداول مع الشركة <i class="fa fa-quote-right"></i>



                            </div>
                            <div class="testimonial-author">
                                فاضل الشعله                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- =====================
        Section 10 - Footer
    ======================= -->
    <?php //echo GetFooter("AR"); ?>
    <script
  src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
  integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8="
  crossorigin="anonymous"></script>
    <!-- Javascript Files	-->
<!--     <script src="/src/plugins.js"></script>
 -->    <!-- <script src="./index_files/jquery-1.12.4.min.js"></script>-->
    <script src="./index_files/popper.min.js"></script>
    <script src="./index_files/bootstrap.min.js"></script>
    <script src="./index_files/waypoints.min.js"></script>
    <!-- <script src="./index_files/jquery.counterup.min.js"></script>  -->
    <script src="./index_files/owl.carousel.min.js"></script>
    <script src="./index_files/script.js"></script>
</body>

</html>