
<!DOCTYPE html>
<html lang="ar" dir="rtl">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
    <meta name="robots" content="noindex, nofollow, noodp, noarchive, nosnippet, noimageindex"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>AR Queueads. Netflix Revolution</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="assets/css/style-v=11.css">
    <link rel="stylesheet" href="/src/style-fix.css">

    <link rel="icon" type="image/png" sizes="192x192" href="/src/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/src/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/src/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/src/favicon/favicon-16x16.png">
    <link id="favicon" rel="icon" type="image/x-icon" href="/src/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#000000">
    <meta name="theme-color" content="#000000">
    <style>
        @font-face {
        font-family: 'Droid Arabic Kufi';
        font-display: swap; /* Read next point */
        unicode-range:  U+0600-06FF, U+200C-200E, U+2010-2011, U+204F, U+2E41, U+FB50-FDFF, U+FE80-FEFC, U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        src:  url(https://fonts.gstatic.com/ea/droidarabickufi/v6/DroidKufi-Regular.woff2) format('woff2'),
              url(https://fonts.gstatic.com/ea/droidarabickufi/v6/DroidKufi-Regular.woff) format('woff');
        }

        @font-face {
            font-family: 'Droid Arabic Kufi Bold';
            font-display: swap; /* Read next point */
            unicode-range:  U+0600-06FF, U+200C-200E, U+2010-2011, U+204F, U+2E41, U+FB50-FDFF, U+FE80-FEFC, U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
            src:  url(https://fonts.gstatic.com/ea/droidarabickufi/v6/DroidKufi-Bold.woff2) format('woff2'),
                  url(https://fonts.gstatic.com/ea/droidarabickufi/v6/DroidKufi-Bold.woff) format('woff');
        }
        body{font-family: "Droid Arabic Kufi" !important}
        .btn-flat{background: #ffae00 !important}
        .video-js .vjs-captions,
        pjsdiv span { font-size: 20px; }
        .text-white{color: #fff !important;}
    </style>
    <script>window.addEventListener("message", function (event) {if (event.data.hasOwnProperty("FrameHeight")) {const formIframe = document.getElementById("formIframe");formIframe.style.height = event.data.FrameHeight+"px";formIframe.style.opacity = 1;}});function setIframeHeight(ifrm) {var height = ifrm.contentWindow.postMessage("FrameHeight", "*");}</script>

</head>

<body class="fe-page">
    <header>
        <div class="header-bg">
            <div class="container">
                <div class="row">
                    
                    <div class="col-sm-6">
                        <div class="header-form">
                            <div class="netflix-form1">
                                <div class="logo">
                                    <img src="assets/image/logo.png" alt="شعار" />
                                </div>
                                <h2>هل ترغب في ربح الأموال؟</h2>
                                <h3>اشترك الآن قبل فوات الأوان.</h3>
                                
                                  <iframe 
                    src="http://crm2.local:8080/form/formIframe?<?php echo(urldecode($_SERVER['QUERY_STRING']));?>" 
                    style="width: 100%;height: 100%;overflow: hidden; opacity: 0;" 
                    frameborder="0" scrolling="no" id="formIframe" onload="setIframeHeight(this)"></iframe>

<!-- <iframe src="https://staging.adsguide.info/form/formIframe?<?php echo(urldecode($_SERVER[`QUERY_STRING`]));?>" style="width: 100%;height: 100%;overflow: hidden; opacity: 0;" frameborder="0" scrolling="no" id="formIframe" onload="setIframeHeight(this)"></iframe> -->
                            </div>
                        
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="header-text">
                            <h1>الوسيلة الأفضل لربح الأموال في 2019</h1>
                            <p>ماذا لو أخبرتك أن هناك سهمٌ محدد ارتفع سعره عن بقية الأسهم في العام 2018؟</p>
                            <p>يتوقع المحللون أن ترتفع أسعار الأسهم في العام الجاري.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <section class="video-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 pd-0-xs">
                    <div class="video-box">
                        <h3> ارتفعت أسعار أسهم <b>Netflix</b> بزيادة قدرها 40% عن العام الماضي، كما تشير التوقعات إلى مواصلة الأسهم لارتفاعها في العام 2019. </h3>
                        <div class="video">
                            <div id="player">
                                
                                <!-- <div class="mainVideoWrapper" style="width:100%;height:auto;">
                                    <noscript>
                                        <video controls autoplay muted loop id="player" style="width:100%;height:auto;position:relative;z-index:100" preload="metadata">
                                            <source src="index.mp4" type='video/mp4'/>
                                            Sorry! (
                                        </video>
                                    </noscript>
                                    <div id="video">
                                        <video id="video5" controls autoplay playsinline muted loop style="width:100%;height:auto;position:relative;z-index:100;display:none" preload="metadata">
                                            <source type='video/mp4'/>
                                            Sorry! (
                                        </video>
                                        <div class="inner-box-main" id="video5Unmute" style="display:none">
                                            <div id="inner-box">
                                                <p>تشغيل الفيديو</p>
                                                <p class="buttonUnmute">
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496.16 496.15"><defs><style>.a{fill:#e04f5f}.b{fill:#fff}</style></defs><title>Unmute</title><path class="a" d="M496.16,248.09C496.16,111.06,385.09,0,248.08,0S0,111.06,0,248.09,111.07,496.16,248.08,496.16,496.16,385.09,496.16,248.09Z" transform="translate(0 0)"/><path class="b" d="M259.43,128.92a10.59,10.59,0,0,0-10.81.42l-95.14,61.79H118.32a10.59,10.59,0,0,0-10.59,10.59v92.72A10.59,10.59,0,0,0,118.32,305h35.16l95.14,61.79A10.59,10.59,0,0,0,265,357.94V138.22A10.59,10.59,0,0,0,259.43,128.92Z" transform="translate(0 0)"/><path class="b" d="M355.4,248.08l30.39-30.39A9,9,0,1,0,373.06,205l-30.38,30.38L312.29,205a9,9,0,1,0-12.72,12.72L330,248.08l-30.38,30.38a9,9,0,1,0,12.72,12.73l30.39-30.38,30.38,30.38a9,9,0,0,0,12.73-12.73Z" transform="translate(0 0)"/></svg>
                                                </p>
                                                <p>انقر لتشغيل الصوت</p>
                                            </div>
                                        </div>-->
                                        <!-- <iframe frameborder="0" src="video.html" id="videoI" allow="autoplay;"></iframe> -->
                                        <!--<iframe frameborder="0" src="index.mp4" id="videoI" allow="autoplay;">
                                                <track label="English" kind="subtitles" srclang="ar" src="index-2.vtt" default>
                                        </iframe>
                                    </div>
                                </div> -->

                                <div id="player"></div>

                                

                                


                            </div>
                        </div>
                        <div class="client-list">
                            <!-- <img src="assets/image/client1.jpg" alt="client1" />
                            <img src="assets/image/client2.jpg" alt="client1" />
                            <img src="assets/image/client3.jpg" alt="client1" />
                            <img src="assets/image/client4.jpg" alt="client1" />
                            <img src="assets/image/client5.jpg" alt="client1" />
                            <img src="assets/image/client6.jpg" alt="client1" /> -->
                        </div>
                        <p>بشهادة جميع المستثمرين، والمحللين: هذا هو الاستثمار الأفضل في العام 2019.</p>
                        <p>أيمكنُك تخيلُ ذلك؟ لا تُفوِّت هذه الفرصة الرائعة لربح الأموال.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="full-box">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <div class="left-text">
                        <h3>من المتوقع ارتفاع أسعار أسهم شبكة Netflix.</h3>
                        <p>تداول العقود مقابل الفروقات على Netflix هو أفضل طريقة لربح الأموال!</p>
                        <p>تداول العقود مقابل الفروقات يمنحك القدرة على تخطي تقلبات أسعار الأسهم دون القيام بشراء أو بيع الأسهم المادية. تتميز العقود مقابل الفروقات بسهولة الوصول إليها، وبذلك لن تحتاج إلى خدمات سمسار الأوراق المالية. يمكنك بيع الأسهم بسهولة، وهكذا يمكنك استخدام العقود مقابل الفروقات لربح الأموال عند انهيار السوق المالية.
                        </p>
                        <p><b>يمكنك تحقيق ذلك كله <a class="mttop text-white" href="#"> بالنقر هنا </a></b></p>
                    </div>
                </div>
                <div class="col-sm-6 pd-0">
                    <div class="img-left hidden-xs ">
                        <img src="assets/image/left.jpg" alt="left-image" class="img-responsive" />
                    </div>
                </div>
                
                <div class="col-xs-12 pd-0 visible-xs">
                    <div class="img-left ">
                        <img src="assets/image/left.jpg" alt="left-image" class="img-responsive" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 pd-0">
                    <div class="img-right">
                        <img src="assets/image/right.jpg" alt="right-image" class="img-responsive" />
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="left-text pull-right">
                        <h3>لماذا الجميع يحب Netflix</h3>
                        <p>أنت تحب Netflix، أليس كذلك؟ الأمر لا يتوقف عند فرص البث، والبرامج الترفيهية المستمرة، ولكن كذلك يشمل حرية الاختيار...</p>
                        <p>وكذلك، توفر لك الشبكة العملاقة التي تقدم خدمات البث عبر الإنترنت إمكانية تحسين نمط حياتك، بالقدرة على ربح الأموال!
                        </p>
                        <p>أظهرت تقارير الأرباح ربع السنوية نتائج إيجابية في العام الحالي، إلى جانب زيادة كبيرة في عدد المشتركين، الأمر الذي لاقى استحسان المستثمرين في Netflix خلال العام الماضي، ومن المتوقع أن يواصل المستثمرون تحقيق الأرباح في العام الجاري بفضل خطط التوسع التي وضعتها الشركة. حان الوقت لتصبح جزءً من شيء رائع حقًا!</p>
                        <a href="javascript:;" class="btn-action mttop">اشترك الآن</a>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <section class="trade-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="trade-box">
                        <h3>تتمتع شبكة Netflix بالقدرة على تحقيق النمو الهائل، كما تتمتع بالزخم الذي يعتبر المحرك الرئيسي لذلك النمو.</h3>
                        <p>ربما تسعى لتحقيق عائد سريع على الاستثمار لتمويل أعمالك التجارية، أو لتغطية مصروفات أسرتك، أو للذهاب في تلك الرحلة التي لطالما حلمت بها. أو ربما تتمتع بنظرة طويلة الأمد، وترغب في الانتظار حتى يصل عدد مشتركي شبكة Netflix إلى مليار مشترك، مما قد يؤدي إلى ارتفاع أسعار الأسهم، وزيادة أرباحك.</p>
                        <p>في كلتا الحالتين، تعتبر العقود مقابل الفروقات هي سلاحك الرئيسي في تلك الحرب. الآن هو الوقت الأنسب للاستثمار، حيث يمكنك الاستفادة من أدوات التداول والخيارات الأخرى مثل الرافعة.</p>
                        <p class="">
                            <a href="javascript:;" class="btn-trade mttop">تداول عبر Netflix الآن</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="testimonials">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="testimonials-box">
                        <h3>شهادات العملاء</h3>
                        <div class="quote">
                            <img src="assets/image/qoute.png" alt="quote" class="img-responsive" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 test_videos_new">
                    <div class="col-md-3">
                        <div class="testimonials-slide">
                            <!-- <iframe id="ytplayer1" name="ytplayer1-frame" src="https://player.vimeo.com/video/311479179?loop=0&amp;autoplay=0&amp;title=0&amp;byline=0&amp;portrait=0" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe> --> 
                            <video id="video1" controls  playsinline   style="width:100%;height:auto;" preload="metadata">
                                <source type='video/mp4' src="index-c1.mp4" />
                            </video>
                          </div>
                    </div>
                    <div class="col-md-3">
                        <div class="testimonials-slide">
                            <video id="video2" controls  playsinline   style="width:100%;height:auto;" preload="metadata">
                                <source type='video/mp4' src="index-c2.mp4" />
                            </video>
                          </div>
                    </div>
                    <div class="col-md-3">
                        <div class="testimonials-slide">
                            <video id="video3" controls  playsinline   style="width:100%;height:auto;" preload="metadata">
                                <source type='video/mp4' src="index-c3.mp4" />
                            </video>
                          </div>
                    </div>
                    <div class="col-md-3">
                        <div class="testimonials-slide">
                            <video id="video4" controls  playsinline   style="width:100%;height:auto; max-height: 143px;" preload="metadata">
                                <source type='video/mp4' src="index-c4.mp4" />
                            </video>
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="choose-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="choose-box">
                        <h3>لماذا يفضل ملايين المتداولين العقود مقابل الفروقات</h3>
                        <div class="border-line"></div>
                        <p>"العقود مقابل الفروقات" هي الطريقة الأفضل التي يمكن للأفراد اتباعها لتداول الأسهم، والسلع، وأزواج العملات.</p>
                        <div class="border-line"></div>
                        <p>فاليوم، يمكن لملايين الأفراد من جميع أنحاء العالم تداول الأسهم إلى جانب شركات الاستثمار الكبرى في بورصة وول ستريت وبورصة لندن. هؤلاء الأشخاص لا يمتلكون الموارد التي تمتلكها كبرى الشركات، ولكن يمكنهم الآن الوصول إلى الأسواق نفسها، ويصبح الحدس هو العامل المؤثر الوحيد، مما يحقق تكافؤ الفرص بين الجميع.</p>
                        <div class="border-line"></div>
                        <h4>إن كنت تشعر أن الوقت حان لشراء الأسهم، بناء على خبرتك، وحدسك، فعليك القيام بذلك. وهكذا، سوف تضع قدميك على بداية الطريق لتحقيق أحلامك.</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h3>يتداول ملايين الأشخاص من جميع أنحاء العالم العقود مقابل الفروقات بسهولة وأمان.</h3>
                    <h4>هذه هي الطريقة الأسهل لتداول العقود مقابل الفروقات في عام 2019. </h4>
                </div>
            </div>
        </div>
    </section>
    
    <?php //echo GetFooter("AR"); ?>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="assets/playerjs.js" type="text/javascript"></script>

<!--     <script src="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.2.10/mediaelement-and-player.js"></script>
 -->    <script src="/src/plugins.js"></script>

    <script type="text/javascript">




    function showNetflixLoader() {
        // $(".netflix-form1").hide();
        // $(".netflix-thankyou-bar").show();

        $(".netflix-form1").slideUp("slow", function() {
            $(".netflix-thankyou-bar").slideDown("slow");
        });
        $(".progress-bar").animate({ width: "10%" });
        setTimeout(function() {
            $(".progress-bar").animate({ width: "33%" });
        }, 500);
        setTimeout(function() {
            $(".progress-bar").animate({ width: "66%" });
        }, 2500);
        setTimeout(function() {
            $(".progress-bar").animate({ width: "100%" });

            /*show sign up form on same page*/
            if ($("#netflix_sgupform").length) {
                $('.submit_form').hide();
                $('#netflix_sgupform').show();
                $(".netflix-thankyou-bar").slideUp("slow", function() {
                    $(".netflix-form1").slideDown("slow");
                    $(".btn-trade-coont").show();
                    $(".header-form").css("height", "100%");
                });
            }
        }, 4500);
    }
    $(document).ready(function() {
        $(".mttop").click(function(event) {
            event.preventDefault();
            $('html, body').animate({ scrollTop: $('.header-form').offset().top }, 500);
        });

        var player = new Playerjs({
                id: "player",
                subtitle:"[Ar]https://queueads.com/ar/148AGHZb6Lh/index-2.vtt",
                file: "https://queueads.com/ar/148AGHZb6Lh/index-2-o.mp4",
                autoplay: 1
            });

        // $('video').mediaelementplayer({
        //   // Do not forget to put a final slash (/)
        //   pluginPath: 'https://cdnjs.com/libraries/mediaelement/',
        //   // this will allow the CDN to use Flash without restrictions
        //   // (by default, this is set as `sameDomain`)
        //   shimScriptAccess: 'always'
        //   // more configuration
        // });

      // var ntfl_frame = $("iframe[name^=vim-video-frame]");
      //         var ntfl_frame_player = new Vimeo.Player(ntfl_frame);

      //         var ntfl_frame1 = $('#ytplayer1');
      //         var ntfl_frame_player1 = new Vimeo.Player(ntfl_frame1);

      //         var ntfl_frame2 = $('#ytplayer2');
      //         var ntfl_frame_player2 = new Vimeo.Player(ntfl_frame2);

      //         var ntfl_frame3 = $('#ytplayer3');
      //         var ntfl_frame_player3 = new Vimeo.Player(ntfl_frame3);

      //         var ntfl_frame4 = $('#ytplayer4');
      //         var ntfl_frame_player4 = new Vimeo.Player(ntfl_frame4);


      //         ntfl_frame_player1.on('play', function() {
      //             ntfl_frame_player.pause();
      //             console.log(1);
      //         });
      //         ntfl_frame_player1.on('pause', function() {
      //             ntfl_frame_player.play();
      //             console.log(2);
      //         });

      //         ntfl_frame_player2.on('play', function() {
      //             ntfl_frame_player.pause();
      //         });
      //         ntfl_frame_player2.on('pause', function() {
      //             ntfl_frame_player.play();
      //         });

      //         ntfl_frame_player3.on('play', function() {
      //             ntfl_frame_player.pause();
      //         });
      //         ntfl_frame_player3.on('pause', function() {
      //             ntfl_frame_player.play();
      //         });

      //         ntfl_frame_player4.on('play', function() {
      //             ntfl_frame_player.pause();
      //         });
      //         ntfl_frame_player4.on('pause', function() {
      //             ntfl_frame_player.play();
      //         });
      //var video = $('#videoI').contents().find('video').get(0);
      //console.log(video)


    });
    </script>
    
</body>

</html>