<?php
include( $_SERVER['DOCUMENT_ROOT'].'/src/inc/forms/optsgen.php' );
$ipaddress = getClientIP();
$userDetails = getUserInfoByIP($ipaddress);
?>
<!DOCTYPE html>
<html lang="ar" dir="rtl" data-lt-installed="true">

<head>
<meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
    <meta name="robots" content="noindex, nofollow, noodp, noarchive, nosnippet, noimageindex"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Queueads</title>
    <!-- <link rel="stylesheet" href="./index_files/all.css"> -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" href="./index_files/7YR5JA9IYLLS.css">
    <link rel="stylesheet" href="/src/style-fix.css">
    
    <link rel="icon" type="image/png" sizes="192x192" href="/src/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/src/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/src/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/src/favicon/favicon-16x16.png">
    <link id="favicon" rel="icon" type="image/x-icon" href="/src/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#fea622">
    <meta name="theme-color" content="#fea622">


<style>
        @font-face {
    font-family: 'Droid Arabic Kufi';
    font-display: swap; /* Read next point */
    unicode-range:  U+0600-06FF, U+200C-200E, U+2010-2011, U+204F, U+2E41, U+FB50-FDFF, U+FE80-FEFC, U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    src:  url(https://fonts.gstatic.com/ea/droidarabickufi/v6/DroidKufi-Regular.woff2) format('woff2'),
          url(https://fonts.gstatic.com/ea/droidarabickufi/v6/DroidKufi-Regular.woff) format('woff');
}

@font-face {
    font-family: 'Droid Arabic Kufi Bold';
    font-display: swap; /* Read next point */
    unicode-range:  U+0600-06FF, U+200C-200E, U+2010-2011, U+204F, U+2E41, U+FB50-FDFF, U+FE80-FEFC, U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    src:  url(https://fonts.gstatic.com/ea/droidarabickufi/v6/DroidKufi-Bold.woff2) format('woff2'),
          url(https://fonts.gstatic.com/ea/droidarabickufi/v6/DroidKufi-Bold.woff) format('woff');
}
body{
    font-family: "Droid Arabic Kufi"
}
.btn-flat{
    margin-top: 20px !important;
    background: #FEA622 !important;
}
#tnc{
    background: #232323 !important;
}
#.register-box{
    padding: 0 !important
}
.registerForm.registerFormHome h3{
    margin-bottom: 20px !important;
}
        label.control-label, div.checkbox, #gaff > form > div:nth-child(11) > div:nth-child(1) { display:none; }
    div.col-md-6.form-group { width:100%; }
    #gaff > form > div:nth-child(10) { margin-bottom:15px; }
    #gaff > form > button, .submit-btn {
        min-height: 50px;
        width: 100%;
        background: #FEA622;
        color: white;
        font-family: "Poppins",sans-serif;
        font-weight: 500;
        font-size: 1.5em;
        border-color: #feb03b;
        white-space: normal;
        box-shadow: none;
        border: 1px solid transparent;
        border-radius: 4px;
    } 
    
    #gaff > form > button { margin-top: 15px; }
    
    /* Step 2 */
    #gaff > form > div:nth-child(12) > div:nth-child(2) { display:none; }
    
    /* Step 3 */
    #gaff > form > div:nth-child(11) > div:nth-child(2), #gaff > form > button { display:none; }

<?php if(!isset($_GET['gdpr'])){ ?>
#cookie-bar{
    display: none ;
}
<?}?>

</style>
 <script>window.addEventListener("message", function (event) {if (event.data.hasOwnProperty("FrameHeight")) {const formIframe = document.querySelectorAll(".formIframe");for (var i = 0; i < formIframe.length; i++) {formIframe[i].style.height = event.data.FrameHeight+"px";formIframe[i].style.opacity = 1;}}});function setIframeHeight(ifrm) {var height = ifrm.contentWindow.postMessage("FrameHeight", "*");}</script>

    <?php //echo getPageCode(@$_GET['ref_id']); ?>
</head>

<body data-target="#mainNavbar" cz-shortcut-listen="true">
    <?php //echo AfterHeader(@$_GET['ref_id']); ?>

    <div id="aff_place"></div>
    <div id="app">
        <div class="topBarCTA">
            <div class="container">
                <span class="fas fa-exclamation-triangle"></span> لدينا عدد غير مسبوق من المستخدمين الجدد. <strong class="topBarCTA-date">13/05/2019</strong>
            </div>
        </div>
        <section class="heroSection" id="about">
            <!-- <div class="disclaimerHeader">
                -&nbsp;إعلاني&nbsp;-
            </div> -->
            <nav class="navbar navbar-default" id="mainNavbar">
                <div class="container">
                    <div class="navbar-header">
                        <span class="navbar-brand">
                            <!-- <img src="./index_files/NAB6K3CD7WTG.png" class="img-responsive" alt="Bitcoin Compass"> -->
                        </span>
                    </div>
                    <div id="modal-winner" class="notificationWrap" style="display: block;">
                        <div class="flex-row">
                            <div class="text-right">
                                <img src="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/2.1.0/flags/4x3/<?=strtolower($userDetails["countryCode"]); ?>.svg" class="img-responsive pull-right winnerFlag" alt="Flag">
                            </div>
                            <div class="text-left winnerName">
                                <span class="name">أتى نادية</span>
                            </div>
                            <div class="text-right status">
                                <span><i class="fas fa-check"></i> منضم</span>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
            <div class="container">
                <h1 class="heroHeadline">يزداد الناس ثراءً من البيتكوين</h1>
                <h2 class="heroSubheadline">يمكنك أن تكون المليونير القادم!</h2>
                <div class="row hero-flex-block">
                    <div class="col-lg-8 col-md-8">
                        <div class="whiteBorderWrap">
                            <div class="videoOverlayEnd">
                                <h1>المشاركة في ثورة البيتكوين الآن أسهل من أي وقت مضى.</h1>
                                <h2>ما عليك سوى ملء بياناتك على الجانب الأيمن واتباع الإرشادات لتبدأ اليوم في الاستثمار.</h2>
                            </div>
                            <div id="mainVideo">
                                <div class="component-ivideo" id="kp2fwa0wpvr-ivideo">
                                    <!-- <div class="videoWrapper">
                                        <iframe src="./index_files/334375923.html" width="640" height="480" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                                    </div>
                                    <div class="videoOverlayInner" style="display: none;">
                                        <div class="videoOverlayHeadline">انقر لتشاهد وتتعلم السر!</div>
                                        <a href="https://ar.thebitcoincompass.live/#overlay" id="videoOverlayPlay"><img src="./index_files/5IVBOMHNSPSI.png" alt=""></a>
                                    </div> -->
                                    <div class="mainVideoWrapper" style="width:100%;height:auto;">
                                    <noscript>
                                        <video controls autoplay muted loop id="player" style="width:100%;height:auto;position:relative;z-index:100" preload="metadata">
                                            <source src="index.mp4" type='video/mp4'/>
                                            Sorry! (
                                        </video>
                                    </noscript>
                                    <div id="video">
                                        <video id="video5" controls autoplay playsinline muted loop style="width:100%;height:auto;position:relative;z-index:100;display:none" preload="metadata">
                                            <source type='video/mp4'/>
                                            Sorry! (
                                        </video>
                                        <div class="inner-box-main" id="video5Unmute" style="display:none">
                                            <div id="inner-box">
                                                <p>تشغيل الفيديو</p>
                                                <p class="buttonUnmute">
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496.16 496.15"><defs><style>.a{fill:#e04f5f}.b{fill:#fff}</style></defs><title>Unmute</title><path class="a" d="M496.16,248.09C496.16,111.06,385.09,0,248.08,0S0,111.06,0,248.09,111.07,496.16,248.08,496.16,496.16,385.09,496.16,248.09Z" transform="translate(0 0)"/><path class="b" d="M259.43,128.92a10.59,10.59,0,0,0-10.81.42l-95.14,61.79H118.32a10.59,10.59,0,0,0-10.59,10.59v92.72A10.59,10.59,0,0,0,118.32,305h35.16l95.14,61.79A10.59,10.59,0,0,0,265,357.94V138.22A10.59,10.59,0,0,0,259.43,128.92Z" transform="translate(0 0)"/><path class="b" d="M355.4,248.08l30.39-30.39A9,9,0,1,0,373.06,205l-30.38,30.38L312.29,205a9,9,0,1,0-12.72,12.72L330,248.08l-30.38,30.38a9,9,0,1,0,12.72,12.73l30.39-30.38,30.38,30.38a9,9,0,0,0,12.73-12.73Z" transform="translate(0 0)"/></svg>
                                                </p>
                                                <p>انقر لتشغيل الصوت</p>
                                            </div>
                                        </div>
                                        <iframe width="640" height="480" frameborder="0" allow="autoplay; fullscreen" allowfullscreen="" src="index.mp4" id="videoI" allow="autoplay;"></iframe>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="registerForm registerFormHome">
                            <h3>غيّر حياتك اليوم!</h3>
                            <div class="regform">
                                <div class="component-form-registration" id="dcrzwyzu-registrationform">
                                    <!-- <div class="stepIndicator">
                                        <div class="li-1 stepBadge active"><span class="hide"><i class="fas fa-check"></i></span><span class="">1</span></div>
                                        <div class="li-2 stepBadge"><span class="hide"><i class="fas fa-check"></i></span><span class="">2</span></div>
                                        <div class="li-3 stepBadge"><span class="hide"><i class="fas fa-check"></i></span><span class="">3</span></div>
                                    </div> -->
                                   
                                        <input class="brokerName" type="hidden" value="FXVCAU">
                                        <div class="model-msg-wrapper">
                                            <div class="model-msg">
                                                <div class="model-msg__close"></div>
                                                <div class="model-msg__response"></div>
                                            </div>
                                        </div>


                                         <iframe src="https://staging.adsguide.info/form/formIframe?<?php echo(urldecode($_SERVER['QUERY_STRING']));?>" style="width: 100%;height: 100%;overflow: hidden; opacity: 0;" frameborder="0" scrolling="no" class="formIframe" onload="setIframeHeight(this)"></iframe>

                                       
                                        <div class="wait">
                                            <div class="loader"></div>
                                        </div>
                                    </div>&nbsp;
                                    <!-- <button class="submit-btn step2">AVANTI</button>
                                    <button class="submit-btn step3" style="display:none;">AVANTI</button> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="heroLogos">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-md-7">
                            <div class="flex-row">
                                <img src="./index_files/6FT1NWLHTH14.png" alt="" class="img-responsive">
                                <img src="./index_files/LMEQBP3310TC.png" alt="" class="img-responsive">
                                <img src="./index_files/SXWL0J420KUP.png" alt="" class="img-responsive">
                                <img src="./index_files/AIJHVI0IPDB2.png" alt="" class="img-responsive">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-5">
                            <div class="heroBtc">
                                <span class="btc-title">BTC السعر</span>
                                <div class="flex-row">
                                    <div class="item btc-item">
                                        <img src="./index_files/S1QNRGY3RL79.png" alt="" class="img-responsive">
                                    </div>
                                    <div class="item flex-row">
                                        <span>01/2017</span>
                                        <strong>
                                            13.96
                                        </strong>
                                    </div>
                                    <div class="item flex-row">
                                        <span class="btc-date">05/2019</span><strong class="btc-rise">$7883</strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="heroBTC">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <img src="./index_files/B9068PPPRJV7.jpg" alt="Bitcoin Compass" class="img-responsive hero-btc-man">
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <h1>
                            انضم إلى ثورة البيتكوين الآن وابدأ رحلة الثراء
                        </h1>
                        <span style="opacity:.5;">
                            البيتكوين في الإعلام:
                        </span>
                        <img src="./index_files/4WKCB9UXBVYU.png" alt="Bitcoin Compass" class="img-responsive hero-btc-logos">
                        <p class="lead">
                            يقدم لك البيتكوين فرصة حصرية لتصبح مليونيراً! تقدم الثورة الجديدة عوائد استثنائية على استثماراتك وبفضلها أصبح الكثير من الناس بين عشية وضحاها من أصحاب الملايين. أعضائنا هم من أكثر الأشخاص ثراءً في العالم، وهم يكسبون هذه الملايين دون بذل أي مجهود.
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <hr class="divider">
        <section id="howToStart">
            <div class="container">
                <h1 class="sectionHeadline text-center">انضم إلى Bitcoin Compass اليوم لتصبح مليونيراً غداً!</h1>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="step">
                            <div class="step-head">الخطوة 1</div>
                            <h2>سجّل اليوم</h2>
                            <p class="lead">يستغرق التسجيل بضع دقائق فقط وبمجرد الانتهاء منه، ستصبح عضواً في Bitcoin Compass وسيمكنك الانضمام إلى أصحاب الملايين معنا.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="step">
                            <div class="step-head">الخطوة 2</div>
                            <h2>قم بتمويل حسابك</h2>
                            <p class="lead">ابدأ في استثمارك الأول بما لا يقل عن 250 دولار أمريكي، وشاهد ما يمكنك تحقيقه من ربح. لا توجد حدود. كلما استثمرت أكثر، زاد الربح.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="step">
                            <div class="step-head">الخطوة 3</div>
                            <h2>ابدأ في التداول</h2>
                            <p class="lead">الأمر بهذه البساطة. تسمح لك منصتنا بالتداول بسرعة وسهولة وضمان تحقيق أكبر الأرباح.</p>
                        </div>
                    </div>
                </div>
                <div class="chartWrapper" id="btc-chart">
                    <div class="chart-title">
                        BTC
                    </div>
                    <div class="chart-axis">
                        <span class="chart-year start">
                            2015
                        </span>
                        <span class="chart-year">
                            2016
                        </span>
                        <span class="chart-year">
                            2017
                        </span>
                        <span class="chart-year end">
                            2018
                        </span>
                    </div>
                    <div class="chartInnerWrapper">
                        <div class="arrowIndicator">
                        </div>
                        <svg preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="3 0 243.52 94.97">
                            <defs>
                                <style>
                                    .cls-1,
                                    .cls-4 {
                                        fill: none;
                                    }
                                    
                                    .cls-2 {
                                        clip-path: url(#clip-path);
                                    }
                                    
                                    .cls-3 {
                                        fill: #daeeba;
                                        stroke: #daeeba;
                                        stroke-linejoin: round;
                                        stroke-width: 0px;
                                    }
                                    
                                    .cls-3,
                                    .cls-4 {
                                        stroke-linecap: round;
                                    }
                                    
                                    .cls-4 {
                                        stroke: #3AAA35;
                                        stroke-miterlimit: 10;
                                        stroke-width: 1px;
                                    }
                                </style>
                                <clippath id="clip-path" transform="translate(0 -3.19)">
                                    <rect class="cls-1" x="1.25" width="249.02" height="98.04"></rect>
                                </clippath>
                            </defs>
                            <title>BTC</title>
                            <g id="chart">
                                <g class="cls-2">
                                    <g id="chart-group">
                                        <path class="cls-3" fill="#daeeba" id="chart-shape" d="M247.5 98.5h-253v-6.4L-3 92l2.5-.1 3.1.2 2.5-.1 2.5.1 2.5-.1h7.5l2.5-.1 2.5-.1h5l2.5.1 2.5-.2 3.2-.4 2.5-.1 2.5-.7 2.5.4 2.5-.1 2.5.1h7.5l2.5.3h2.5l2.5.1h3.1l2.5-.1 2.5-.2 2.5.1 2.5.1 2.5-.1h2.5l2.5-.1 2.5-.1 2.5-.3 2.5.1h2.5l2.5-.2h3.1l2.5-.1h2.5l2.5-.1 2.5-.5 2.5-.3 2.5.3 2.5.4 2.5-.5h2.5l2.5-.5 2.5.1 2.5-.2 3.1-.5 2.5-.5 2.5.4 2.5.9 2.5.1 2.5-.6 2.5-.4h2.5l2.5-.5 2.5-.3 2.5-1 2.5-1 2.5-1.2h3.2l2.5-2.3 2.5-1.6 2.5 1.2 2.5.3 2.5.7 2.5-.7 2.5 2.7 2.5-4 2.5.6 2.5-2.5 2.5-2.9 3.1-1.3 2.5-.9 2.5-1.1 2.5 1.2 2.5 2.9 2.5-.4 2.5-2.7 2.5-.3 2.5-6.4 2.5-.9 2.5 1.3 2.5-7.7 2.5 4.9 3.1-6.7 2.5-4.5 2.5-10 2.5-18.5 2.5-20.7 2.5 13.7 2.5 9L247 9.8z"></path>
                                        <g stroke-linejoin="round" id="chart-line" class="cls-4">
                                            <path vector-effect="non-scaling-stroke" fill="none" stroke="#337ab7" stroke-width="2" d="M-300-294"></path>
                                            <path vector-effect="non-scaling-stroke" fill="none" stroke="#3aaa35" stroke-linecap="round" stroke-miterlimit="10" d="M-5.5 92.1L-3 92l2.5-.1 3.1.2 2.5-.1 2.5.1 2.5-.1h7.5l2.5-.1 2.5-.1h5l2.5.1 2.5-.2 3.2-.4 2.5-.1 2.5-.7 2.5.4 2.5-.1 2.5.1h7.5l2.5.3h2.5l2.5.1h3.1l2.5-.1 2.5-.2 2.5.1 2.5.1 2.5-.1h2.5l2.5-.1 2.5-.1 2.5-.3 2.5.1h2.5l2.5-.2h3.1l2.5-.1h2.5l2.5-.1 2.5-.5 2.5-.3 2.5.3 2.5.4 2.5-.5h2.5l2.5-.5 2.5.1 2.5-.2 3.1-.5 2.5-.5 2.5.4 2.5.9 2.5.1 2.5-.6 2.5-.4h2.5l2.5-.5 2.5-.3 2.5-1 2.5-1 2.5-1.2h3.2l2.5-2.3 2.5-1.6 2.5 1.2 2.5.3 2.5.7 2.5-.7 2.5 2.7 2.5-4 2.5.6 2.5-2.5 2.5-2.9 3.1-1.3 2.5-.9 2.5-1.1 2.5 1.2 2.5 2.9 2.5-.4 2.5-2.7 2.5-.3 2.5-6.4 2.5-.9 2.5 1.3 2.5-7.7 2.5 4.9 3.1-6.7 2.5-4.5 2.5-10 2.5-18.5 2.5-20.7 2.5 13.7 2.5 9L247 9.8"></path>
                                            <path vector-effect="non-scaling-stroke" fill="none" stroke="#337ab7" stroke-width="1.1" d="M-190-39"></path>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </svg>
                    </div>
                </div>
                <p class="lead text-center col-md-10 col-md-offset-1">صنفت فوربس البيتكوين كأفضل استثمار في عام 2013. وفي عام 2014، صنفت بلومبرج البيتكوين كأحد أسوأ استثماراتها خلال العام، ولكن في عام 2015، عاد البيتكوين ليتصدر جداول العملات لدى بلومبرج وما زالت قيمة البيتكوين في ارتفاع.</p>
                <div class="newsLogos text-center">
                    <img src="./index_files/ACHFYX35AC0G.png" class="img-responsive" alt="Bloomberg">
                    <img src="./index_files/C1STKIWQNRQP.png" class="img-responsive" alt="Forbes">
                </div>
            </div>
        </section>
        <section class="lightGreySection" id="happyUsers">
            <!-- <div class="famousPeople">
                <div class="container">
                    <h1 class="text-center sectionHeadline">أسماء مشهورة في التداول</h1>
                    <h2 class="text-center col-lg-8 col-lg-offset-2">هناك الكثير من الأسماء الكبيرة في عالم التداول. إليك ما يقوله بعض المتداولون الأكثر شهرة.</h2>
                </div>
                <div class="container famousPeopleInnerWrap">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="famousPeopleSinglePerson">
                                <h3 class="famousPerson--name">
                                    Alexander Elder
                                </h3>
                                <p class="famousPerson--quote">
                                    بإمكانك أن تكون حراً. يمكنك العيش والعمل في أي مكان في العالم. بإمكانك التخلص من الروتين ولن يتعين عليك الإجابة على أي شخص.
                                </p>
                                <img src="./index_files/8U3GJ9FUAOUB.jpg" class="img-responsive" alt="">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="famousPeopleSinglePerson">
                                <h3 class="famousPerson--name">
                                    Ed Seykota
                                </h3>
                                <p class="famousPerson--quote">
                                    الفوز أو الخسارة، يحصل الجميع على ما يريدون من السوق. ويبدو أن بعض الناس يرغبون في الخسارة، لذا فهم يربحون بخسارة المال.
                                </p>
                                <img src="./index_files/G7X3UQKC8KKT.jpg" class="img-responsive" alt="">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="famousPeopleSinglePerson">
                                <h3 class="famousPerson--name">
                                    William O’Neil
                                </h3>
                                <p class="famousPerson--quote">
                                    ما يبدو مرتفعاً جداً ومحفوفاً بالمخاطر بالنسبة للغالبية يرتفع بشكل عام، وما يبدو منخفضاً ورخيصاً يتراجع بشكل عام.
                                </p>
                                <img src="./index_files/BHUZGFLIDNN4.jpg" class="img-responsive" alt="">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="famousPeopleSinglePerson">
                                <h3 class="famousPerson--name">
                                    Warren Buffett
                                </h3>
                                <p class="famousPerson--quote">
                                    لست بحاجة لأن تكون عالم صواريخ. الاستثمار ليس لعبة يسحق فيها الرجل صاحب معدل الذكاء 160 ذاك الرجل صاحب معدل الذكاء 130.
                                </p>
                                <img src="./index_files/T1ODLZWGWYF1.jpg" class="img-responsive" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
            <div class="app-features">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="feature">
                                <div class="feature-icon">
                                    <img src="./index_files/FS31IF5AKMCG.png" alt="" class="img-responsive">
                                </div>
                                <div class="feature-name">
                                    دقة الأداء
                                </div>
                                <div class="feature-text">
                                    لا توجد منصة تداول أخرى تعمل بشكل أكثر دقة من Bitcoin Compass. ولذلك فعملائنا قادرون على مضاعفة أرباحهم بسرعة وسهولة.
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="feature">
                                <div class="feature-icon">
                                    <img src="./index_files/EM6RXVE4RZA5.png" alt="" class="img-responsive">
                                </div>
                                <div class="feature-name">
                                    أحدث التقنيات
                                </div>
                                <div class="feature-text">
                                    تستخدم منصتنا أحدث التقنيات المتطورة لإدارة المشهد حتى لا تضطر لفعل ذلك بنفسك. كما توفر منصتنا فعلياً أداءً متميزاً وسهل الاستخدام بشكل لا يصدق.
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="feature">
                                <div class="feature-icon">
                                    <img src="./index_files/IMETQD3US9KT.png" alt="" class="img-responsive">
                                </div>
                                <div class="feature-name">
                                    تداول موثوق
                                </div>
                                <div class="feature-text">
                                    حصلت Bitcoin Compass على العديد من جوائز التداول ذات الثقة، بما في ذلك جائزة نقابة التجار الأوروبيين وكأس مؤتمر التجار في العالم.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="userTable">
            <div class="container">
                <div class="userTable-block">
                    <h1 id="liveUsers" class="text-center">المتداولون المتصلون عبر الإنترنت</h1>
                    <div class="table-responsive">
                        <div id="tradersTableWrap">
                            <div class="tradersTableFadeOutWrap">
                                <div class="tradersTableFadeOut"></div>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tradersTable table-striped">
                                    <tbody>
                                        <tr>
                                            <th width="158" scope="col">مستخدم</th>
                                            <th width="160" scope="col">الوقت</th>
                                            <th width="104" scope="col">مسجل</th>
                                        </tr>
                                        <tr class="myhide">
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">J******</td>
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">01:29:33 15/05/2019</td>
                                            <td class="tradersTableWinnerCell" style="height: 40px; line-height: 40px; font-size: 14px;"><i class="fas fa-check"></i></td>
                                        </tr>
                                        <tr class="myhide">
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">T******</td>
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">01:28:32 15/05/2019</td>
                                            <td class="tradersTableWinnerCell" style="height: 40px; line-height: 40px; font-size: 14px;"><i class="fas fa-check"></i></td>
                                        </tr>
                                        <tr class="myhide">
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">E******</td>
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">01:28:30 15/05/2019</td>
                                            <td class="tradersTableWinnerCell" style="height: 40px; line-height: 40px; font-size: 14px;"><i class="fas fa-check"></i></td>
                                        </tr>
                                        <tr class="myhide">
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">N******</td>
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">01:27:58 15/05/2019</td>
                                            <td class="tradersTableWinnerCell" style="height: 40px; line-height: 40px; font-size: 14px;"><i class="fas fa-check"></i></td>
                                        </tr>
                                        <tr class="myhide">
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">D******</td>
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">01:28:27 15/05/2019</td>
                                            <td class="tradersTableWinnerCell" style="height: 40px; line-height: 40px; font-size: 14px;"><i class="fas fa-check"></i></td>
                                        </tr>
                                        <tr class="myhide">
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">T******</td>
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">01:29:26 15/05/2019</td>
                                            <td class="tradersTableWinnerCell" style="height: 40px; line-height: 40px; font-size: 14px;"><i class="fas fa-check"></i></td>
                                        </tr>
                                        <tr class="myhide">
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">H******</td>
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">01:27:39 15/05/2019</td>
                                            <td class="tradersTableWinnerCell" style="height: 40px; line-height: 40px; font-size: 14px;"><i class="fas fa-check"></i></td>
                                        </tr>
                                        <tr class="myhide">
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">L******</td>
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">01:28:08 15/05/2019</td>
                                            <td class="tradersTableWinnerCell" style="height: 40px; line-height: 40px; font-size: 14px;"><i class="fas fa-check"></i></td>
                                        </tr>
                                        <tr class="myhide">
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">L******</td>
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">01:32:21 15/05/2019</td>
                                            <td class="tradersTableWinnerCell" style="height: 40px; line-height: 40px; font-size: 14px;"><i class="fas fa-check"></i></td>
                                        </tr>
                                        <tr class="myhide">
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">J******</td>
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">01:27:49 15/05/2019</td>
                                            <td class="tradersTableWinnerCell" style="height: 40px; line-height: 40px; font-size: 14px;"><i class="fas fa-check"></i></td>
                                        </tr>
                                        <tr class="myhide">
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">S******</td>
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">01:29:18 15/05/2019</td>
                                            <td class="tradersTableWinnerCell" style="height: 40px; line-height: 40px; font-size: 14px;"><i class="fas fa-check"></i></td>
                                        </tr>
                                        <tr class="myhide">
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">T******</td>
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">01:28:01 15/05/2019</td>
                                            <td class="tradersTableWinnerCell" style="height: 40px; line-height: 40px; font-size: 14px;"><i class="fas fa-check"></i></td>
                                        </tr>
                                        <tr class="myhide">
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">L******</td>
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">01:28:00 15/05/2019</td>
                                            <td class="tradersTableWinnerCell" style="height: 40px; line-height: 40px; font-size: 14px;"><i class="fas fa-check"></i></td>
                                        </tr>
                                        <tr class="myhide">
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">P******</td>
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">01:32:14 15/05/2019</td>
                                            <td class="tradersTableWinnerCell" style="height: 40px; line-height: 40px; font-size: 14px;"><i class="fas fa-check"></i></td>
                                        </tr>
                                        <tr class="myhide">
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">J******</td>
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">01:29:12 15/05/2019</td>
                                            <td class="tradersTableWinnerCell" style="height: 40px; line-height: 40px; font-size: 14px;"><i class="fas fa-check"></i></td>
                                        </tr>
                                        <tr class="myhide">
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">G******</td>
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">01:27:54 15/05/2019</td>
                                            <td class="tradersTableWinnerCell" style="height: 40px; line-height: 40px; font-size: 14px;"><i class="fas fa-check"></i></td>
                                        </tr>
                                        <tr class="myhide">
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">L******</td>
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">01:28:06 15/05/2019</td>
                                            <td class="tradersTableWinnerCell" style="height: 40px; line-height: 40px; font-size: 14px;"><i class="fas fa-check"></i></td>
                                        </tr>
                                        <tr class="myhide">
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">H******</td>
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">01:29:00 15/05/2019</td>
                                            <td class="tradersTableWinnerCell" style="height: 40px; line-height: 40px; font-size: 14px;"><i class="fas fa-check"></i></td>
                                        </tr>
                                        <tr class="myhide">
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">L******</td>
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">01:31:55 15/05/2019</td>
                                            <td class="tradersTableWinnerCell" style="height: 40px; line-height: 40px; font-size: 14px;"><i class="fas fa-check"></i></td>
                                        </tr>
                                        <tr class="myhide">
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">O******</td>
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">01:27:21 15/05/2019</td>
                                            <td class="tradersTableWinnerCell" style="height: 40px; line-height: 40px; font-size: 14px;"><i class="fas fa-check"></i></td>
                                        </tr>
                                        <tr class="myhide">
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">O******</td>
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">01:27:19 15/05/2019</td>
                                            <td class="tradersTableWinnerCell" style="height: 40px; line-height: 40px; font-size: 14px;"><i class="fas fa-check"></i></td>
                                        </tr>
                                        <tr class="myhide">
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">S******</td>
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">01:27:31 15/05/2019</td>
                                            <td class="tradersTableWinnerCell" style="height: 40px; line-height: 40px; font-size: 14px;"><i class="fas fa-check"></i></td>
                                        </tr>
                                        <tr class="myhide">
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">J******</td>
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">01:27:45 15/05/2019</td>
                                            <td class="tradersTableWinnerCell" style="height: 40px; line-height: 40px; font-size: 14px;"><i class="fas fa-check"></i></td>
                                        </tr>
                                        <tr class="myhide">
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">J******</td>
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">01:26:57 15/05/2019</td>
                                            <td class="tradersTableWinnerCell" style="height: 40px; line-height: 40px; font-size: 14px;"><i class="fas fa-check"></i></td>
                                        </tr>
                                        <tr class="myhide">
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">T******</td>
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">01:26:56 15/05/2019</td>
                                            <td class="tradersTableWinnerCell" style="height: 40px; line-height: 40px; font-size: 14px;"><i class="fas fa-check"></i></td>
                                        </tr>
                                        <tr class="myhide">
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">H******</td>
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">01:27:24 15/05/2019</td>
                                            <td class="tradersTableWinnerCell" style="height: 40px; line-height: 40px; font-size: 14px;"><i class="fas fa-check"></i></td>
                                        </tr>
                                        <tr class="myhide">
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">G******</td>
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">01:27:35 15/05/2019</td>
                                            <td class="tradersTableWinnerCell" style="height: 40px; line-height: 40px; font-size: 14px;"><i class="fas fa-check"></i></td>
                                        </tr>
                                        <tr class="myhide">
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">J******</td>
                                            <td style="height: 40px; line-height: 40px; font-size: 14px;">01:28:32 15/05/2019</td>
                                            <td class="tradersTableWinnerCell" style="height: 40px; line-height: 40px; font-size: 14px;"><i class="fas fa-check"></i></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="faq">
            <div class="container">
                <h1>الأسئلة المتكررة</h1>
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="faqBlock">
                            <div class="faqItem">
                                <a class="faqToggle collapsed" data-toggle="collapse" data-target="#faq1" aria-expanded="false">كم يمكنني أن أربح؟</a>
                                <div class="collapse" id="faq1" aria-expanded="false" style="height: 0px;">
                                    <p>دائماً ما يعتمد التداول على قدر ما أنت مستعد لاستثماره. فكلما زاد المال المستثمر، زاد الربح المتوقع. ونظراً لثقة المتداولين بنا، فهم قادرون على ربح الملايين.</p>
                                </div>
                            </div>
                            <div class="faqItem">
                                <a class="faqToggle collapsed" data-toggle="collapse" data-target="#faq2">هل أنا بحاجة لأي خبرة في التداول؟</a>
                                <div class="collapse" id="faq2">
                                    <p>لا! لقد تم تصميم Bitcoin Compass مع وضع المبتدئين في الاعتبار. لذا فمنصتنا هي أسهل مكان في العالم لبدء التداول.</p>
                                </div>
                            </div>
                            <div class="faqItem">
                                <a class="faqToggle collapsed" data-toggle="collapse" data-target="#faq3">كم يكلف الأمر؟</a>
                                <div class="collapse" id="faq3">
                                    <p>لن تكلفك منصتنا أي شيء. جميع الأموال التي تستثمرها هي أموالك التي يمكنك تداولها وسحبها في أي وقت.</p>
                                </div>
                            </div>
                            <div class="faqItem">
                                <a class="faqToggle collapsed" data-toggle="collapse" data-target="#faq4">كم عدد الأعضاء لديكم؟</a>
                                <div class="collapse" id="faq4">
                                    <p>لدينا حالياً أكثر من 400,000 عضو مسجل من جميع أنحاء العالم بقيمة مجمعة تزيد على 5 مليارات دولار!</p>
                                </div>
                            </div>
                            <div class="faqItem">
                                <a class="faqToggle collapsed" data-toggle="collapse" data-target="#faq5">ماذا لو انخفض سعر البيتكوين؟</a>
                                <div class="collapse" id="faq5">
                                    <p>حينها يحقق معظم أعضائنا أرباحهم. من خلال منصتنا، يمكنك كسب المال عندما ترتفع أسعار Bitcoin وعندما تنهار. في الواقع، لقد زاد عدد أصحاب الملايين لدينا خلال انهيار السعر عام 2018 عن عددهم في عام 2017 عندما ارتفع السعر!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="getStarted" id="startNow">
            <div class="container">
                <h1 class="sectionHeadline text-center">ابدأ حياتك الجديدة الآن</h1>
                <div class="row">
                    <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4">
                        <div class="registerForm registerFormHome">
                            <h3>غيّر حياتك اليوم!</h3>
                            <div class="regform">
                                <div class="component-form-registration" id="lnemwsyk-registrationform">
                                    <!-- <div class="stepIndicator">
                                        <div class="li-1 stepBadge active"><span class="hide"><i class="fas fa-check"></i></span><span class="">1</span></div>
                                        <div class="li-2 stepBadge"><span class="hide"><i class="fas fa-check"></i></span><span class="">2</span></div>
                                        <div class="li-3 stepBadge"><span class="hide"><i class="fas fa-check"></i></span><span class="">3</span></div>
                                    </div> -->
                                    
                                    <div id="gaff">
                                        
                                        <input class="brokerName" type="hidden" value="FXVCAU">
                                        <div class="model-msg-wrapper">
                                            <div class="model-msg">
                                                <div class="model-msg__close"></div>
                                                <div class="model-msg__response"></div>
                                            </div>
                                        </div>
                                         
                                        <iframe src="https://staging.adsguide.info/form/formIframe?<?php echo(urldecode($_SERVER['QUERY_STRING']));?>" style="width: 100%;height: 100%;overflow: hidden; opacity: 0;" frameborder="0" scrolling="no" class="formIframe" onload="setIframeHeight(this)"></iframe>

                                        <div class="wait">
                                            <div class="loader"></div>
                                        </div>
                                    </div>&nbsp;
                                    <!-- <button class="submit-btn step2">AVANTI</button>
                                    <button class="submit-btn step3" style="display:none;">AVANTI</button> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <?php //echo GetFooter("AR"); ?>
    </div>
    <script src="/src/plugins.js"></script>
    <script src="./index_files/1FUVFSTIR5TP.js"></script>
<!--     <script src="./index_files/ZLYPKBQXN48G.js"></script>
 -->    <script>
        var globalCountry = "<?=$userDetails["countryCode"];?>";
        var globalLocale = "ar";
        var globalCurrency = '[i18n] currencyModal';
        var globalProject = "btccompass";
    </script>
    <script>
        var translations = {
            ivideo: {
                'overlay_text': 'انقر لتشاهد وتتعلم السر!'
            },
            registrationForm: {
                "firstname": 'الاسم الأول',
                "lastname": 'الاسم الأخير',
                "email": 'البريد الإلكتروني',
                "agreement": 'أوافق علي تقديم عنوان بريدي الإلكتروني لأغراض تلقي العروض التجارية التي نعتقد أنها قد تهمك بالنيابة عن الشركات والصناعات الموضحة تفصيلاً في \x3ca target=\x22_blank\x22 href=\x22#privacy\x22\x3eسياسة الخصوصية\x3c\/a\x3e لدينا.',
                "agreement_note_short": 'بياناتك الخاصة محمية دائماً معنا. \x3cbr\x3e يمكنك تغيير رأيك في أي وقت من خلال...  \x3cbr\x3e\x3cspan class=\x22tooltiplink\x22\x3eالمزيد \x3cspan class=\x22tooltiptext\x22\x3eيمكنك تغيير رأيك في أي وقت من خلال النقر على رابط إلغاء الاشتراك في الهامش السفلي لأي رسالة إلكترونية تتلقاها منا. سنتعامل مع معلوماتك باحترام. بالنقر أعلاه، فإنك توافق على أننا قد نقوم بمعالجة معلوماتك وفقاً لهذه الشروط.\x3c\/span\x3e\x3c\/span\x3e',
                "terms": 'أوافق على \x3ca target=\x22_blank\x22 href=\x22\/legal\/terms.html\x22\x3eالشروط والأحكام\x3c\/a\x3e و \x3ca target=\x22_blank\x22 href=\x22\/legal\/privacy-policy.html\x22\x3eسياسة الخصوصية\x3c\/a\x3e',
                "next": 'التالي',
                "submit": 'احصل على تصريح دخول الآن',
                "phone": 'الهاتف',
                "password": 'كلمة المرور',
                'close': 'إغلاق',
                'robotMainTitle': 'تهانينا!',
                'robotMsg': 'أنت الآن مسجل لدى وسيطنا المؤتمن.',
                'robotSecondTitle': 'عائد مرتفع',
                'robotSecondMsg': 'سهولة البدء',
                'brandMsg': 'مع \x3cb\x3ebrandName\x3c\/b\x3e يمكنك الحصول على متوسط عائد 88% على كل عملية تداول ناجحة.',
                'ftdInfo': 'للبدء، قم بإيداع مبدئي قدره 250 دولار أمريكي وسيمكنك البدء في التداول.',
                'robotSubmit': 'انقر هنا للبدء في التداول',
                "invalidPassword": '[i18n] invalidPassword'
            }
        };
    </script>

    </div>
    
    <script>
        $( '.step2' ).click(function() {
       $('#gaff > form > div:nth-child(10)').hide();
       $('#gaff > form > div:nth-child(12) > div:nth-child(1)').hide();
       $('.step2').hide();
       $('.li-1').removeClass('active').addClass('done');
       $('.li-1').html('<span class=""><i class="fas fa-check"></i></span><span class="hide">1</span>');
       $('.li-2').addClass('active');
       $('#gaff > form > div:nth-child(12) > div:nth-child(2)').show();
       $('.step3').show();
    });
    
    $( '.step3').click(function() {
       $('#gaff > form > div:nth-child(12) > div:nth-child(2)').hide();
       $('.step3').hide();
       $('.li-2').removeClass('active').addClass('done');
       $('.li-2').html('<span class=""><i class="fas fa-check"></i></span><span class="hide">1</span>');
       $('.li-3').addClass('active');
       $('#gaff > form > div:nth-child(11) > div:nth-child(2)').show();
       $('#gaff > form > button').show();
    });
</script>


</body>

</html>