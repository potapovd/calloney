var locationPath = locationPath ? locationPath : '';

$(document).ready(function() {

    function getDateTime(sec) {
        var now = new Date(Date.now() - (sec * 1000));
        var year = now.getFullYear();
        var month = now.getMonth() + 1;
        var day = now.getDate();
        var hour = now.getHours();
        var minute = now.getMinutes();
        var second = now.getSeconds();
        if (month.toString().length == 1) {
            month = '0' + month;
        }
        if (day.toString().length == 1) {
            day = '0' + day;
        }
        if (hour.toString().length == 1) {
            hour = '0' + hour;
        }
        if (minute.toString().length == 1) {
            minute = '0' + minute;
        }
        if (second.toString().length == 1) {
            second = '0' + second;
        }
        var dateTime = year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;
        return dateTime;
    }
    function rand_num(min, max) {
        var number = min + Math.floor(Math.random() * max);
        return number;
    }
  function createTableExpireDate(selector){
    var time_interval = 0;
    this.selector = selector;
      $(this.selector).each(function (i,e) {
          time_interval += rand_num(300, 500);
          var newDateTable = getDateTime(time_interval + rand_num(60, 180));
          $(e).html(newDateTable);
      });
  }
  createTableExpireDate('#tab--203 .expire-date');
  createTableExpireDate('#tab--209 .expire-date');
  createTableExpireDate('#tab--190 .expire-date');
  createTableExpireDate('#tab--192 .expire-date');


  var date = new Date();

  function GetFormattedDate(data) {
      var day = ('0' + data.getDate()).slice(-2),
        month = ('0' + (data.getMonth() + 1)).slice(-2),
        year = ('0' + data.getFullYear()).slice(-2),
        minutes = ('0' + data.getMinutes()).slice(-2),
        sec = ('0' + data.getSeconds()).slice(-2),
        hours = ('0' + data.getHours()).slice(-2);
        return(hours + ':' + minutes + ':' + sec + '  ' + day + '/' + month + '/' +  year)
  }
  $("#live-accounts-date").html(GetFormattedDate(date));
  // EXIT POPUP
  var exitMessageDisplayed = false;
  $(document).mousemove(function(e) {
      if(e.pageY <= 5)
      {
          if(! exitMessageDisplayed){
              $('#onExitModal').modal('show');
              exitMessageDisplayed = true;
          }
      }
  });
  modalPopup();

  if(locationPath) {
      profitCarousel();
      var waypoint = new Waypoint({
          element: document.getElementById('waypoint-starter-1'),
          offset:'80%',
          handler: function(d) {
              setTimeout(startProve,500);
              waypoint.destroy();
          }
      })
  } else if(!locationPath) {
      var clock1 = $('#counter-1').FlipClock(10 * 60, {
          countdown: true,
          language: 'arabic'
      });
      var clock2 = $('#counter-2').FlipClock(10 * 60, {
          countdown: true,
          language: 'arabic'
      });
  }
  $('.counter-date').html(getCurDate());



  $('.counter-date').html(getCurDate());
  //positionsLeft =  readCookie('positionsLeft');
  positionsLeft =  20;
  if(!positionsLeft) positionsLeft = 20;

   var clock = $('#counter-container').FlipClock(positionsLeft, {
    countdown: true,
    clockFace: 'Counter',
    language: 'arabic'
   });


   setTimeout(flipCounter,5 * 1000);
   function flipCounter() {
     
     val = clock.time.getSeconds(true);
     if(val) document.cookie = "positionsLeft=" + val;
     $('#positions-left').text(val);
     delay = getRandomInRange(4,50);
     if(val < 11){
      clock.$el.addClass('lowerthen10');
     }
     if(val <= 6){
      clock.decrement();
      clock.$el.addClass('lowerthen6');
      return;
     }
     if(delay < 20) {
      setTimeout(flipCounter,1500);
      clock.decrement();
      return;
     }
     clock.decrement();
     setTimeout(flipCounter,delay * 1000);
   }


});

$(document).ready(function () {
  modalPopup();
});
function modalPopup(){
  var popup = $("#modal");

  popup.on('show.bs.modal', function(event){
    var button = $(event.relatedTarget);
    var content = button.data('content');
    var modal = $(this);
    var text = $(modal).find('.modal-body');

    text.load(content, function(e){});
  });

  popup.on('shown.bs.modal', function () {
    $(this).removeData('bs.modal');
  });
}


function startProve(){
  var browser = $('.checkout__list .browser');
  var country = $('.checkout__list .country');
  var currency = $('.checkout__list .currency');
  var system = $('.checkout__list .system');

  browser.find('.result').append($.client.browser);
  country.find('.result').append(userCountry);
    //country.find('.result').append($('<img>').attr('src',));

    var currentCode = '';
    $("#country > option").each(function() {
        if($(this).text() == userCountry){
            currentCode = $(this).data('currency')
        }
    });
    switch(currentCode) {
        case 'USD':
            currentCode = 'Dollar';
            break;
        case 'EUR':
            currentCode = 'Euro';
            break;
    }
    currency.find('.result').append(currentCode);
    system.find('.result').append($.client.os);
    checker(browser,function(){return true});
    checker(country,function(){return true});
    checker(currency,function(){return true});
    checker(system,function(){return true});


    setTimeout(function(){$('#checkout-before').fadeOut(500)},3600);
    setTimeout(function(){$('#checkout-after').fadeIn(500)},4600);

  function checker(el,checkFunction){
    var transitionTime = getRandomInRange(1500,2500);
    el.find('.progress-bar').css('transition-duration',transitionTime / 1000 + 's','-webkit-transition-duration',transitionTime / 1000 + 's','-ms-transition-duration',transitionTime / 1000 + 's').css('width','100%');
    setTimeout(function(){el.find('.progress').fadeOut(300,function(){
      el.find('.result').fadeIn(300, function(){
        if(checkFunction()){
          el.addClass('success');
        }
        else{
          el.addClass('error');
        }
      });
    })},transitionTime + 400);
  }
}
function profitCarousel(){
    var tradersMax = 3;
    var traders = [{name:'أكرم', img: '1.jpg',country:'SA',rating: 'junior'},
        {name:'دلال', img: '2.jpg',country:'SA',rating: 'pro'},
        {name:'علاء الدين', img: '3.jpg',country:'SA',rating: 'pro'},
        {name:'أمجد', img: '4.jpg',country:'AE',rating: 'pro'},
        {name:'أنس', img: '5.jpg',country:'AE',rating: 'senior'},
        {name:'فارس', img: '6.jpg',country:'KW',rating: 'junior'},
        {name:'محمّد', img: '7.jpg',country:'KW',rating: 'senior'},
        {name:'عبد العزيز', img: '8.jpg',country:'QA',rating: 'pro'},
        {name:'رشيد', img: '9.jpg',country:'QA',rating: 'senior'}

    ];
    var actions =[
        {caption:'سَحَب ',header:'أجري سحب',value:'5,000'},
        {caption:'سَحَب ',header:'أجري سحب',value:'230'},
        {caption:'سَحَب ',header:'أجري سحب',value:'1,300'},
        {caption:'سَحَب ',header:'أجري سحب',value:'200'},
        {caption:'سَحَب ',header:'أجري سحب',value:'700'},
        {caption:'سَحَب ',header:'أجري سحب',value:'950'},
        {caption:'سَحَب ',header:'أجري سحب',value:'1,000'},
        {caption:'سَحَب ',header:'أجري سحب',value:'2,500'},
        {caption:'سَحَب ',header:'أجري سحب',value:'10,000'},
        {caption:'سَحَب ',header:'أجري سحب',value:'3,700'},
        {caption:'سَحَب ',header:'أجري سحب',value:'450'},
        {caption:'أجرى تداوَل على USD/EUR',header:'ربح الان',value:'530.30'},
        {caption:'أجرى تداوَل على USD/GBP ',header:'ربح الان',value:'380.20'},
        {caption:'أجرى تداوَل على SILVER',header:'ربح الان',value:'217.50'},
        {caption:'أجرى تداوَل على OIL',header:'خسر الان',value:'78'},
        {caption:'أجرى تداوَل على GBP/JPY',header:'ربح الان',value:'790.50'},
        {caption:'أجرى تداوَل على USD/JPY',header:'ربح الان',value:'260'},
        {caption:'أجرى تداوَل على USD/CAD',header:'خسر الان',value:'17'},
        {caption:'أجرى تداوَل على USD/EUR',header:'ربح الان',value:'570.20'},
        {caption:'أجرى تداوَل على USD/GBP ',header:'ربح الان',value:'380'},
        {caption:'أجرى تداوَل على SILVER',header:'ربح الان',value:'217.50'},
        {caption:'أجرى تداوَل على OIL',header:'ربح الان',value:'75'},
        {caption:'أجرى تداوَل على GBP/JPY',header:'ربح الان',value:'766.37'},
        {caption:'أجرى تداوَل على USD/JPY',header:'ربح الان',value:'26'},
        {caption:'أجرى تداوَل على USD/CAD',header:'ربح الان',value:'19'},
        {caption:'أجرى تداوَل على GOLD',header:'ربح الان',value:'320.45'},
        {caption:'أجرى تداوَل على USD/EUR',header:'ربح الان',value:'1,250'},
        {caption:'أجرى تداوَل على GOOGLE',header:'ربح الان',value:'226.30'},
        {caption:'أجرى تداوَل على AUD/JPY',header:'ربح الان',value:'118'},
        {caption:'أجرى تداوَل على USD/EUR',header:'خسر الان',value:'109.15'},
        {caption:'أجرى تداوَل على APPLE ',header:'خسر الان',value:'150'},
        {caption:'أجرى تداوَل على USD/EUR',header:'خسر الان',value:'200'},
        {caption:'أجرى تداوَل على AUD/NZD ',header:'خسر الان',value:'25.15'},
        {caption:'أجرى تداوَل على USD/EUR',header:'خسر الان',value:'105.10'},
        {caption:'أجرى تداوَل على USD/CHF ',header:'خسر الان',value:'50.15'}

    ]
    var accets = ['USD/EUR','USD/GBP','SILVER','OIL','GBP/JPY','USD/JPY','USD/CAD ','GOLD','USD/EUR','USD/EUR','USD/EUR','GOOGLE','AUD/JPY','APPLE','AUD/NZD','GBP/JPY','USD/CHF'];

    function addTrader(){
        var action = actions[getRandomInRange(0,34)];
        var trader = traders[getRandomInRange(0,8)];

        if($('.traders__list .traders__item').length >= tradersMax){
            var last = $('.traders__list .traders__item').last();
            last.hide(350,function(){
                $(this).remove();

            });
        }
        var minAgo = ' دقائق'+getRandomInRange(2,10) + ' قبل ';
        var el = $('#trader').clone();
        el.attr('id','');
        el.find('.ava').attr('src',locationPath + '/images/' + trader.img);
        el.find('.name').html(trader.name + ' <img src=' + locationPath +'/images/' + trader.country + '.png>');
        el.find('.caption').html(action.caption);
        el.find('.min-ago').html(minAgo);
        el.find('.trade-result').text(action.header);
        el.find('.trade-result-sum').text('$' + action.value);
        el.find('.star-rating').addClass(trader.rating);
        $('.traders__list').prepend(el);
        setInterval(function(){el.show(400)},200);
    }
    addTrader();
    addTrader();
    addTrader();
    setInterval(addTrader,getRandomInRange(6000,10000));



    $('#waiting-people').text(getRandomInRange(210, 612));
    setInterval(updateWaitingPeople,5000);


}


function getCurDate(){
  var now = new Date();
  var curYear = now.getFullYear();
  var curMonth = now.getMonth() + 1;
  var curDay = now.getDate();
  if(curDay < 10) curDay = '0' + curDay;
  if(curMonth < 10) curMonth = '0' + curMonth;
  return curDay + '.' + curMonth + '.' + curYear;
}
function getRandomInRange(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function updateWaitingPeople(){
    console.log("upd")
  var min = 210;
  var max = 612;
  var cur = $('#waiting-people').text();
  if(getRandomInRange(1,2) == 1) cur = +cur + getRandomInRange(1,3);
  else cur = +cur - getRandomInRange(1,3)
  if((cur >= min) && (cur <= max)) $('#waiting-people').text(cur);
}