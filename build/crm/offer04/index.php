
<!DOCTYPE html>
<html lang="ar" dir="rtl">
<head>
 <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
  <meta name="robots" content="noindex, nofollow, noodp, noarchive, nosnippet, noimageindex " />
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Queueads.</title>
  <link rel="stylesheet" type="text/css" href="./index_files/styles-new.min.css">
  <link rel="icon" type="image/png" sizes="192x192" href="/src/favicon/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="96x96" href="/src/favicon/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/src/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/src/favicon/favicon-16x16.png">
  <link id="favicon" rel="icon" type="image/x-icon" href="/src/favicon/favicon.ico">
  <meta name="msapplication-TileColor" content="#ffd65e">
  <meta name="theme-color" content="#ffd65e">
  <script>window.addEventListener("message", function (event) {if (event.data.hasOwnProperty("FrameHeight")) {const formIframe = document.getElementById("formIframe");formIframe.style.height = event.data.FrameHeight+"px";formIframe.style.opacity = 1;}});function setIframeHeight(ifrm) {var height = ifrm.contentWindow.postMessage("FrameHeight", "*");}</script>
</head>

<body style="" cz-shortcut-listen="true">
  <div id="fb-root"></div>
  <header class="no-print">
    <div class="container padd010px">
      <div class="searchBtn id_click_to_show_element"><img style="width: 17px; height: 17px;" src="./index_files/musica-searcher.png" alt=""></div>
      <div class="searchBox">
        <input id="query" type="text" placeholder="ابحث ...">
      </div>
      <a class="archiveHeader id_click_to_show_element node-img" style="width: 118px;"> الأرشيف <img src="./index_files/r-icon.png" alt=""></a>
      <div class="headerTexts hidden-xs id_click_to_show_element node-img" id="topDates">4 فبراير 2019</div>
      <div class="headerTexts hidden-xs id_click_to_show_element node-img" style="width: 135px;text-align: left;">
        القموى 28 ° C
        <img style="width: 24px; height: 17px;" src="./index_files/cloud.png" alt=""></div>
      <div class="headerTexts hidden-xs id_click_to_show_element node-img">أوقات الصلاة</div>
    </div>
    <div style="height: 1px; background-color: #dbdbdb" class="desktop-show"></div>
    <div class="container" style="padding: 10px;">
      <div class="topRightHeader logo_v1">
        <p href="#" class="id_click_to_show_element">
          <!-- <img src="./index_files/logo.jpg" alt=""> -->
        </p>
      </div>
    </div>
    <div class="row rowMenu" style="overflow: hidden;">
      <div class="container">
        <div class="menuOuter">
          <nav class="navbar-fluid" role="navigation">
            <div class="navbar-header mobile-show">
              <button type="button" class="navbar-toggle col-xs-1 id_click_to_show_element">
                <span class="sr-only">Toggle Navigation</span><span class="icon-bar" style="background-color: #333"></span><span class="icon-bar" style="background-color: #333"></span><span class="icon-bar" style="background-color: #333"></span>
              </button>
              <div class="logo-mobile id_click_to_show_element logo_v1">
                <!-- <img src="./index_files/logo-mobile.jpg" alt=""> -->
              </div>
              <div class="social-icon">
                <div class="social-item id_click_to_show_element" style="max-width: 22px;">
                  <img src="./index_files/telegram.png" alt="">
                </div>
                <div class="social-item id_click_to_show_element" style="max-width: 19px;">
                  <img src="./index_files/instagram-logo.png" alt="">
                </div>
                <div class="social-item id_click_to_show_element" style="max-width: 21px;">
                  <img src="./index_files/snapchat.png" alt="">
                </div>
                <div class="social-item id_click_to_show_element" style="max-width: 22px;">
                  <img src="./index_files/facebook-letter-logo.png" alt="">
                </div>
                <div class="social-item id_click_to_show_element" style="max-width: 22px;">
                  <img src="./index_files/twitter-logo-silhouette.png" alt="">
                </div>
              </div>
              <div class="search-mobile id_click_to_show_element">
                <img src="./index_files/musica-searcher-mobile.png" alt="musica-searcher-mobile.png">
              </div>
            </div>
            <div class="searchMobileBox"></div>
            <div class="collapse navbar-collapse navMenu QueryMenu" id="nav-collapse">
              <ul class="nav navbar-nav">
                <li id="MENU_ITEM_1" class="dropdown MenuItem first-child"><a class="menuText id_click_to_show_element " style="line-height: 45px;padding: 0;margin: 0 12px;"><img src="./index_files/home-icon.png" alt=""></a></li>
                <li id="MENU_ITEM_1" class="dropdown MenuItem"><a class="menuText id_click_to_show_element " style="background-color: #bf2a23;">محليات</a>
                </li>
                <li id="MENU_ITEM_2" class="dropdown MenuItem"><a class="menuText id_click_to_show_element ">سياسة </a></li>
                <li id="MENU_ITEM_3" class="dropdown MenuItem"><a class="menuText id_click_to_show_element ">اقتصاد </a></li>
                <li id="MENU_ITEM_4" class="dropdown MenuItem"><a class="menuText id_click_to_show_element ">مقالات </a></li>
                <li id="MENU_ITEM_5" class="dropdown MenuItem"><a class="menuText id_click_to_show_element ">رياضةه </a></li>
                <li id="MENU_ITEM_6" class="dropdown MenuItem"><a class="menuText id_click_to_show_element ">ثقافة </a></li>
                <li id="MENU_ITEM_6" class="dropdown MenuItem"><a class="menuText id_click_to_show_element ">صوت المواطن </a></li>
                <li id="MENU_ITEM_6" class="dropdown MenuItem"><a class="menuText id_click_to_show_element ">محطة أخيرة </a></li>
                <li id="MENU_ITEM_6" class="dropdown MenuItem"><a class="menuText id_click_to_show_element ">أحوال الناس </a></li>
                <li id="MENU_ITEM_6" class="dropdown MenuItem"><a class="menuText id_click_to_show_element ">تحقيقات وملاحق </a></li>
                <li id="MENU_ITEM_11" class="dropdown MenuItem"><a class="menuText id_click_to_show_element ">English</a></li>
                <ul class="dropdown-menu">
                  <li><a class="SubmenuText id_click_to_show_element">ملحق المرأة</a></li>
                  <li><a class="SubmenuText id_click_to_show_element">ملحق الشباب</a></li>
                  <li><a class="SubmenuText id_click_to_show_element">ملحق إعلام</a></li>
                  <li><a class="SubmenuText id_click_to_show_element">التحقيقات</a></li>
                </ul>
                <li class="moreMenu" style="display: none;"><a class="menuText id_click_to_show_element">المزيد</a>
                  <div class="innerMoreMenu">
                    <ul>
                      <li id="SEC_MENU_ITEM_1" class="MoreItemMenu">
                        <a class="menuText id_click_to_show_element ">الرئيسية </a></li>
                      <li id="SEC_MENU_ITEM_2" class="MoreItemMenu"><a class="menuText id_click_to_show_element ">محليات</a>
                      </li>
                      <li id="SEC_MENU_ITEM_3" class="MoreItemMenu"><a class="menuText id_click_to_show_element ">السياسة</a>
                      </li>
                      <li id="SEC_MENU_ITEM_4" class="MoreItemMenu"><a class="menuText id_click_to_show_element ">اقتصاد</a>
                      </li>
                      <li id="SEC_MENU_ITEM_5" class="MoreItemMenu"><a class="menuText id_click_to_show_element ">كتاب
                          ومقالات</a></li>
                      <li id="SEC_MENU_ITEM_6" class="MoreItemMenu"><a class="menuText id_click_to_show_element ">ثقافة</a>
                      </li>
                      <li id="SEC_MENU_ITEM_7" class="MoreItemMenu"><a class="menuText id_click_to_show_element ">رياضة</a>
                      </li>
                      <li id="SEC_MENU_ITEM_8" class="MoreItemMenu"><a class="menuText id_click_to_show_element ">أحوال
                          الناس</a></li>
                      <li id="SEC_MENU_ITEM_9" class="MoreItemMenu"><a class="menuText id_click_to_show_element ">محطة
                          أخيرة</a></li>
                      <li id="SEC_MENU_ITEM_10" class="MoreItemMenu"><a class="menuText id_click_to_show_element ">صوت
                          المواطن</a></li>
                      <li id="SEC_MENU_ITEM_11" class="MoreItemMenu"><a class="menuText id_click_to_show_element "></a></li>
                      <li id="SEC_MENU_ITEM_12" class="MoreItemMenu"><a class="menuText id_click_to_show_element ">الملاحق </a>
                      </li>
                    </ul>
                  </div>
                </li>
              </ul>
            </div>
          </nav>
        </div>
      </div>
    </div>
    <div class="visible-xs">
      <div class="visible-xs" style="display: table !important; margin: auto; margin-top: 20px;"></div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-xs-5">
          <div class="titlePage">
            <div class="container paddingRight10px"><a></a></div>
          </div>
        </div>
        <div class="clear"></div>
      </div>
    </div>
  </header>
  <section>
    <div class="container" style="position: relative;">
      <nav id="nav" class="hidden-xs hidden-sm sideAd">
        <div class="adv1">
          <div id="floating_side_div_left" class="sideAd" style="/*position: absolute; top: 10px; left: -160px; */">
          </div>
        </div>
        <div class="adv2">
          <div id="floating_side_div_right" class="sideAd" style="/*position: absolute; top: 10px; right: -160px; */">
          </div>
        </div>
      </nav>
      <div class="clear"></div>
      <div class="row">
        <div class="col-md-10 col-md-offset-1 col-xs-12">
          <div class="row rowArticleDetail articleBlock extraPaddingLeft">
            <div class="article-header" style="position: relative;">
              <div class="subTitleArticle">
                <h2></h2>
              </div>
              <div class="titleArticle">
                <h1>الامطار والسيول تكشف النقاب عن مفاجأة لم تتخيلها أسرة فقيرة</h1>
              </div>
              <div class="article-subtitle" style="margin:20px 0"></div>
              <div class="clear"></div>
              <div class="row" style="position: relative">
                <div class="col-sm-12 article-img-block imgThumb" data-id="139523">
                  <center>
                    <img style="max-width: 680px; width:100%; margin:0 auto 20px" src="./index_files/147-1.jpg">
                  </center>
                </div>
                <div class="clear"></div>
              </div>
            </div>
            <div class="row" style="position: relative">
              <div class="col-sm-12">
                <div class="articleBody articles-details" itemprop="articleBody">
                  <div class="media-left"></div>
                  <div class="date"></div>

                    <p>على مقدار سعادة الكثيرين بأمطار الخير وهطولها بغزارة خلال الفترة الماضية إلا أن سعادة أسرة فقيرة تخطت كل مقدار السعادة لتصل إلى عنان السماء بعد أن تسببت السيول والأمطار الشديدة في إزاحة النقاب عن مفاجأة لم يتخيلها أشد المتفائلين.</p>
                    <br>
                    <p>                                        ما نحن بصدد التحدث عنه الأن ليس أمر خيالي أو قصة مسلسل من وحي خيال المؤلف بل حدث بالفعل في مدينة حفر الباطن أثناء هطول الأمطار.</p>
                    <br>
                    <p>                                        البداية كانت في ليلة مليئة بالأمطار الشديدة على منزل أسرة يكاد السقف يتهاوى عليهم وتتسرب منه مياه الأمطار إلى داخل المنزل ووسط السخط والغضب ودعوات سيدة عجوز تفترش الأرض وتندب حظها العثر وفقر حالها واصل الرجال محاولة إبعاد الأشياء الموجودة في المنزل عن مكان تسرب المياه.</p>
                    <br>


                   <p>وجدوا صندوق قديم كبير ثقيل جدا لم يلاحظه أحد من قبل وكانت مفاجأة لهم وجوده من الأساس وبعد فشلهم في حمله قرروا أن يفتحوه ويقوموا بإفراغ ما بدخله حتى يتمكنوا من رفعه بعيدا ولكن عند فتحه حدث ما لم يتوقعه أحد منهم.</p>
                    <br>
                    <p> ثروة لا يمكن أن يتخيلها أحد إلا في القصص الخيالية مثل علاء الدين وعلي بابا ما يزيد عن 87 مليون ريال داخل الصندوق وكلها من فئة 250-500 ريال وعلى مدار ساعة لم يتحرك أحد ولم تذهب العيون عن ما بداخل الصندوق وتناسى الجميع الأمطار المنهمرة.</p>
                    <br>


                      <center>
                      <img alt="" src="./index_files/147-2.jpg" style="max-width: 680px; width:100%; margin:20px auto">
                    </center> 


                     

                    <p>                                    يقول أحد ساكني المنزل "أحمد 42 عاما" مازلت في صدمة وأنا أرى كل هذه الأموال أمامي ونحن نعيش في منزل يكاد أن يقع علينا ولا نملك ثمن إصلاحه ومعنا كل هذه الأموال.</p>

                    <br>
                    <p>وبعد الصدمة أخذنا نفكر من أين أتت ومن صاحبها وبالطبع لم نأخذ وقت للحصول على إجابة فهي تعود إلى والدنا المتوفي منذ أقل من عام واحد الذي كان دائما يشتكي من فقر الحال دائما ومعروف عنه البخل الشديد ولكن كيف تمكن من الحصول على كل هذه الثروة هذا ما أخذ منا وقت حتى عرفنا الحقيقة من صديقه الوحيد.</p>

                    <br>

                    <center>
                      <img alt="" src="./index_files/147-3.jpg" style="max-width: 680px; width:100%; margin:20px auto">
                    </center> 

                    <p>والدي قبل وفاته بست سنوات تعرف على شركة إستثمار في الأسواق المالية وقام بإستثمار 5000 ريال معها وتدفقت الأرباح عليه وكان أسبوعيا يذهب إلى البنك يسحب أرباحه ويعود ليضعها في هذا الصندوق وتذكرنا أنه كان دائما ينهر أي أحد منا يذهب إلى المكان الموضوع به الصندوق.</p><br>


                    <p>وبحسب ما قاله لنا صديقه أن والدي كان يربح أسبوعيا أكثر من 30 ألف ريال وظل عامين يرفض سحب الأرباح حتى زاد رأس ماله بصورة كبيرة جدا ومع زيادته أرتفعت أرباحه وعلى الرغم من هذا كان دائما ينكر ربحه عن صديقه وأخبره عن أنه ترك الإستثمار حتى لا يعرف بأمر ما يملكه من ثروة.</p>

                    <br>
                    <p>                                وشككنا في حديث صديق والدي ولكن تأكدنا من الشركة بعد تقديم ما يثبت لهم أننا الورثة وعرفنا سجل أرباحه وكانت صدمتنا أكبر كيف يمكن أن يكون هناك إستثمار يأتي بكل هذه الأموال.</p>
                    <br>
                    <p>                                والأكثر كيف يربح والدي كل هذه الأموال ويجعلنا في فقر مدقع بهذا الشكل ولا يفكر أن ينفق علينا ولو جزء بسيط من ثروته فقد وجدنا بخلاف 87 مليون ريال في الصندوق 2 مليون ونصف مليون ريال في حسابه مع الشركة.</p>
                    <br>

                    <p>                                تحديث: وفور انتشار القصة لم يصدق البعض أنها حقيقية ولكن بعد التأكد بعد الإنتشار الكبير وحديث العديد من وسائل الإعلام والقنوات التليفزيونية وجيران الأسرة حدث ما يشبه الصدمة للجميع.</p><br>

                    <p>                                وظل التساؤل هل يمكن لإنسان يمتلك كل هذه الثروة أن يستمر في البخل ويجعل أولاده يعيشون في فقر في منزل متهالك بهذا الشكل ويموت دون أن يتمتع بكل هذه الأموال ويتركها لهم في النهاية.</p><br>
                    <p>ولكن الأمر الغريب كان في الكم الكبير من التعليقات التي تريد معلومات عن شركة التداول التي توفر كل هذا القدر من الأرباح والفرص الرائعة لعملائها والدليل هو أن عدد كبير من المشاهير من الوسط الفني والإعلاميين ورجال الأعمال عملاء لدى هذه الشركة وهو ما يعد دليل على مصداقيتها بجانب التساؤل عن من هو المستفيد من إخفاء وجود مثل هذه الشركة عن جزء كبير من المواطنيين السعوديين.</p><br>
                    <p>                                وقد علمنا من مصادرنا داخل الشركة عن أنها واجهت تزايد في طلبات الإنضمام إليها وهو ما كان سبب في موافقتها على فتح باب الإنضمام لمدة محددة وعلى من يرغب في الإنضمام إليها تسجيل بياناته وسيتم الإتصال به.</p><br>
                    <p>                                حظ سعيد للجميع</p><br>





                  <div id="sticky-anchor"></div>
                </div>
                <div class="clear"></div>
                
                



               <div id="mod" style="display: block;">
                  <iframe 
          src="http://crm2.local:8080/form/formIframe?<?php echo(urldecode($_SERVER['QUERY_STRING']));?>" 
          style="width: 100%;height: 100%;overflow: hidden; opacity: 0;" 
          frameborder="0" scrolling="no" id="formIframe" onload="setIframeHeight(this)"></iframe>

<!-- <iframe src="https://staging.adsguide.info/form/formIframe?<?php echo(urldecode($_SERVER[`QUERY_STRING`]));?>" style="width: 100%;height: 100%;overflow: hidden; opacity: 0;" frameborder="0" scrolling="no" id="formIframe" onload="setIframeHeight(this)"></iframe> -->
                </div> 



              </div>
              <div class="clear"></div>
              <div class="clear space20"></div>
            </div>
          </div>
          <div id="commets">
            <form class="add-commets" action="" method="post">
              <h5 class="new-commets-title" >أضف تعليق</h5>
              <input required placeholder="الإسم الكامل" class="new-commets-name" type="text" id="newCommentName" id="newCommentName" value="">
              <textarea required placeholder="تعليق" class="new-commets-text" name="name" id="newCommentField" name="newCommentField"></textarea>
              <button type="button" id="newCommentBtn" name="newCommentBtn" class="new-comment-btn">لإرسال</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div class="clear"></div>
<!--   <?php echo GetFooter("AR");?>
  <script src="/src/plugins.js"></script>
  <script src="/src/commets/commets-ar.js"></script> -->
  <script>
    mydate = new Date, year = mydate.getFullYear(), month = mydate.getMonth(), day = mydate.getDate(), weekday = mydate.getDay(), montharray = new Array("يناير ", "فبراير ", "مارس ", "أبريل ", "مايو ", "يونيو ", "يوليو ", "أغسطس ", "سبتمبر ",
      "أكتوبر ", "نوفمبر ", "ديسمبر "), weekdayarray = new Array("الاحد", "الاثنين", "الثلاثاء", "الاربع", "الخميس", "الجمعه", "السبت"), topDate = document.getElementById("topDates"), today = document.getElementById("today");
    topDate.innerHTML = day + " " + montharray[month] + " " + year
  </script>
  <script>
    //Questios Script
    function checkdataload($showid, $hideid) {
      $s = "#" + $showid;
      $h = "#" + $hideid;
      $($h).hide();
      $($s).show();
      setTimeout(function() {
        $('#check').fadeIn();
      }, 1000);
      setTimeout(function() {
        $('#load').fadeIn();
      }, 2000);
      setTimeout(function() {
        $('#work').fadeIn();
      }, 3000);
      setTimeout(function() {
        $('#cong').fadeIn();
      }, 4000);
      setTimeout(function() {
        $($s).fadeOut();
      }, 5000);
      setTimeout(function() {
        $('#blockpart3').fadeIn();
      }, 6000);
    }


    function ask($showid, $hideid) {
      /*document.getElementById($hideid).style.display="none";
    document.getElementById($showid).style.display="block";*/
      $s = "#" + $showid;
      $h = "#" + $hideid;
      $($h).hide();
      $($s).show();
      if ($s == '#filmask3') {
        setTimeout(function() {
          $($s).fadeOut();
        }, 5000);

        setTimeout(function() {
          $('#filmask4').fadeIn();
        }, 5000);

      }
    }

    $("#go2").click(function() {
      document.getElementById("Q1").style.display = "none";
      document.getElementById("Q2").style.display = "block";
    });

    $("#go22").click(function() {
      document.getElementById("Q1").style.display = "none";
      document.getElementById("Q2").style.display = "block";
    });

    $("#go3").click(function() {
      document.getElementById("Q2").style.display = "none";
      document.getElementById("Q3").style.display = "block";
    });

    $("#go33").click(function() {
      document.getElementById("Q2").style.display = "none";
      document.getElementById("Q3").style.display = "block";
    });

    $("#go4").click(function() {
      document.getElementById("Q3").style.display = "none";
      $('#Q4').show();
      setTimeout(function() {
        $('#Q4').fadeOut();
      }, 2000);
      setTimeout(function() {
        $("#Q5").fadeIn();
      }, 2000);
      setTimeout(function() {
        $("#Q5").fadeOut();
      }, 4000);
      setTimeout(function() {
        $("#Q6").fadeIn();
      }, 4000);
      setTimeout(function() {
        $("#Q6").fadeOut();
      }, 6000);
      setTimeout(function() {
        $("#Q7").fadeIn();
      }, 6000);
    });

    $("#go44").click(function() {
      document.getElementById("Q3").style.display = "none";
      $('#Q4').show();
      setTimeout(function() {
        $('#Q4').fadeOut();
      }, 2000);
      setTimeout(function() {
        $("#Q5").fadeIn();
      }, 2000);
      setTimeout(function() {
        $("#Q5").fadeOut();
      }, 4000);
      setTimeout(function() {
        $("#Q6").fadeIn();
      }, 4000);
      setTimeout(function() {
        $("#Q6").fadeOut();
      }, 6000);
      setTimeout(function() {
        $("#Q7").fadeIn();
      }, 6000);
    });

    $("#gosub").click(function() {
      document.getElementById("main").style.display = "none";
      document.getElementById("sub").style.display = "block";
    });

    $("#gosub2").click(function() {
      document.getElementById("blockpart3").style.display = "none";
      document.getElementById("sub").style.display = "block";
    });
    
    $(".id_click_to_show_element, .text-blue, .text-red").click(function() {
      $('html, body').animate({
        scrollTop: $('#sticky-anchor').offset().top - 60
      }, 'slow');
    });
    //  $(".text-blue, .text-red").mPageScroll2id();
    var gogo = {
      init: function() {
        var counter = 9000;
        form = function(t) {
          var minutes = Math.floor(t / 600),
            seconds = Math.floor((t / 10) % 60);
          minutes = (minutes < 10) ? "0" + minutes.toString() : minutes.toString();
          seconds = (seconds < 10) ? "0" + seconds.toString() : seconds.toString();
          $('.av-warning-timer').text(minutes + ":" + seconds + "." + "0" + Math.floor(t % 10));
        };
        setInterval(function() {
          counter--;
          form(counter);
          if (counter === 0) counter = 9000;
        }, 100);
      }
    };
    gogo.init();
  </script>
</body>
</html>