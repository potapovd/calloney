<?php
include($_SERVER['DOCUMENT_ROOT'] . '/src/inc/forms/optsgen.php');
include($_SERVER['DOCUMENT_ROOT'] . '/stats/c.php');

?>
<!DOCTYPE html>
<html lang="ar" dir="rtl">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
    <meta name="robots" content="noindex, nofollow, noodp, noarchive, nosnippet, noimageindex	"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Queueads Dreams</title>

    <link rel="stylesheet" href="/src/style-fix.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="icon" type="image/png" sizes="192x192" href="/src/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/src/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/src/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/src/favicon/favicon-16x16.png">
    <link id="favicon" rel="icon" type="image/x-icon" href="/src/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#e6c300">
    <meta name="theme-color" content="#e6c300">
    <style>
        .register-box {
            background-color: rgba(51, 51, 51, 0.85);
            border: 1px solid #e6c300;
        }


        body .opt-in-form .btn-flat {
            background: #e6c300;
            color: #000;

        }

        body .register-box .termsblock a {
            color: #ccc !important;
        }

        .register-box.ar .has-feedback .selectcountry {
            padding: 6px 10px !important;
        }

        .register-box.ar .country-code-short-line img {
            display: none !important;

        }

        footer {
            width: 100%;
            background-color: rgba(51, 51, 51, 0.95);

        }

        footer a {
            color: #ccc !important;
        }

        footer #tnc {
            background: transparent !important;
        }

        .video--overlay {
            display: flex;
        }

        .media-dreamstest {
            max-width: 750px;
            width: 100%;
        }

        .mainVideoWrapper {
            border: 2px solid #fff;
        }

        .mainVideoWrapper iframe {
            border: none;
        }

        @media screen and (max-width: 575.98px) {
            body .mainVideoWrapper iframe {
                height: 100% !important;
            }
        }

        @media screen and (max-width: 768px) {

            .media-dreamstest,
            .elements-review-dreamstest {
                flex-direction: column-reverse;
                flex-wrap: wrap;
            }

            .media-dreamstest {
                align-items: center;
            }

            .form-wrapper-dreamstest {
                margin: 40px auto;
            }

            .mainVideoWrapper {
                margin: 0 20px;
            }

            /* .form-wrapper-dreamstest,
            .video-wrapper-dreamstest {
                width: 90%;
                margin: 0 auto 40px;
            } */
        }

    </style>
    <noscript>
        <style>
            .inner-box-main {
                display: none
            }
        </style>
    </noscript>
    <?php echo getPageCode(@$_GET['ref_id']); ?>

</head>

<body class="dreamstest-body">
<?php echo AfterHeader(@$_GET['ref_id']); ?>
<div class="wrap-dreamstest">
    <header class="header-dreamstest">
        <h1 class="caption-dreamstest">أحصل على التطبيق المجاني الذي سوف يجعل من احلامك
            حقيقة ويمكنك من ربح أكثر من - 3,715 ريال سعودي</h1>
        <div class="logo-dreamstest">
            <img src="img/logo.png" alt="logo">
        </div>
    </header>
    <main class="main-dreamstest">
        <div class="media-dreamstest">
            <div class="form-wrapper-dreamstest">

                <?php
                $fields = array("FullName", "Email", "Age", "CountryCodeShort", "PhoneShort", "UniteTermsAndIcon18Plus");
                echo FormsGenerator("AR", $fields, "", "", 51608018);
                ?>

            </div>
            <div class="video-wrapper-dreamstest video--overlay">

                <!-- <iframe width="100%" height="350" src="https://www.youtube.com/embed/QwdLLVJKPgw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen ></iframe> -->

                <div class="mainVideoWrapper" style="width:100%; height: auto; max-width: 545px">
                    <iframe src="index.mp4" allow="autoplay"></iframe>
                </div>
                <!-- <video  controls autoplay playsinline muted loop id="player" style="width:100%; height: auto; max-width: 545px">
                    <source src="index.mp4" type='video/mp4' />
                    Sorry! (
                </video>
                <div class="inner-box-main">
                      <div id="inner-box">
                          <p>تشغيل الفيديو</p>
                          <p class="buttonUnmute">
                              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496.16 496.15"><defs><style>.a{fill:#e04f5f;}.b{fill:#fff;}</style></defs><title>Unmute</title><path class="a" d="M496.16,248.09C496.16,111.06,385.09,0,248.08,0S0,111.06,0,248.09,111.07,496.16,248.08,496.16,496.16,385.09,496.16,248.09Z" transform="translate(0 0)"/><path class="b" d="M259.43,128.92a10.59,10.59,0,0,0-10.81.42l-95.14,61.79H118.32a10.59,10.59,0,0,0-10.59,10.59v92.72A10.59,10.59,0,0,0,118.32,305h35.16l95.14,61.79A10.59,10.59,0,0,0,265,357.94V138.22A10.59,10.59,0,0,0,259.43,128.92Z" transform="translate(0 0)"/><path class="b" d="M355.4,248.08l30.39-30.39A9,9,0,1,0,373.06,205l-30.38,30.38L312.29,205a9,9,0,1,0-12.72,12.72L330,248.08l-30.38,30.38a9,9,0,1,0,12.72,12.73l30.39-30.38,30.38,30.38a9,9,0,0,0,12.73-12.73Z" transform="translate(0 0)"/></svg>
                          </p>
                          <p>انقر لتشغيل الصوت</p>
                      </div>
                  </div> -->


            </div>
        </div>
        <div class="reviews-dreamstest">
            <div class="entry-dreamstest">
                <p>
                    تمتع بمستوى حياة أكثر رفاهية وجودة يومياً
                    الآلاف من الحاصلين على هذا التطبيق قاموا بتحقيق الكثير من احلامهم وأصبحوا من الاثرياء!
                    حان الوقت لكي تنضم أنت أيضاً الى هذه النخبة وتبدأ في كسب أكثر من 31000 ريال في
                    الشهر والتمتع بالاستقلالية المالية التي لطالما رغبت بها.
                </p>
            </div>
            <div class="elements-review-dreamstest">
                <div class="first-review-dreamstest">
                    <img src="img/1-1.png" alt="">
                    <p>
                        بندر مسعود. الرياض
                        بعد شهرين
                        , حققت أرباح أكثر من 52,000 ريال,
                        واشتريت لنفسي رولكس دايتونا
                    </p>
                    <span class="text_gold-dreamstest">
                            مجموع الأرباح
                            ريال 62,255.44
                        </span>
                </div>
                <div class="second-review-dreamstest">
                    <img src="img/2.png" alt="">
                    <p>
                        فيصل آل سعود. جدة
                        في سنة من التداول اشتريت هذه
                        LaFerrarii الأحمر
                    </p>
                    <span class="text_gold-dreamstest">
                            مجموع الأرباح
                            ريال 1,658,488.14
                        </span>
                </div>
                <div class="third-review-dreamstest">
                    <img src="img/3.png" alt="">
                    <p>
                        سالم راشد القحطاني
                        كانت أمنيتى دائما أن أذهب للحج.
                        بعد 20 يوم من التداول, وفعلت ذلك
                    </p>
                    <span class="text_gold-dreamstest">
                            مجموع الأرباح
                            ريال 9,271.47
                        </span>
                </div>
            </div>
        </div>
    </main>
    <?php echo GetFooter("AR"); ?>

</div>
<script src="/src/plugins.js"></script>
<!--     <script src="//crm.adsguide.info/dist/optindata/js/optin/ar/optingen.min.js"></script>
-->
<script>


    $(document).ready(function () {

        var allParams = getUrlVars();
        if (allParams.t == 0) {
            var terms = false;
        } else {
            var terms = true;
        }


        // $('#opt-in-form').initOpt({
        //     inputFirstName: {
        //         display: true,
        //         placeholder: 'الاسم الشخصي'
        //     },
        //     inputLastName: {
        //         display: true,
        //         placeholder: 'اسم العائله'
        //     },
        //     inputAge: {
        //         display: true,
        //         placeholder: 'العمر'
        //     },
        //     thankYouURL: 'https://queueads.com/ar/thank-you/',
        //     showTerms: terms
        // });

    });
</script>
</body>

</html>
