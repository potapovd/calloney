<?php
//include($_SERVER['DOCUMENT_ROOT'] . '/src/inc/forms/optsgen.php');
//include($_SERVER['DOCUMENT_ROOT'] . '/stats/c.php');

?>
<!DOCTYPE html>
<html lang="ar" dir="rtl">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
    <meta name="robots" content="noindex, nofollow, noodp, noarchive, nosnippet, noimageindex	"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Queueads Dreams</title>
    <link rel="stylesheet" href="/src/style-fix.css">
    <link rel="icon" type="image/png" sizes="192x192" href="/src/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/src/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/src/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/src/favicon/favicon-16x16.png">
    <link id="favicon" rel="icon" type="image/x-icon" href="/src/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#e6c300">
    <meta name="theme-color" content="#e6c300">
    <style>.dreamstest-body{background:url(img/bg.jpg) center center no-repeat;direction:rtl;font-weight:300;margin:0;-webkit-background-size:cover;background-size:cover}.wrap-dreamstest{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center}.header-dreamstest{background-color:rgba(51,51,51,0.85);display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:reverse;-webkit-flex-direction:row-reverse;-ms-flex-direction:row-reverse;flex-direction:row-reverse;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;min-width:100%;min-height:150px;-webkit-flex-wrap:wrap;-ms-flex-wrap:wrap;flex-wrap:wrap}.caption-dreamstest{color:white;font-size:26px;width:42%;margin-right:30px}.main-dreamstest{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center}.media-dreamstest{-webkit-box-orient:horizontal;-webkit-box-direction:reverse;-webkit-flex-direction:row-reverse;-ms-flex-direction:row-reverse;flex-direction:row-reverse;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;margin:50px auto 20px!important}.form-wrapper-dreamstest{margin-right:20px;max-width:485px}.reviews-dreamstest{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;font-size:28px;background-color:rgba(51,51,51,0.85);color:#fff;text-align:center;border-top:1px solid #e6c300;border-bottom:1px solid #e6c300;margin-top:40px}.entry-dreamstest{margin:0;color:white;width:60%;line-height:1.1;font-size:28px;text-align:center}.elements-review-dreamstest{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;-webkit-flex-direction:row;-ms-flex-direction:row;flex-direction:row;width:60%;margin-bottom:50px;-webkit-justify-content:space-around;-ms-flex-pack:distribute;justify-content:space-around}.elements-review-dreamstest div>p{margin:10px}.first-review-dreamstest{padding-left:10px}.second-review-dreamstest{padding-left:10px}.text_gold-dreamstest{color:#e6c300}.footer__content-dreamstest{color:#fff;font-size:15px;text-align:center}@media(max-width:575.98px){.header-dreamstest{-webkit-box-orient:vertical;-webkit-box-direction:reverse;-webkit-flex-direction:column-reverse;-ms-flex-direction:column-reverse;flex-direction:column-reverse}.caption-dreamstest{font-size:22px;width:90%;margin-right:0;text-align:center}.media-dreamstest{-webkit-box-orient:vertical;-webkit-box-direction:reverse;-webkit-flex-direction:column-reverse;-ms-flex-direction:column-reverse;flex-direction:column-reverse;margin-top:30px;margin-bottom:0}.form-wrapper-dreamstest{margin-right:0}.video-wrapper-dreamstest{padding:30px 10px 0 10px;box-sizing:border-box}.video-wrapper-dreamstest iframe{height:100%}.entry-dreamstest{width:90%;font-size:24px;line-height:1.3}.reviews-dreamstest{-webkit-box-orient:vertical;-webkit-box-direction:normal;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column}.elements-review-dreamstest{-webkit-box-orient:vertical;-webkit-box-direction:normal;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;width:100%}.elements-review-dreamstest div{padding:10px}}.body-611{background-color:#dbdbdb}.content-wrapper-611{height:auto;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;border:0;width:100%;margin-top:0;padding:0;max-width:100%;background-color:#dbdbdb;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;-webkit-box-align:start;-webkit-align-items:flex-start;-ms-flex-align:start;align-items:flex-start;direction:rtl}.p-611{margin:5px;font-size:18px;line-height:2}.h3-611{margin:5px;font-size:28px}.body-777{background-color:#dbdbdb;direction:ltr}.content-wrapper-777{height:auto;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;border:0;width:100%;margin-top:0;margin-left:1.5%;padding:0;max-width:100%;background-color:#dbdbdb;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;-webkit-box-align:start;-webkit-align-items:flex-start;-ms-flex-align:start;align-items:flex-start;direction:ltr}.p-777{margin:5px;font-size:18px}.h3-777{margin:5px;font-size:24px}.register-box{background-color:rgba(51,51,51,0.85);border:1px solid #e6c300}body .opt-in-form .btn-flat{background:#e6c300;color:#000}body .register-box .termsblock a{color:#ccc!important}.register-box.ar .has-feedback .selectcountry{padding:6px 10px!important}.register-box.ar .country-code-short-line img{display:none!important}footer{width:100%;background-color:rgba(51,51,51,0.95)}footer a{color:#ccc!important}footer #tnc{background:transparent!important}.video--overlay{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex}.media-dreamstest{max-width:750px;width:100%}.mainVideoWrapper{border:2px solid #e6c300}.mainVideoWrapper iframe{border:0}@media screen and (max-width:575.98px){body .mainVideoWrapper iframe{height:100%!important}}@media screen and (max-width:768px){.media-dreamstest,.elements-review-dreamstest{-webkit-box-orient:vertical;-webkit-box-direction:reverse;-webkit-flex-direction:column-reverse;-ms-flex-direction:column-reverse;flex-direction:column-reverse;-webkit-flex-wrap:wrap;-ms-flex-wrap:wrap;flex-wrap:wrap}.media-dreamstest{-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center}.form-wrapper-dreamstest{margin:40px auto}.mainVideoWrapper{margin:0 auto}}</style>
    <noscript>
        <style>.inner-box-main{display:none}.mainVideoWrapper{padding-bottom:0!important}</style>
    </noscript>
    <?php //echo getPageCode(@$_GET['ref_id']); ?>
     <script>window.addEventListener("message", function (event) {if (event.data.hasOwnProperty("FrameHeight")) {const formIframe = document.querySelectorAll(".formIframe");for (var i = 0; i < formIframe.length; i++) {formIframe[i].style.height = event.data.FrameHeight+"px";formIframe[i].style.opacity = 1;}}});function setIframeHeight(ifrm) {var height = ifrm.contentWindow.postMessage("FrameHeight", "*");}</script>
</head>
<body class="dreamstest-body">
<?php //echo AfterHeader(@$_GET['ref_id']); ?>
<div class="wrap-dreamstest">
    <header class="header-dreamstest">
        <h1 class="caption-dreamstest">أحصل على التطبيق المجاني الذي سوف يجعل من احلامك
            حقيقة ويمكنك من ربح أكثر من - 3,715 ريال سعودي</h1>
        <div class="logo-dreamstest">
            <img src="img/logo.png" alt="logo">
        </div>
    </header>
    <main class="main-dreamstest">
        <div class="media-dreamstest">
            <div class="form-wrapper-dreamstest">
                 <iframe src="https://staging.adsguide.info/form/formIframe?<?php echo(urldecode($_SERVER['QUERY_STRING']));?>" style="width: 100%;height: 100%;overflow: hidden; opacity: 0;" frameborder="0" scrolling="no" class="formIframe" onload="setIframeHeight(this)"></iframe>
            </div>
            <div class="video-wrapper-dreamstest video--overlay">
                <!-- <iframe width="100%" height="350" src="https://www.youtube.com/embed/QwdLLVJKPgw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen ></iframe> -->
                <div class="mainVideoWrapper" style="width:100%;height:auto;max-width:485px">
                    <noscript>
                        <video controls autoplay muted loop id="player" style="width:100%;height:auto;position:relative;z-index:100" preload="metadata">
                            <source src="index.mp4" type='video/mp4'/>
                            Sorry! (
                        </video>
                    </noscript>
                    <div id="video">
                        <video id="video5" controls autoplay playsinline muted loop style="width:100%;height:auto;position:relative;z-index:100;display:none" preload="metadata">
                            <source type='video/mp4'/>
                            Sorry! (
                        </video>
                        <div class="inner-box-main" id="video5Unmute" style="display:none">
                            <div id="inner-box">
                                <p>تشغيل الفيديو</p>
                                <p class="buttonUnmute">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496.16 496.15"><defs><style>.a{fill:#e04f5f}.b{fill:#fff}</style></defs><title>Unmute</title><path class="a" d="M496.16,248.09C496.16,111.06,385.09,0,248.08,0S0,111.06,0,248.09,111.07,496.16,248.08,496.16,496.16,385.09,496.16,248.09Z" transform="translate(0 0)"/><path class="b" d="M259.43,128.92a10.59,10.59,0,0,0-10.81.42l-95.14,61.79H118.32a10.59,10.59,0,0,0-10.59,10.59v92.72A10.59,10.59,0,0,0,118.32,305h35.16l95.14,61.79A10.59,10.59,0,0,0,265,357.94V138.22A10.59,10.59,0,0,0,259.43,128.92Z" transform="translate(0 0)"/><path class="b" d="M355.4,248.08l30.39-30.39A9,9,0,1,0,373.06,205l-30.38,30.38L312.29,205a9,9,0,1,0-12.72,12.72L330,248.08l-30.38,30.38a9,9,0,1,0,12.72,12.73l30.39-30.38,30.38,30.38a9,9,0,0,0,12.73-12.73Z" transform="translate(0 0)"/></svg>
                                </p>
                                <p>انقر لتشغيل الصوت</p>
                            </div>
                        </div>
                        <iframe frameborder="0" id="videoI" src="index.mp4" allow="autoplay"></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div class="reviews-dreamstest">
            <div class="entry-dreamstest">
                <p>
                    تمتع بمستوى حياة أكثر رفاهية وجودة يومياً
                    الآلاف من الحاصلين على هذا التطبيق قاموا بتحقيق الكثير من احلامهم وأصبحوا من الاثرياء!
                    حان الوقت لكي تنضم أنت أيضاً الى هذه النخبة وتبدأ في كسب أكثر من 31000 ريال في
                    الشهر والتمتع بالاستقلالية المالية التي لطالما رغبت بها.
                </p>
            </div>
            <div class="elements-review-dreamstest">
                <div class="first-review-dreamstest">
                    <img src="img/1-1.png" alt="">
                    <p>
                        بندر مسعود. الرياض
                        بعد شهرين
                        , حققت أرباح أكثر من 52,000 ريال,
                        واشتريت لنفسي رولكس دايتونا
                    </p>
                    <span class="text_gold-dreamstest">
مجموع الأرباح
ريال 62,255.44
</span>
                </div>
                <div class="second-review-dreamstest">
                    <img src="img/2.png" alt="">
                    <p>
                        فيصل آل سعود. جدة
                        في سنة من التداول اشتريت هذه
                        LaFerrarii الأحمر
                    </p>
                    <span class="text_gold-dreamstest">
مجموع الأرباح
ريال 1,658,488.14
</span>
                </div>
                <div class="third-review-dreamstest">
                    <img src="img/3.png" alt="">
                    <p>
                        سالم راشد القحطاني
                        كانت أمنيتى دائما أن أذهب للحج.
                        بعد 20 يوم من التداول, وفعلت ذلك
                    </p>
                    <span class="text_gold-dreamstest">
مجموع الأرباح
ريال 9,271.47
</span>
                </div>
            </div>
        </div>
    </main>
    <?php //echo GetFooter("AR"); ?>
</div>
<script src="/src/plugins.js"></script>
</body>
</html>