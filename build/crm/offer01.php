<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
    <meta name="robots" content="noindex, nofollow, noodp, noarchive, nosnippet, noimageindex"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Offer</title>
    <!-- <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic"> -->
	<link rel="stylesheet" href="//cdn.rawgit.com/necolas/normalize.css/master/normalize.css">
	<link rel="stylesheet" href="//cdn.rawgit.com/milligram/milligram/master/dist/milligram.min.css">
	
	<style>
		.formdiv{
			width: 60%; 
			margin: 0 auto;
		}
		@media screen and (max-width: 769px) {
			.formdiv{
				width: 100%; 
			}
		}
	</style>
	<script>
  		window.addEventListener('message', function (event) {
			if (event.data.hasOwnProperty("FrameHeight")) { 
				const formIframe = document.getElementById("formIframe");
				formIframe.style.height = event.data.FrameHeight+"px";
				formIframe.style.opacity = 1;
			}
		});
		function setIframeHeight(ifrm) {
			var height = ifrm.contentWindow.postMessage("FrameHeight", "*");   
		}
	</script>
</head>
<body>

<div class="container">
  	<div class="row">
  		<div class="column">
  			<h1 style="text-align: center;">Offer 01</h1>
  		</div>
	</div>

	<div class="row">
		<div class="column">
			<div class="formdiv">

				<div class="text-center">
			
					<iframe 
					src="http://crm2.local:8080/form/formIframe?<?php echo(urldecode($_SERVER['QUERY_STRING']));?>" 
					style="width: 100%;height: 100%;overflow: hidden; opacity: 0;" 
					frameborder="0" scrolling="no" id="formIframe" onload="setIframeHeight(this)"></iframe>

				</div>


			</div>
		</div>
	</div>
</div>



</body>
</html>