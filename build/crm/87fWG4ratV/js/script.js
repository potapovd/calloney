(function ($) {
    $('.red-btn').click(function(event) {
        event.preventDefault();
    });


    document.body.oncontextmenu = function (e) {
        return false;
    };

    $(".openq2").click(function () {
        $("#tdwl-q1").hide();
        $("#tdwl-q2").show();
        $("#tdwl-q2").addClass('flex');
    });

    $(".openq3").click(function () {
        $("#tdwl-q2").hide();
        $("#tdwl-q3").show();
        $("#tdwl-q3").addClass('flex');
    });

    $(".openq4").click(function () {
        $("#tdwl-q3").hide();
        $("#tdwl-q4").show();
        $("#tdwl-q4").addClass('flex');
        setTimeout(function () {
            $('.step1').hide();
            $('.step2').show();
            $(".step2").addClass('flex');
        }, 1500);
        setTimeout(function () {
            $('.step2').hide();
            $('.step3').show();
            $(".step3").addClass('flex');
        }, 2500);
        setTimeout(function () {
            $('.step3').hide();
            $('.step4').show();
            $(".step4").addClass('flex');
        }, 3500);
        setTimeout(function () {
            $('.step4').hide();
            $('.step5').show();
            $(".step5").addClass('flex');
        }, 4500);
    });

    $(".openq5").click(function () {
        $("#tdwl-q4").hide();
        $("#tdwl-q5").show();
    });

    sec = 30;
    min = 2;
    $(".s-sec").text(sec);
    $(".s-min").text(min);
    i = setInterval(function () {
        --sec;

        if (sec === 1) {
            if (min === 0) {
                min = 6;
            }
            sec = 59;
            --min;
        }
        $(".s-sec").text(sec);
        $(".s-min").text(min);
    }, 1000);

    snum = 59;
    $(".s-snum").text(snum);
    i = setInterval(function () {
        --snum;

        if (snum === 1) {
            snum = 59;
        }
        $(".s-snum").text(snum);
    }, 2500);

    ssec = 30;
    mmin = 2;
    $(".s-ssec").text(ssec);
    $(".s-mmin").text(mmin);
    i = setInterval(function () {
        --ssec;

        if (ssec === 1) {
            if (mmin === 0) {
                mmin = 6;
            }
            ssec = 59;
            --mmin;
        }
        $(".s-ssec").text(ssec);
        $(".s-mmin").text(mmin);
    }, 1000);

    ssnum = 59;
    $(".s-ssnum").text(ssnum);
    i = setInterval(function () {
        --ssnum;

        if (ssnum === 1) {
            ssnum = 59;
        }
        $(".s-ssnum").text(ssnum);
    }, 2500);

}(jQuery));