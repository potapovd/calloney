<?php
//include( $_SERVER['DOCUMENT_ROOT'].'/src/inc/forms/optsgen.php' );
//include($_SERVER['DOCUMENT_ROOT'] . '/stats/c.php');

?>
<!DOCTYPE html>
<html lang="ar">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
  <meta name="robots" content="noindex, nofollow, noodp, noarchive, nosnippet, noimageindex	" />
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Queueads Successtenk</title>

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css">
    <link rel="stylesheet" href="/src/style-fix.css">

    <link rel="stylesheet" href="css/styles.css">
    <link rel="icon" type="image/png" sizes="192x192" href="/src/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/src/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/src/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/src/favicon/favicon-16x16.png">
    <link id="favicon" rel="icon" type="image/x-icon" href="/src/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#656464">
    <meta name="theme-color" content="#656464">
    <style>
        body {
			font-family: 'Droid Arabic Kufi' !important;
		}
    body .opt-in-form .btn-flat {
        background: #656464;
        color: #fff;
    }
    .register-box.ar .termsblock,
    .register-box.ar .termsblock a{
        color: #444 !important;
    }


     @media screen and (max-width: 576px){
        .form-questions .btns .red-btn{
            height: 70px;
        }
        .form-questions #tdwl-q5 p{
            margin: 10px 30px 10px 10px;
        }
    }
    </style>
      <?php //echo getPageCode(@$_GET['ref_id']); ?>

       <script>window.addEventListener("message", function (event) {if (event.data.hasOwnProperty("FrameHeight")) {const formIframe = document.querySelectorAll(".formIframe");for (var i = 0; i < formIframe.length; i++) {formIframe[i].style.height = event.data.FrameHeight+"px";formIframe[i].style.opacity = 1;}}});function setIframeHeight(ifrm) {var height = ifrm.contentWindow.postMessage("FrameHeight", "*");}</script>
</head>

<body class="body-successtenk">
  <?php //echo AfterHeader(@$_GET['ref_id']); ?>
    <header class="header-successtenk">
        <h1 class="thetitle">
            <a href="/ar/87fWG4ratV">
                إستثمرت 10 الاف ريال لتحسين حياة عائلتي !! واليوم أنا من الأثرياء
            </a>
        </h1>
    </header>
    <main class="main-successtenk">
        <div class="thethumb">
            <a href="/ar/87fWG4ratV">
                <picture class="main-img">
                    <img src="img/main.jpg" class="main-img">
                    <source srcset="img/main.webp" type="image/webp">
                </picture>
            </a>
        </div>
        <p>إستثمرت 10 الاف ريال لتحسين حياة عائلتي !! واليوم أنا من الأثرياء</p>
        <p>من الصعب أن تفكر في المستقبل من دون أن يخطر في بالك أنك قاصر مالياً ، وهذا التفكير غالباً ما يجهد النفس
            ويحطمها.</p>
        <p>هل سألت نفسك يوماً كيف يحقق الأثرياء مالهم ومن أين يأتون به ؟</p>
        <p>أمامك فرصة مدتها فرصة مدتها <span class="s-min text-red">1</span> دقائق <span class="s-sec text-red">18</span>
            ثواني&nbsp;أجب عن ثلاث أسئلة وأغتنم الفرصة لتغيير حياتك إلى الأبد.</p>
        <p>السر وراء إمتلاك 10% من سكان الأرض ثروات الـ 90% الآخرين أنهم عرفوا أسرار الأسواق المالية فهل أنت مستعد
            لمعرفتها أيضاً ؟ أجب عن الأسئلة التالية بشكل دقيق</p>
    </main>
    <noscript>
         <iframe src="https://staging.adsguide.info/form/formIframe?<?php echo(urldecode($_SERVER['QUERY_STRING']));?>" style="width: 100%;height: 100%;overflow: hidden; opacity: 0;" frameborder="0" scrolling="no" class="formIframe" onload="setIframeHeight(this)"></iframe>
    </noscript>

    <section class="form-questions">
        <div id="tdwl-q1">
            <p>الرجاء الإجابة على الأسئلة التالية بدقة، عدد العضويات المتاحة محدود</p>
            <div class="btns">
                <a class="red-btn openq2 goto" href="#tdwl-q2" onclick="return false">اضغط هنا اذا كنت ترغب بالتسجيل بالشركه</a>
            </div>
            <h3>هذه فرصه محدوده جدا . اذا اغلقت الصفحه فلن تجدها مره اخرى !</h3>
        </div>
        <div id="tdwl-q2">
            <p>الرجاء الإجابة على الأسئلة التالية بدقة، عدد العضويات المتاحة محدود</p>
            <h3>السؤال الثانى: ما الدافع لديك للبحث عن فرصة جديدة؟
            </h3>
            <div class="btns">
                <a class="red-btn openq3" href="#" onclick="return false">حياة أفضل لأسرتي</a>
                <a class="red-btn openq3" href="#" onclick="return false">دوافع شخصية</a>
            </div>
            <h3>هذه فرصه محدوده جدا . اذا اغلقت الصفحه فلن تجدها مره اخرى !</h3>
        </div>
        <div id="tdwl-q3">
            <p>الرجاء الإجابة على الأسئلة التالية بدقة، عدد العضويات المتاحة محدود</p>
            <h3>
                االسؤال الثالث: هذا العمل يتطلب ساعتين من وقتك ورأس مال 200$ ؟
            </h3>
            <div class="btns">
                <a class="red-btn openq4" href="#" onclick="return false">نعم، امتلك الوقت والمال</a>
                <a class="red-btn openq4" href="#" onclick="return false">لا</a>
            </div>
            <h3>هذه فرصه محدوده جدا . اذا اغلقت الصفحه فلن تجدها مره اخرى !</h3>
        </div>
        <div id="tdwl-q4">
            <div class="step1">
                <h3>
                    جاري التحقق من البيانات المرسلة
                </h3>
                <img alt="loading" src="img/loading.gif">
                <p>
                    مبرك، دولتك مطابقة للدول المسموح بها
                </p>
            </div>
            <div class="step2">
                <h3>
                    ججاري التحقق من الدولة
                </h3>
                <img alt="loading" src="img/loading.gif">
                <p>
                    تم إضافة معلومات الأيبي الخاصة بك لقائمة السماح
                </p>
            </div>
            <div class="step3">
                <h3>
                    جاري التأكيد من عدد الأماكن المتاحة
                </h3>
                <img alt="loading" src="img/loading.gif">
                <p>
                    الرجاء التحلي بالصبر لحين البحث في بياناتك
                </p>
            </div>
            <div class="step4">
                <h3>
                    جاري الإتصال بقاعدة البيانات
                </h3>
                <img alt="loading" src="img/loading.gif">
                <p>
                    الرجاء التركيز والدقة في الخطوة التالية
                </p>
            </div>
            <div class="step5">
                <h3>مبروك أنت مؤهل للإنتقال للخطوة التالية</h3>
                <p>
                    يوجد 5 أماكن شاغرة فقط تم إضافة معلومات الأيبي الخاصة بك لقائمة السماح الرجاء التحلي بالصبر لحين
                    البحث في بياناتك
                    الرجاء التركيز والدقة في الخطوة التالية
                </p>
                <p>
                    <b>
                        الخطوات التالية التي يجب عليك فعلها
                    </b>
                </p>
                <p>
                    أولاً: بعد الضغط على التسجيل الآن سيتعين عليك كتابة معلوماتك الشخصية حتى نتمكن من التواصل معك.
                    ثانياً: الرجاء
                    التأكد من كتابة معلوماتك ورقم هاتفك بشكل صحيح حتى لا تضيع الفرصة من يدك.
                </p>
                <div class="btns">
                    <a class="red-btn openq5" href="#" onclick="return false">“سجل الآن”</a>
                </div>
            </div>
        </div>
        <div id="tdwl-q5">
            <div class="form-container" style="text-align: center;  width:100; max-width:600px; margin:0 auto">
                <iframe src="https://staging.adsguide.info/form/formIframe?<?php echo(urldecode($_SERVER['QUERY_STRING']));?>" style="width: 100%;height: 100%;overflow: hidden; opacity: 0;" frameborder="0" scrolling="no" class="formIframe" onload="setIframeHeight(this)"></iframe>

            </div>
        </div>
    </section>

    <?php //echo GetFooter("AR");?>

    <!-- <script src='/src/jQuery/jquery-3.3.1.min.js'></script>
	<script  src='/src/fancybox/jquery.fancybox.min.js'></script> -->
    <script src="js/script.js" defer></script>
    <script src="/src/plugins.js"></script>
    <!--     <script src="//crm.adsguide.info/dist/optindata/js/optin/ar/optingen.min.js"></script>
 -->
    <script>
        WebFontConfig = {
			custom: {
				families: ['Droid Arabic Kufi', 'Droid Arabic Kufi Bold'],
			}
        };
        $(".goto").mPageScroll2id();
        $(document).ready(function () {

            // var allParams = getUrlVars();
            // if (allParams.t == 0) {
            //     var terms = false;
            // } else {
            //     var terms = true;
            // }


            // $('#opt-in-form').initOpt({
            //     inputFirstName: {
            //         display: true,
            //         placeholder: 'الاسم الشخصي'
            //     },
            //     inputLastName: {
            //         display: true,
            //         placeholder: 'اسم العائله'
            //     },
            //     inputAge: {
            //         display: true,
            //         placeholder: 'العمر'
            //     },
            //     thankYouURL: 'https://queueads.com/ar/thank-you/',
            //     showTerms: terms
            // });
        });
    </script>
</body>
<link rel="stylesheet" href="/src/fonts/font-Kufi.css">

</html>
