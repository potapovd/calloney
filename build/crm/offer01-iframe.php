<html>

<head>
	<meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>FormsGeneratorV2</title>

    <style>
        html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed,
figure, figcaption, footer, header, hgroup,
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
margin: 0;
padding: 0;
border: 0;
font-size: 100%;
font: inherit;
vertical-align: baseline;
}
/* HTML5 display-role reset for older browsers */
article, aside, details, figcaption, figure,
footer, header, hgroup, menu, nav, section {
display: block;
}
body {
line-height: 1;
}
ol, ul {
list-style: none;
}
blockquote, q {
quotes: none;
}
blockquote:before, blockquote:after,
q:before, q:after {
content: '';
content: none;
}
table {
border-collapse: collapse;
border-spacing: 0;
}
*,
::before,
::after {
background-repeat: no-repeat;
/* 1 */
box-sizing: border-box;
/* 2 */
}
/**
* 1. Add text decoration inheritance in all browsers (opinionated).
* 2. Add vertical alignment inheritance in all browsers (opinionated).
*/
::before,
::after {
text-decoration: inherit;
/* 1 */
vertical-align: inherit;
/* 2 */
}
/**
* 1. Use the default cursor in all browsers (opinionated).
* 2. Use the default user interface font in all browsers (opinionated).
* 3. Correct the line height in all browsers.
* 4. Use a 4-space tab width in all browsers (opinionated).
* 5. Prevent adjustments of font size after orientation changes in
* IE on Windows Phone and in iOS.
* 6. Breaks words to prevent overflow in all browsers (opinionated).
*/
html {
cursor: default;
/* 1 */
font-family:
system-ui,
/* macOS 10.11-10.12 */
-apple-system,
/* Windows 6+ */
Segoe UI,
/* Android 4+ */
Roboto,
/* Ubuntu 10.10+ */
Ubuntu,
/* Gnome 3+ */
Cantarell,
/* KDE Plasma 4+ */
Oxygen,
/* fallback */
sans-serif,
/* macOS emoji */
"Apple Color Emoji",
/* Windows emoji */
"Segoe UI Emoji",
/* Windows emoji */
"Segoe UI Symbol",
/* Linux emoji */
"Noto Color Emoji";
/* 2 */
line-height: 1.15;
/* 3 */
-moz-tab-size: 4;
/* 4 */
tab-size: 4;
/* 4 */
-ms-text-size-adjust: 100%;
/* 5 */
-webkit-text-size-adjust: 100%;
/* 5 */
word-break: break-word;
/* 6 */
}
body {
margin: 0;
}
::-moz-selection {
background-color: #b3d4fc;
/* 1 */
color: #000;
/* 1 */
text-shadow: none;
}
::selection {
background-color: #b3d4fc;
/* 1 */
color: #000;
/* 1 */
text-shadow: none;
}
img {
-ms-interpolation-mode: bicubic;
image-rendering: crisp-edges;
}
button,
input,
optgroup,
select,
textarea {
font-family: inherit;
font-size: 100%;
line-height: 1.15;
margin: 0;
}
button,
input {
overflow: visible;
}
button,
select {
text-transform: none;
}
a {
background-color: transparent;
}
*:focus {
outline: none !important;
}
button,
[type="button"],
[type="reset"],
[type="submit"] {
-webkit-appearance: button;
}
button::-moz-focus-inner,
[type="button"]::-moz-focus-inner,
[type="reset"]::-moz-focus-inner,
[type="submit"]::-moz-focus-inner {
border-style: none;
padding: 0;
}
button:-moz-focusring,
[type="button"]:-moz-focusring,
[type="reset"]:-moz-focusring,
[type="submit"]:-moz-focusring {
outline: 1px dotted ButtonText;
}
select {
text-transform: none;
}
textarea {
margin: 0;
/* 1 */
overflow: auto;
/* 2 */
resize: vertical;
/* 3 */
}
[type="checkbox"],
[type="radio"] {
padding: 0;
}
[type="search"] {
-webkit-appearance: textfield;
/* 1 */
outline-offset: -2px;
/* 2 */
}
::-webkit-inner-spin-button,
::-webkit-outer-spin-button {
height: auto;
}
::-webkit-input-placeholder {
color: inherit;
opacity: 0.54;
}
::-webkit-search-decoration {
-webkit-appearance: none;
}
::-webkit-file-upload-button {
-webkit-appearance: button;
/* 1 */
font: inherit;
/* 2 */
}
::-moz-focus-inner {
border-style: none;
padding: 0;
}
:-moz-focusring {
outline: 1px dotted ButtonText;
}
.clearfix::after {
display: block;
content: "";
clear: both;
}
audio,
canvas,
iframe,
img,
svg,
video {
vertical-align: middle;
}
/**
* Add the correct display in IE 9-.
*/
audio,
video {
display: inline-block;
}
/**
* Add the correct display in iOS 4-7.
*/
audio:not([controls]) {
display: none;
height: 0;
}
/**
* Remove the border on images inside links in IE 10-.
*/
img {
border-style: none;
}
/**
* Change the fill color to match the text color in all browsers (opinionated).
*/
svg {
fill: currentColor;
}
/**
* Hide the overflow in IE.
*/
svg:not(:root) {
overflow: hidden;
}
.register-box * {
box-sizing: border-box;
}
.register-box {
padding: 5%;
}
.register-box .has-feedback {
position: relative;
}
.register-box .user_avatar {
position: absolute;
left: 10px;
top: calc(50% - 10px);
width: 20px;
height: 20px;
}
.register-box.ar .user_avatar {
position: absolute;
left: auto;
right: 10px;
top: calc(50% - 10px);
}
.register-box .captha {
position: absolute;
left: 10px;
width: 20px;
height: 20px;
bottom: 7px;
}
.register-box.ar .captha {
position: absolute;
left: auto;
right: 8px;
}
.register-box .captcha-img {
display: block;
width: 100%;
max-width: 120px;
border-radius: 5px 5px 0 0 !important;
}
.register-box .captcha-input {
border-radius: 0 0 5px 5px !important;
}
.register-box .form-group {
margin-bottom: 15px;
text-align: left;
}
.register-box.ar {
direction: rtl;
}
.register-box.ar .form-group {
text-align: right
}
.register-box .has-feedback .form-control {
padding-left: 38px;
padding-right: 10px
}
.register-box.ar .has-feedback .form-control {
padding-right: 38px;
padding-left: 10px
}
/*.register-box.ar .user_avatar {
left: 15px !important;
}*/
.register-box.col-sg-12 {
/*width: 100%;*/
/*float: left;*/
position: relative;
/*min-height: 1px;*/
/*padding-left: 15px;
padding-right: 15px;*/
}
.register-box .form-control {
display: block;
width: 100%;
padding: 6px 12px;
background-color: #fff;
background-image: none;
border: 1px solid #ccc;
border-radius: 4px;
-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
-webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
-o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
}
/*.form-group {
margin-bottom: 15px;
}*/
.register-box .btn-block {
display: block;
width: 100%;
}
.register-box h1 {
font-size: 28px;
text-align: center;
margin: 10px 0 20px;
line-height: 32px;
}
.register-box .full-name-short-line {
margin-right: 5px;
}
.register-box .country-code-short-line,
.register-box .nationality-line {
direction: ltr;
}
.register-box .country-code-short-line img,
.register-box .age-short-line img {
display: none;
}
.register-box .age-short-line,
.register-box .country-code-short-line {
width: 60px;
/*display: inline-block;*/
float: left;
margin-right: 2px;
}
.register-box .phone-short-line,
.register-box .full-name-short-line {
/*display: inline-block;*/
float: left;
width: calc(100% - 62px);
}
.register-box.ar .has-feedback.age-short-line .form-control,
.register-box.ar .has-feedback .form-control.selectcountry {
padding-right: 10px !important;
}
.register-box.ar .has-feedback .form-control.selectcountry,
.register-box.ar .has-feedback .form-control.selectnationality{
direction:rtl;
padding-right: 38px;
}
body .register-box .has-feedback.country-code-short-line .form-control.selectcountry,
body .register-box .has-feedback.country-code-short-line .form-control.selectnationality {
padding-left: 10px !important;
}
@media screen and (max-width: 480px) {
.age-short-line,
.country-code-short-line {
width: 30%;
}
.phone-short-line,
.full-name-short-line {
width: 67.5%;
}
.age-short-line img {
display: none;
}
.register-box.ar .age-short-line .form-control,
.register-box.ar .has-feedback .form-control.selectcountry {
padding-right: 10px !important;
}
.register-box.ar .has-feedback .form-control.selectnationality {
padding-right: 38px !important;
}
}
.register-box .btn {
/* display: inline-block;*/
padding: 10px 12px;
margin-bottom: 0;
font-weight: normal;
text-align: center;
touch-action: manipulation;
cursor: pointer;
/*border: 1px solid transparent;*/
border: none;
border-radius: 4px;
text-transform: uppercase;
color: #fff;
background-image: linear-gradient(to bottom, #222222 0%, #45484d 100%);
border-radius: 4px;
/*transition: all 0.5s ease;*/
/* background-image: linear-gradient(#5187c4, #1c2f45);*/
background-size: auto 200%;
background-position: 0 100%;
transition: background-position 0.5s;
}
.register-box .btn:hover {
background-position: 0 0;
}
/*.register-box .btn:hover {
background: linear-gradient(to bottom, #45484d 0%, #222222 100%);
}*/
.register-box .termsblock a,
.register-box .gdrp a {
color: #45484d;
text-decoration: underline;
}
.register-box .termsblock a:hover {
color: #222;
text-decoration: none;
}
.register-box .termsblock {
line-height: 1.6em;
}
.register-box .pluslog,
.register-box.ar .pluslog {
max-width: 22px;
width: 100%;
margin-left: 8px;
}
.register-box .termsblock,
.register-box.ar .termsblock {
color: #ccc;
font-size: 12px;
margin-left: 20px;
}
.register-box .gdrp{
color: #ccc;
font-size: 12px;
line-height: 1em;
}
.register-box.ar .pluslog {
margin-left: auto;
margin-right: 8px;
}
.register-box.ar .termsblock {
margin-left: auto;
margin-right: 20px;
text-align: right;
}
.register-box.ar .full-name-short-line {
margin-right: 0px !important
}
.register-box.ar .age-short-line {
margin-right: 5px !important
}
.register-box .customBlock {
max-width: 530px;
height: auto;
max-width: 80%;
max-height: none;
}
#mod {
background-color: #ffd65e;
background-image: -webkit-gradient(linear, left top, left bottom, from(#ffd65e), to(#febf04));
background-image: -webkit-linear-gradient(top, #ffd65e, #febf04);
background-image: -moz-linear-gradient(top, #ffd65e, #febf04);
background-image: -ms-linear-gradient(top, #ffd65e, #febf04);
background-image: -o-linear-gradient(top, #ffd65e, #febf04);
background-image: linear-gradient(to bottom, #ffd65e, #febf04);
filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0, startColorstr=#ffd65e, endColorstr=#febf04);
box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
transition: all 0.3s cubic-bezier(.25, .8, .25, 1);
transition: all 0.5s;
border-radius: 20px;
margin: 0 auto;
max-width: 400px;
width: 100%;
}
#mod:hover {
background-color: #ffc92b;
background-image: -webkit-gradient(linear, left top, left bottom, from(#ffc92b), to(#ce9a01));
background-image: -webkit-linear-gradient(top, #ffc92b, #ce9a01);
background-image: -moz-linear-gradient(top, #ffc92b, #ce9a01);
background-image: -ms-linear-gradient(top, #ffc92b, #ce9a01);
background-image: -o-linear-gradient(top, #ffc92b, #ce9a01);
box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22);
background-image: linear-gradient(to bottom, #ffc92b, #ce9a01);
filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0, startColorstr=#ffc92b, endColorstr=#ce9a01);
}
#mod #form-reg .register-box-body .btn-flat {
background: #7d7e7d;
background: -moz-linear-gradient(top, #7d7e7d 0%, #0e0e0e 100%);
background: -webkit-linear-gradient(top, #7d7e7d 0%, #0e0e0e 100%);
background: linear-gradient(to bottom, #7d7e7d 0%, #0e0e0e 100%);
filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7d7e7d', endColorstr='#0e0e0e', GradientType=0);
transition: all 0.5s;
}
#mod #form-reg .register-box-body .btn-flat:hover {
background: #4c4c4c;
background: -moz-linear-gradient(top, #4c4c4c 0%, #0e0e0e 100%);
background: -webkit-linear-gradient(top, #4c4c4c 0%, #0e0e0e 100%);
background: linear-gradient(to bottom, #4c4c4c 0%, #0e0e0e 100%);
filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#4c4c4c', endColorstr='#0e0e0e', GradientType=0);
}
/*Start page-questions*/
.page-questions {
width: 100%;
text-align: center;
padding-top: 30px;
/* height: 100%; */
}
.page-questions h3 {
font-family: 'Droid Arabic Kufi', sans-serif;
color: #333;
margin-bottom: 20px;
font-size: 30px;
font-weight: bold;
}
.page-questions .normal {
color: #222;
font-size: 16px;
font-weight: bold;
line-height: 1.7;
}
.page-questions .normal strong {
font-size: 17px;
color: #000;
}
.page-questions #blockpart2,
.page-questions #blockpart3,
.page-questions #ask2,
.page-questions #ask3,
.page-questions #load,
.page-questions #work,
.page-questions #cong {
display: none;
}
.page-questions .asks p {
color: #333;
font-size: 25px;
font-weight: bold;
margin-bottom: 20px;
}
.page-questions .asks p span {
font-weight: normal;
}
.page-questions .asks a,
.page-questions .asks button,
.page-questions #gosub2 {
outline: 0;
color: #fff;
font-weight: bold;
height: 60px;
border: 0;
background-color: #cd9c2e;
border-radius: 5px;
text-align: center;
display: block;
text-decoration: none;
line-height: 60px;
font-size: 22px;
transition: .2s ease-in-out;
width: 300px;
margin: 0 auto;
margin-bottom: 20px;
}
.page-questions .asks a:hover,
.page-questions .asks button:hover {
background-color: #e4a10a;
}
.page-questions #check,
.page-questions .mab {
font-size: 17px;
font-weight: bold;
color: #bf0000;
}
.page-questions #load,
.page-questions #work,
.page-questions #cong {
font-size: 17px;
color: #000;
}
.page-questions .info {
font-size: 17px;
color: #636363;
}
.page-questions .info strong {
font-size: 19px;
}
#sub {
display: none;
}
/* OPT FORM FIX*/
/*
.login-box-body,
.register-box-body {
background: transparent;
}
[type="checkbox"],
[type="radio"] {
box-sizing: border-box;
padding: 0;
}
[type="number"]::-webkit-inner-spin-button,
[type="number"]::-webkit-outer-spin-button {
height: auto;
}
*:focus {
outline: none !important;
}
body .col-sg-12 center {
margin-bottom: 20px;
}
body .btn-primary,
body .form-group a {
transition: all 0.5s;
}
.modal-content {
width: 85%;
}*/
/* FORM GENERAL FIXING*/
/*
body input,
body button{
height: unset;
}
body #phone_required,
body .ntl-tel-input .error-msg {
text-align: left;
}
body .login-box,
body .register-box {
margin: 0 auto;
}
.opt-in-form .form-control {
padding-left: 55px !important;
padding-right: 10px ;
}
.opt-in-form #terms p {
font-size: 12px;
float: unset;
margin: 0px 0 22px 22px;
padding-left: 8px;
text-indent: 0;
}
body .opt-in-form a#reply,
body .opt-in-form a#replyprivacy {
padding: 0;
}
.opt-in-form .btn-primary {
color: #fff;
text-transform: uppercase;
}
.opt-in-form .login-box-body,
.opt-in-form .register-box-body {
background: transparent;
}
.opt-in-form .intl-tel-input {
width: 100%;
}
.opt-in-form .intl-tel-input .selected-flag .iti-flag {
position: absolute;
top: 0;
bottom: 0;
margin: auto;
left: 0;
right: 0;
}
.opt-in-form .user_avatar {
right: auto;
left: 15px;
}
.opt-in-form .col-sg-12 center,
.opt-in-form .form-group {
margin-bottom: 10px;
}
.opt-in-form .form-group {
text-align: left
}
.opt-in-form #pluslog {
margin-right: 10px;
padding-left: 4px;
}
.opt-in-form .intl-tel-input.allow-dropdown {
margin-bottom: 0;
}*/
/* FORM ARABIC FIXING*/
/*
body .opt-in-form.rtl .user_avatar {
left: auto !important;
right: 15px !important;
}
body .opt-in-form.rtl .country {
text-align: right;
}
body .opt-in-form.rtl .form-control {
padding-left: 10px !important;
}
.opt-in-form.rtl .dial-code {
margin-right: 5px;
}
.opt-in-form.rtl #phone_required,
.opt-in-form.rtl .ntl-tel-input .error-msg {
text-align: right;
}
body .opt-in-form.rtl .intl-tel-input.allow-dropdown .flag-container {
left: auto !important;
right: 0 !important;
}
.opt-in-form.rtl #terms p {
margin: 0px 32px 22px 0;
}
.opt-in-form.rtl .col-sg-12 .form-group {
text-align: right
}*/
/* FORM NOSCRIPT FIXING*/
.register-box input {
transition: all 1s;
border: 1px solid #fff;
}
.register-box input:focus:invalid {
border: 1px solid black;
}
.register-box input:focus:valid {
border: 1px solid green;
}
.register-box input:valid {
border: 1px solid green;
}
.register-box input:invalid:not(:focus):not(:placeholder-shown) {
background: pink;
border: 1px solid red;
}
.register-box .selectcountry,
.register-box .selectnationality {
-webkit-appearance: none;
-moz-appearance: none;
appearance: none;
}
.register-box .selectcountry,
.register-box .selectnationality {
padding-right: auto;
padding-left: 45px;
white-space: nowrap;
}
.register-box .customCheckbox {
width: 20px;
position: relative;
display: inline-block;
text-align: right;
float: left;
}
.register-box .customCheckbox label {
width: 20px;
height: 20px;
cursor: pointer;
position: absolute;
top: 0;
left: 0;
background: linear-gradient(to bottom, #222222 0%, #45484d 100%);
border-radius: 4px;
box-shadow: inset 0px 1px 1px rgba(0, 0, 0, 0.5), 0px 1px 0px rgba(255, 255, 255, 0.4);
}
.register-box .customCheckbox label:after {
content: '';
width: 10px;
height: 5px;
position: absolute;
top: 4px;
left: 4px;
border: 3px solid #fcfff4;
border-top: none;
border-right: none;
background: transparent;
opacity: 0;
-webkit-transform: rotate(-45deg);
transform: rotate(-45deg);
}
.register-box .customCheckbox input[type=checkbox] {
visibility: hidden;
}
.register-box .customCheckbox input[type=checkbox]:checked+label:after {
opacity: 1;
box-sizing: content-box;
}
.register-box.ar .customCheckbox {
width: 20px;
position: relative;
display: inline-block;
text-align: right;
float: right;
}
footer #tnc {
text-align: center;
background: #e4e4e4;
padding: 20px 10px 10px;
}
footer #tnc p {}
footer #tnc a {
display: inline-block;
text-align: center;
color: #45484d;
text-decoration: underline;
}
footer #tnc a:hover {
text-decoration: none;
color: #222;
}
footer .footer-logos,
footer .footer-logos-pay {
text-align: center;
margin: 0 auto;
}
footer .footer-logos img {
height: 90px;
width: auto;
margin: 20px 10px 10px
}
footer .footer-logos-pay img {
height: 30px;
text-align: center;
width: auto;
margin: 10px 10px 20px
}
footer .trad-resp{
font-size: 10px;
padding: 20px;
line-height: 1.1em;
color: #888;
}
footer .powered-by{
text-align: center;
margin: 10px 0
}
footer .powered-by img{
width: 30px;
}
footer .powered-by span{
color: #45484d;
position: relative;
/* top:-11px; */
line-height: 37px;
}
@media screen and (max-width:500px) {
footer .footer-logos img {
height: 60px;
}
footer .trad-resp{
font-size: 8px !important;
color: #999;
}
}
.video--overlay {
width: 100%;
position: relative;
}
.inner-box-main {
position: absolute;
width: 100%;
height: 100%;
top: 0;
left: 0;
background-color: rgba(0, 0, 0, .4);
cursor: pointer;
z-index: 999;
}
#inner-box {
display: flex;
flex-direction: column;
font-size: 16px;
height: 100%;
justify-content: center;
text-align: center;
}
#inner-box p {
color: #fff;
font-size: 20px;
font-weight: 500;
margin: 5px 0;
}
#inner-box p.buttonUnmute {
width: 100%;
text-align: center;
}
#inner-box p.buttonUnmute svg {
display: inline-block;
width: 64px;
height: 64px;
object-fit: cover;
margin: 0 auto;
}
#inner-box img {
display: inline-block;
width: 64px;
height: 64px;
object-fit: cover;
}

body .register-box {
opacity: 1 !important;
transition: all 1s;
}








    </style>
</head>

<body>
    <?php 
		//Get Client IP
		function getClientIP(){
			if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			  $ip = $_SERVER['HTTP_CLIENT_IP'];
			} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			  $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			} else {
			  $ip = $_SERVER['REMOTE_ADDR'];
			}
			return $ip;
		}
		//Get Client info by IP
		function getUserInfoByIP($ip) {
			$opts = array(
			  'http'=>array(
			    'method'=>"GET",
			    'header'=>"Accept-language: en\r\n" ,
			    'timeout' => 3,
			    'ignore_errors' => '1'
			  )
			);
			$context = stream_context_create($opts);
			$json = file_get_contents("http://pro.ip-api.com/json/$ip?key=7M9xU1gL9NRwmC4", false, $context);
			$details = json_decode($json, true);
			return $details;
		}
		$referrer = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; 
		$ipaddress = getClientIP();
		$userDetails = getUserInfoByIP($ipaddress);		

		//Get Client Language
		if ( isset($_GET['lang']) ) {
	        $lang = $_GET['lang'];
	    } else {
	        $lang = "ar";
	    }
	    $langJson = file_get_contents(__DIR__."/languages.json");
    	$langFile = json_decode($langJson, true);

    	//Phone codes
    	$allCountries = file_get_contents(__DIR__."/countries.json");
	    $allCountriesList = json_decode($allCountries, true);
	    ksort($allCountriesList);
    	$formCountyCodeList = "";
	    foreach ($allCountriesList as $key => $val) {
	    	if( $userDetails["status"]=='fail' ){
				$formCountyCodeList .= "<option rel='$val[code]' value='$val[name]'>
		                 $val[dial_code]
		            </option>";
	    	}else{
	    		if ($val["code"] == $userDetails["countryCode"]) {
		            $formCountyCodeList .= "
		            <option selected value='$val[name]'>
		                 $val[dial_code]
		            </option>\n";
		        } else {
		            $formCountyCodeList .= "<option rel='$val[code]' value='$val[name]'>
		                 $val[dial_code]
		            </option>";
		        }
	    	}
	        
	    }
    	

	?>

		<div class="register-box <?php echo($lang);?>">
	        <div class="opt-in-form">
	            <div class="hold-transition register-page">
	                <div class="register-box-body">
	                    <div>
	                        <h1></h1>

						    <form action="http://crm2.local:8080/leads/add" method="post" accept-charset="UTF-8">
						        <input type="hidden" name="ref_id" value="<?php echo(@$_GET['ref_id']);?>">
						        <input type="hidden" name="cid" value="<?php echo(@$_GET['cid']);?>">
						        <input type="hidden" name="wid" value="<?php echo(@$_GET['wid']);?>">
						        <input type="hidden" name="widcat" value="<?php echo(@$_GET['widcat']);?>">
						        <input type="hidden" name="campid" value="<?php echo(@$_GET['campid']);?>">
						        <input type="hidden" name="adid" value="<?php echo(@$_GET['adid']);?>">
						        <input type="hidden" name="pubid" value="<?php echo(@$_GET['pubid']);?>">
						        <input type="hidden" name="kw" value="<?php echo(@$_GET['kw']);?>">
						        <!--  <input type="hidden" name="hash" value="$2y$10$W.iT4TXr08OmrOmAktbOoeIQfM4QA1ThhVCTnObRX6mz0AgU7obYu"> -->
						        <input type="hidden" name="user_ip" value="<?php echo(@$ipaddress);?>">
						        <input type="hidden" name="referrer" value="<?php echo(@$referrer);?>">
						        <input type="hidden" name="referrer_source" value="<?php echo(@$_SERVER['HTTP_REFERER']);?>">
						        <input type="hidden" name="lang" value="<?php echo(@$_GET['lang']);?>">
						        <input type="hidden" name="session" value="<?php echo(@_COOKIE['PHPSESSID']);?>">

								<!-- Full Name -->
						        <div class="form-group has-feedback full-name-line"> 
						        	<input 
							        	type="text" class="form-control" name="full_name" required minlength="1" maxlength="80" 
							        	placeholder="<?php echo($langFile[$lang]["inputFullName"]["placeholder"]);?>" 
							        	title="<?php echo($langFile[$lang]["inputFullName"]["errorMsg"]);?>" 
							        	value="">
        							<img src="<?php echo($langFile[$lang]["inputName"]["icon"]);?>" class="user_avatar">
						        </div>

								<!-- E-mail -->
								<div class="form-group has-feedback email-line" >
                                  <input 
	                                  	type="email" class="form-control" name="email" required minlength="1" maxlength="40" 
	                                  	pattern="[^@\s]+@[^@\s]+\.[^@\s]+" 
	                                  	placeholder="<?php echo($langFile[$lang]['inputEmail']['placeholder']);?>"                              
	                                  	title="<?php echo($langFile[$lang]['inputEmail']['errorMsg']);?>" 
	                                  	value="">
                                  <img src="<?php echo($langFile[$lang]["inputEmail"]["icon"]);?>" class="user_avatar">
	                            </div>
	                            <div class="clearfix"></div>
							        
								<!-- Nationality --> 
								<?php if(isset($_GET['nat'])){?>
									<div class="form-group has-feedback nationality-line" >
			                          <select 
			                          name="nationality" id="nationality" class="form-control selectnationality" 
			                          title="<?php echo($langFile[$lang]["selectNationality"]["errorMsg"]);?>"> 
			                                <option value="" disabled="" selected="selected">جنسيتي</option>
			                                <option value="kuwaiti">كويتي</option>
			                                <option value="bahrain">البحريني</option>
			                                <option value="saudi">سعودي</option>
			                                <option value="qatar">القطري</option>
			                                <option value="oman">عمان</option>
			                                <option value="uae">الإماراتي</option>  
			                                <option value="egyptian">مصري</option>      
			                                <option value="sudanese">سوداني</option>  
			                                <option value="philippines">الفلبين</option>  
			                                <option value="yemeni">يمني</option>  
			                                <option value="indian">هندي</option>
			                                <option value="pakistani">باكستاني</option> 
			                                <option value="other">آخر</option>  
			                                </select>
			                                <img src="<?php echo($langFile[$lang]["selectNationality"]["icon"]);?>" class="user_avatar">
			                          </div>
			                          <div class="clearfix"></div>
								<?php } ?>

								<!-- Age --> 
								<?php if(isset($_GET['age'])){?>
									<div class="form-group has-feedback age-line">
	                                	<input 
	                                		type="number" class="form-control" name="age" 
	                                		required min="1" max="99" pattern="\d+"
	                                		placeholder="<?php echo($langFile[$lang]["inputAge"]["placeholder"]);?>"  
	                                		title="<?php echo($langFile[$lang]["inputAge"]["errorMsg"]);?>" value="">
	                                	<img src="<?php echo($langFile[$lang]["inputAge"]["icon"]);?>" class="user_avatar">
	                            	</div>
	                            	<div class="clearfix"></div>
                            	<?php } ?>

								<!-- Phone Code -->
								<div class="form-group has-feedback country-code-short-line">
									<select name="country" class="form-control selectcountry">
   										<?php echo($formCountyCodeList);?>
    								</select>
                                	<img src="<?php echo($langFile[$lang]["selecCountry"]["icon"]);?>" class="user_avatar">
                            	</div>
								
                            	


                            	<!-- Phone Number -->
                            	<div class="form-group has-feedback phone-short-line">
	                                <input 
	                                	type="tel" class="form-control" name="phone"required 
	                                	pattern="^\+?\d{0,15}" minlength="4" maxlength="15" value=""
	                                	placeholder="<?php echo($langFile[$lang]["inputPhone"]["placeholder"]);?>" 
	                                	autocomplete="off" 
	                                	title="<?php echo($langFile[$lang]["inputPhone"]["errorMsg"]);?>" 
	                                	>
	                                <img src="<?php echo($langFile[$lang]["inputPhone"]["icon"]);?>" class="user_avatar">
	                            </div>
								<div class="clearfix"></div>
						        
						        
						       <!-- Checkbox 18Plus -->
								<?php $randomNumber = mt_rand(1, 99); ?>
						        <div class="col-sg-12">
						            <div class="form-group" >
						                <div class="customCheckbox">
						                  <input type="checkbox" value="1" id="customCheckbox<?php echo($randomNumber);?>" name="eighteenPlus" checked required />
						                  <label for="customCheckbox<?php echo($randomNumber);?>" ></label>
						                </div>
						                <p class="termsblock">
						                    <img src="<?php echo($langFile[$lang]["ageIcon"]);?>" class="pluslog">
						                    <span><?php echo($langFile[$lang]["labelTermsAndAgeStr"]);?></span>
						                    <a
						                      data-fancybox data-type="iframe"
						                      data-slide-class="customBlock"
						                      id="reply"
						                      data-src="<?php echo($langFile[$lang]["termsURLAndAge"]);?>"
						                      class="terms-link"
						                      target="_blank"
						                      href="<?php echo($langFile[$lang]["termsURLAndAge"]);?>">
						                      <?php echo($langFile[$lang]["labelTermsAndAgeLink"]);?>
						                    </a>
						                </p>
						            </div>
						         </div>
						         <div class="clearfix"></div>


						        <!-- GDPR --->
						        <?php if(isset($_GET['gdpr'])){
									$randomNumber = mt_rand(1, 99); ?>
							        <div class="col-sg-12">
							            <div class="form-group gdrp">
							                <a data-fancybox 
							                    data-type="iframe" 
							                    class="terms-link"
							                    data-src="<?php echo($langFile[$lang]["privacyURL"]);?>"
							                    href="<?php echo($langFile[$lang]["privacyURL"]);?>"
							                    target="_blank">
							                      Compliant with GDPR policy
							                </a>
							            </div>
							        </div>
							        <div class="clearfix"></div>
						        <?php }?>
																		        
						       


						        <button type="submit" id="submit-button-action" class="btn btn-block btn-flat">تسجيل البيانات</button>
						    </form>

						</div>
					</div>
				</div>
			</div>
		</div>

</body>

</html>