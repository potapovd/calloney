<?php
//include( $_SERVER['DOCUMENT_ROOT'].'/src/inc/forms/optsgen.php' );
//include($_SERVER['DOCUMENT_ROOT'] . '/stats/c.php');

?>
<!DOCTYPE html>
<html>

<head>
    <title>Queueads البيتكوين يجعل الناس أغنياء</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <meta name="robots" content="noindex, nofollow, noodp, noarchive, nosnippet, noimageindex	" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="">


    <link rel="stylesheet" href="/src/style-fix.css">
    <link rel="stylesheet" href="index_files/new-style.css">

    <link rel="icon" type="image/png" sizes="192x192" href="/src/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/src/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/src/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/src/favicon/favicon-16x16.png">
    <link id="favicon" rel="icon" type="image/x-icon" href="/src/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#482672">
    <meta name="theme-color" content="#482672">
    <style>

    .page-id-133 .splash h1,
    body .opt-in-form .btn-flat{
        font-size:3em !important;
    }
    body .opt-in-form .btn-flat{

        line-height: 22px;
        /*font-size: 17px;*/
        height: unset;
        white-space: normal;

    }
    .register-page{
        background: transparent
    }
    .register-box-body{
        padding: 0
    }
    body .register-box.ar .form-control{
        height:unset !important;
    }
    body .register-box.ar .termsblock,
    body .register-box.ar .termsblock a{
        color: #222;
    }
    body .register-box {
        background:#ebd943;
    }
    .form-container{
            margin: 0 auto;
        }


    @media screen and (max-width: 480px){
        #splash {
           padding-top: 110px;
        }
    }@media screen and (min-width: 1980px){
        .form-container{
            margin: unset;
        }
    }


    </style>

        <noscript>
        <style>
            .inner-box-main{
                display: none
            }
        </style>
    </noscript>
    <?php //echo getPageCode(@$_GET['ref_id']); ?>
     <script>window.addEventListener("message", function (event) {if (event.data.hasOwnProperty("FrameHeight")) {const formIframe = document.querySelectorAll(".formIframe");for (var i = 0; i < formIframe.length; i++) {formIframe[i].style.height = event.data.FrameHeight+"px";formIframe[i].style.opacity = 1;}}});function setIframeHeight(ifrm) {var height = ifrm.contentWindow.postMessage("FrameHeight", "*");}</script>
</head>

<body class="page-template-default page page-id-133 mega-menu-header-menu crypto-lp2 elementor-default"
    cz-shortcut-listen="true">
    <?php echo AfterHeader(@$_GET['ref_id']); ?>
    <div class="wrapper content-wrapper">
        <header class="header clear" role="banner">
        </header>
        <main id="success-page" role="main">
            <div class="top-header">
                <p>تحذير: نظرًا للارتفاع الشديد في الطلب على الوسائط، سنغلق التسجيل اعتبارًا من <span class="s-date">11/18/2018</span>
                    - أسرع! <span class="s-min">2</span>:<span class="s-sec">11</span></p>
            </div>
            <div class="success-header">
                <!--<div class="right-part">
                     <div class="testo-txt">
                        <p>إبراهيم و.</p>
                        <p>صنع للتو</p>
                        <p>﷼226</p>
                    </div>
                    <div class="testo-img"> <img src="./index_files/12.jpg" alt="man" class="lazy"
                            data-was-processed="true"></div>
                </div>-->
                <!-- <div class="middle-part">
                    <div class="middle-part-txt">
                        <p></p>
                        <p> <span class="red"></span></p>
                    </div>
                    <div class="middle-part-img"></div>
                </div> -->
                <!-- <div class="header-logo">

                </div> -->
            </div>
            <section id="splash" class="splash">
                <h1>البيتكوين يجعل الناس أغنياء</h1>
                <h2>وبإمكانك أن تصبح<span class="yellow"> المليونير القادم...</span></h2>
                <div class="vid-input-container">
                    <div class="vid-input">
                        <div class="video">


                            <div class="video--overlay">

                               <div class="mainVideoWrapper" style="width:100%;height:auto;">
                            <noscript>
                                <video controls autoplay muted loop id="player" style="width:100%;height:auto;position:relative;z-index:100" preload="metadata">
                                    <source src="index.mp4" type='video/mp4'/>
                                    Sorry! (
                                </video>
                            </noscript>
                            <div id="video">
                                <video id="video5" controls autoplay playsinline muted loop style="width:100%;height:auto;position:relative;z-index:100;display:none" preload="metadata">
                                    <source type='video/mp4'/>
                                    Sorry! (
                                </video>
                                <div class="inner-box-main" id="video5Unmute" style="display:none">
                                    <div id="inner-box">
                                        <p>تشغيل الفيديو</p>
                                        <p class="buttonUnmute">
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496.16 496.15"><defs><style>.a{fill:#e04f5f}.b{fill:#fff}</style></defs><title>Unmute</title><path class="a" d="M496.16,248.09C496.16,111.06,385.09,0,248.08,0S0,111.06,0,248.09,111.07,496.16,248.08,496.16,496.16,385.09,496.16,248.09Z" transform="translate(0 0)"/><path class="b" d="M259.43,128.92a10.59,10.59,0,0,0-10.81.42l-95.14,61.79H118.32a10.59,10.59,0,0,0-10.59,10.59v92.72A10.59,10.59,0,0,0,118.32,305h35.16l95.14,61.79A10.59,10.59,0,0,0,265,357.94V138.22A10.59,10.59,0,0,0,259.43,128.92Z" transform="translate(0 0)"/><path class="b" d="M355.4,248.08l30.39-30.39A9,9,0,1,0,373.06,205l-30.38,30.38L312.29,205a9,9,0,1,0-12.72,12.72L330,248.08l-30.38,30.38a9,9,0,1,0,12.72,12.73l30.39-30.38,30.38,30.38a9,9,0,0,0,12.73-12.73Z" transform="translate(0 0)"/></svg>
                                        </p>
                                        <p>انقر لتشغيل الصوت</p>
                                    </div>
                                </div>
                                <iframe frameborder="0" src="index.mp4" id="videoI" allow="autoplay;"></iframe>
                            </div>
                        </div>



                              <!-- <video controls autoplay playsinline muted loop id="player" poster="/src/video/poster.jpg" style="width:100%; height: auto; max-width: 900px">
                                  <source src="video/index.mp4" type='video/mp4' />
                                  Sorry! (
                              </video>

                              <div class="inner-box-main">
                                  <div id="inner-box">
                                      <p>تشغيل الفيديو</p>
                                      <p class="buttonUnmute">
                                          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496.16 496.15"><defs><style>.a{fill:#e04f5f;}.b{fill:#fff;}</style></defs><title>Unmute</title><path class="a" d="M496.16,248.09C496.16,111.06,385.09,0,248.08,0S0,111.06,0,248.09,111.07,496.16,248.08,496.16,496.16,385.09,496.16,248.09Z" transform="translate(0 0)"/><path class="b" d="M259.43,128.92a10.59,10.59,0,0,0-10.81.42l-95.14,61.79H118.32a10.59,10.59,0,0,0-10.59,10.59v92.72A10.59,10.59,0,0,0,118.32,305h35.16l95.14,61.79A10.59,10.59,0,0,0,265,357.94V138.22A10.59,10.59,0,0,0,259.43,128.92Z" transform="translate(0 0)"/><path class="b" d="M355.4,248.08l30.39-30.39A9,9,0,1,0,373.06,205l-30.38,30.38L312.29,205a9,9,0,1,0-12.72,12.72L330,248.08l-30.38,30.38a9,9,0,1,0,12.72,12.73l30.39-30.38,30.38,30.38a9,9,0,0,0,12.73-12.73Z" transform="translate(0 0)"/></svg>
                                      </p>
                                      <p>انقر لتشغيل الصوت</p>
                                  </div>
                              </div> -->
                            </div>




                        </div>
                        <div class="div-input">
                            <div class="form-container" style="text-align: center; max-width: 350px; ">
                                <div>
                                    <!-- Header -->
                                    <div class="form-header">
                                        <h1> قم بتغيير </h1>
                                        <h1>حياتك اليوم!</h1>
                                    </div>
                                    <div class="register-box" style="width:100%">
                                        <div class="register-box-body">
                                            <div class="row" id="registrationBox">

                                                 <iframe src="https://staging.adsguide.info/form/formIframe?<?php echo(urldecode($_SERVER['QUERY_STRING']));?>" style="width: 100%;height: 100%;overflow: hidden; opacity: 0;" frameborder="0" scrolling="no" class="formIframe" onload="setIframeHeight(this)"></iframe>


                                            </div>
                                            <div class="alert alert-success" id="successMessage" style="display:none;">
                                            </div>
                                        </div>
                                        <!-- /.form-box -->
                                    </div>
                                    <!-- /.register-box -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="second">
                <div class="right">
                    <img data-src="./index_files/safety.jpg" class="lazy" alt="security logos lazy">
                    <noscript>
                        <img src="./index_files/safety.jpg" class="lazy" alt="security logos ">
                    </noscript>
                </div>
                <div class="left">
                    <div class="pcw pcw-leaderboard pcw-color-frame pcw-ct-white pcw-visible" data-symbol="BTC~USD"
                        data-type="leaderboard" data-flash="1">
                        <div class="pcw-header-left">
                            <div> <span class="pcw-market-data-field pcw-field-LOGO64" data-field="LOGO64"><img src="./index_files/1182-BTC.png"
                                        alt="Bitcoin" title="Bitcoin"></span> <span class="pcw-market-data-field pcw-field-NAME"
                                    data-field="NAME">Bitcoin</span></div>
                            <div class="pcw-quote"> <span class="pcw-market-data-field pcw-field-FROMSYMBOL" data-field="FROMSYMBOL">BTC</span>
                                <sup><span class="pcw-market-data-field pcw-field-TOSYMBOLSIGN" data-field="TOSYMBOLSIGN">$</span></sup><span
                                    class="pcw-market-data-field pcw-field-PRICE pcw-flash pcw-animate-red" data-field="PRICE">5,596.00</span>
                                <span class="pcw-market-data-field pcw-change-indicator pcw-field-DUMMY pcw-rise"
                                    data-field="DUMMY"> <i class="fas fa-arrow-down pcw-arrow-icon pcw-arrow-drop"></i>
                                    <i class="fas fa-arrow-up pcw-arrow-icon pcw-arrow-rise"></i> </span></div>
                            <div class="pcw-change-quote"> <span class="pcw-market-data-field pcw-change-indicator pcw-field-ABSCHANGE24HOUR pcw-rise"
                                    data-field="ABSCHANGE24HOUR">23.44</span> <span> / </span> <span class="pcw-market-data-field pcw-change-indicator pcw-field-PCTCHANGE24HOUR pcw-rise"
                                    data-field="PCTCHANGE24HOUR">0.42%</span></div>
                        </div>
                        <div class="pcw-header-right">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>Low 24H</td>
                                        <td><span class="pcw-market-data-field pcw-field-LOW24HOUR" data-field="LOW24HOUR">5,536.74</span></td>
                                    </tr>
                                    <tr>
                                        <td>High 24H</td>
                                        <td><span class="pcw-market-data-field pcw-field-HIGH24HOUR" data-field="HIGH24HOUR">5,629.28</span></td>
                                    </tr>
                                    <tr>
                                        <td>Volume 24H</td>
                                        <td><span class="pcw-market-data-field pcw-field-VOLUME24HOUR" data-field="VOLUME24HOUR">31,147.25</span></td>
                                    </tr>
                                    <tr>
                                        <td>Last Trade Volume</td>
                                        <td><span class="pcw-market-data-field pcw-field-LASTVOLUME" data-field="LASTVOLUME">0.02</span></td>
                                    </tr>
                                    <tr>
                                        <td>Last Market</td>
                                        <td><span class="pcw-market-data-field pcw-field-LASTMARKET" data-field="LASTMARKET">itBit</span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
            <section class="third">
                <div class="third-txt">
                    <h1>انضم إلينا وابدأ في الحصول على الثروة</h1>
                    <h1 class="purple">مع ثورة البيتكوين!</h1>
                    <p>ثورة البيتكوين هي عبارة عن فئة محفوظة حصريًا للأشخاص الذين قفزوا على العوائد المجنونة التي
                        يقدمها البيتكوين وتمكنوا من جمع ثروة أثناء قيامهم بذلك. يستمتع أعضاؤنا بالخلوات في جميع أنحاء
                        العالم كل شهر، بينما يربحون المال على حاسوبهم المحمول مع بضع دقائق من العملكل يوم .</p>
                </div>
                <div class="right">
                    <img data-src="./index_files/girl-holding-bitcoin3.png" alt="woman holding bitcoin" class="lazy"
                        data-was-processed="true">
                    <noscript>
                        <img src="./index_files/girl-holding-bitcoin3.png">
                    </noscript>
                </div>
            </section>
            <section class="forth">
                <div class="left">
                    <p class="purple"> كما شوهد على</p> <img alt="media logos lazy" data-src="./index_files/seenon.png">
                    <noscript>
                        <img alt="media logos " src="./index_files/seenon.png">
                    </noscript>
                </div>
            </section>
            <section class="fifth">
                <div class="fifth-title">
                    <h2 class="purple">شهادات حقيقية من مستخدمينا</h2>
                </div>
                <div class="testomonials lazy">
                    <div class="testo testo-1">
                        <div class="theimg thehovercontainer">
                            <div class="thetxt">
                                <h3>وليد العلي</h3>
                                <h3>الدوحة قطر</h3>
                                <p class="yellow">الفوائد: ﷼ 46,782.32</p>
                            </div>
                            <div class="thehovertxt">
                                <p>لقد كنت عضوًا في ثورة البيتكوين لمدة 47 يومًا. لكن حياتي تغيرت بالفعل! لم أحصل على
                                    أول 10 آلاف دولار فقط ، بل التقيت أيضًا ببعض الأشخاص الأكثر روعة في هذه العملية.
                                    شكرًا، ثورة بيتكوين.!</p>
                            </div>
                        </div>
                    </div>
                    <div class="testo testo-2">
                        <div class="theimg  thehovercontainer">
                            <div class="thetxt">
                                <h3>ثريا برماوي</h3>
                                <h3>الدوحة قطر</h3>
                                <p class="yellow">الفوائد: ﷼ 26,010,00</p>
                            </div>
                            <div class="thehovertxt">
                                <p>أخيرًا أصبحت أعلم ما الذي يعنيه أن أعيش الحلم. لم أعد أشعر وكأنني في الخارج وأنظر
                                    إلى ما في الداخل بينما يحظى كل شخص آخر يحظى بكل المتعة. سمحت لي ثورة البيتكوين
                                    بالتقاعد مبكراً وعيش نمط حياة الأغنياء</p>
                            </div>
                        </div>
                    </div>
                    <div class="testo testo-3">
                        <div class="theimg  thehovercontainer">
                            <div class="thetxt">
                                <h3>عناد العمرو</h3>
                                <h3>دبي الامارات العربية المتحدة</h3>
                                <p class="yellow">الفوائد: ﷼ 38,057.73</p>
                            </div>
                            <div class="thehovertxt">
                                <p>من الغريب أنني قد اعتدت الاستثمار في وول ستريت. ولكنني لم أر قط أي شيء من هذا القبيل
                                    في فترة رئاستي لمدة 10 سنوات في الشركة. ولقد ظن أصدقائي أنني قد جننت عندما استقلت
                                    من الشركة للاستثمار في برنامج ثورة البيتكوين بدوام كامل. 38.459 دولار أرباح لاحقًا،
                                    لقد أصبح جميع أصدقائي يتمنون لأشركهم معي.</p>
                            </div>
                        </div>
                    </div>
                    <div class="testo testo-4">
                        <div class="theimg thehovercontainer">
                            <div class="thetxt">
                                <h3>ناديا الخالدي</h3>
                                <h3>مدينة الكويت ، الكويت</h3>
                                <p class="yellow">الفوائد: ﷼ 30,803.61</p>
                            </div>
                            <div class="thehovertxt">
                                <p>منذ أسبوعين، تم الاستغناء عني مع عدم وجود خيارات متبقية، اعتقدت أن حياتي قد انتهت.
                                    الآن أقوم بعمل أكثر من 1،261.42 دولار يوميًا وكل يوم. وللمرة الأولى في شهرين، حسابي
                                    لم يعد مكشوفًا. شكرًا، ثورة البيتكوين!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="seventh lazy">
                <div class="container">
                    <h1 class="purple">نتائج ربح مباشر!</h1>
                    <div class="quotes">
                        <div id="pcw-315234809" class="pcw pcw-table-trades pcw-basic pcw-ct-default pcw-visible"
                            data-symbol="BTC~USD" data-type="table-trades" data-flash="1" data-fields="M,TYPE,Q,P,TOTAL"
                            data-subs="0~Cryptsy~BTC~USD,0~BTCChina~BTC~USD,0~Bitstamp~BTC~USD,0~OKCoin~BTC~USD,0~Coinbase~BTC~USD,0~Poloniex~BTC~USD,0~Cexio~BTC~USD,0~BTCE~BTC~USD,0~BitTrex~BTC~USD,0~Kraken~BTC~USD,0~Bitfinex~BTC~USD,0~LocalBitcoins~BTC~USD,0~itBit~BTC~USD,0~Coinfloor~BTC~USD,0~Huobi~BTC~USD,0~LakeBTC~BTC~USD,0~Coinsetter~BTC~USD,0~CCEX~BTC~USD,0~MonetaGo~BTC~USD,0~Gatecoin~BTC~USD,0~Gemini~BTC~USD,0~CCEDK~BTC~USD,0~Exmo~BTC~USD,0~Yobit~BTC~USD,0~BitBay~BTC~USD,0~QuadrigaCX~BTC~USD,0~BitSquare~BTC~USD,0~TheRockTrading~BTC~USD,0~bitFlyer~BTC~USD,0~Quoine~BTC~USD,0~LiveCoin~BTC~USD,0~WavesDEX~BTC~USD,0~Lykke~BTC~USD,0~Remitano~BTC~USD,0~Coinroom~BTC~USD,0~Abucoins~BTC~USD,0~TrustDEX~BTC~USD,0~BitFlip~BTC~USD,0~Coincap~BTC~USD,0~Graviex~BTC~USD,0~ExtStock~BTC~USD,0~DSX~BTC~USD,0~Bitlish~BTC~USD,0~CoinDeal~BTC~USD,0~CoinsBank~BTC~USD,0~Neraex~BTC~USD,0~SingularityX~BTC~USD,0~Simex~BTC~USD,0~RightBTC~BTC~USD,0~WEX~BTC~USD,0~BitexBook~BTC~USD,0~IndependentReserve~BTC~USD,0~CoinHub~BTC~USD,0~Ore~BTC~USD,0~Bitsane~BTC~USD,0~BTCAlpha~BTC~USD,0~P2PB2B~BTC~USD,0~StocksExchange~BTC~USD,0~Liquid~BTC~USD,0~Coinsbit~BTC~USD,0~Incorex~BTC~USD,0~Exenium~BTC~USD,0~StocksExchangeio~BTC~USD"
                            data-max-trades="10">
                            <table>
                                <thead>
                                    <tr>
                                        <th class="pcw-trade-heading-M">Market</th>
                                        <th class="pcw-trade-heading-TYPE">Type</th>
                                        <th class="pcw-trade-heading-Q">Quantity</th>
                                        <th class="pcw-trade-heading-P">Price</th>
                                        <th class="pcw-trade-heading-TOTAL">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="pcw-trade-type-buy">
                                        <td data-title="Market" class="pcw-animate-green"><span class="pcw-trade-field-M">Exenium</span></td>
                                        <td data-title="Type" class="pcw-animate-green"><span class="pcw-trade-type-indicator-buy"></span><span
                                                class="pcw-trade-field-TYPE">buy</span></td>
                                        <td data-title="Quantity" class="pcw-animate-green"><span class="pcw-trade-field-Q">0.02148</span></td>
                                        <td data-title="Price" class="pcw-animate-green"><sup>$</sup><span class="pcw-trade-field-P">5,473.08</span></td>
                                        <td data-title="Total" class="pcw-animate-green"><sup>$</sup><span class="pcw-trade-field-TOTAL">117.56</span></td>
                                    </tr>
                                    <tr class="pcw-trade-type-unknown">
                                        <td data-title="Market" class=""><span class="pcw-trade-field-M">itBit</span></td>
                                        <td data-title="Type" class=""><span class="pcw-trade-type-indicator-unknown"></span><span
                                                class="pcw-trade-field-TYPE">unknown</span></td>
                                        <td data-title="Quantity" class=""><span class="pcw-trade-field-Q">0.0244</span></td>
                                        <td data-title="Price" class=""><sup>$</sup><span class="pcw-trade-field-P">5,524.31</span></td>
                                        <td data-title="Total" class=""><sup>$</sup><span class="pcw-trade-field-TOTAL">134.79</span></td>
                                    </tr>
                                    <tr class="pcw-trade-type-unknown">
                                        <td data-title="Market" class=""><span class="pcw-trade-field-M">itBit</span></td>
                                        <td data-title="Type" class=""><span class="pcw-trade-type-indicator-unknown"></span><span
                                                class="pcw-trade-field-TYPE">unknown</span></td>
                                        <td data-title="Quantity" class=""><span class="pcw-trade-field-Q">0.0244</span></td>
                                        <td data-title="Price" class=""><sup>$</sup><span class="pcw-trade-field-P">5,524.31</span></td>
                                        <td data-title="Total" class=""><sup>$</sup><span class="pcw-trade-field-TOTAL">134.79</span></td>
                                    </tr>
                                    <tr class="pcw-trade-type-unknown">
                                        <td data-title="Market" class=""><span class="pcw-trade-field-M">itBit</span></td>
                                        <td data-title="Type" class=""><span class="pcw-trade-type-indicator-unknown"></span><span
                                                class="pcw-trade-field-TYPE">unknown</span></td>
                                        <td data-title="Quantity" class=""><span class="pcw-trade-field-Q">0.2942</span></td>
                                        <td data-title="Price" class=""><sup>$</sup><span class="pcw-trade-field-P">5,524.31</span></td>
                                        <td data-title="Total" class=""><sup>$</sup><span class="pcw-trade-field-TOTAL">1,625.25</span></td>
                                    </tr>
                                    <tr class="pcw-trade-type-unknown">
                                        <td data-title="Market" class=""><span class="pcw-trade-field-M">itBit</span></td>
                                        <td data-title="Type" class=""><span class="pcw-trade-type-indicator-unknown"></span><span
                                                class="pcw-trade-field-TYPE">unknown</span></td>
                                        <td data-title="Quantity" class=""><span class="pcw-trade-field-Q">0.2942</span></td>
                                        <td data-title="Price" class=""><sup>$</sup><span class="pcw-trade-field-P">5,524.31</span></td>
                                        <td data-title="Total" class=""><sup>$</sup><span class="pcw-trade-field-TOTAL">1,625.25</span></td>
                                    </tr>
                                    <tr class="pcw-trade-type-unknown">
                                        <td data-title="Market" class=""><span class="pcw-trade-field-M">itBit</span></td>
                                        <td data-title="Type" class=""><span class="pcw-trade-type-indicator-unknown"></span><span
                                                class="pcw-trade-field-TYPE">unknown</span></td>
                                        <td data-title="Quantity" class=""><span class="pcw-trade-field-Q">1.2776</span></td>
                                        <td data-title="Price" class=""><sup>$</sup><span class="pcw-trade-field-P">5,524.97</span></td>
                                        <td data-title="Total" class=""><sup>$</sup><span class="pcw-trade-field-TOTAL">7,058.70</span></td>
                                    </tr>
                                    <tr class="pcw-trade-type-unknown">
                                        <td data-title="Market" class=""><span class="pcw-trade-field-M">itBit</span></td>
                                        <td data-title="Type" class=""><span class="pcw-trade-type-indicator-unknown"></span><span
                                                class="pcw-trade-field-TYPE">unknown</span></td>
                                        <td data-title="Quantity" class=""><span class="pcw-trade-field-Q">1.2776</span></td>
                                        <td data-title="Price" class=""><sup>$</sup><span class="pcw-trade-field-P">5,524.97</span></td>
                                        <td data-title="Total" class=""><sup>$</sup><span class="pcw-trade-field-TOTAL">7,058.70</span></td>
                                    </tr>
                                    <tr class="pcw-trade-type-sell">
                                        <td data-title="Market" class="pcw-animate-red"><span class="pcw-trade-field-M">Exmo</span></td>
                                        <td data-title="Type" class="pcw-animate-red"><span class="pcw-trade-type-indicator-sell"></span><span
                                                class="pcw-trade-field-TYPE">sell</span></td>
                                        <td data-title="Quantity" class="pcw-animate-red"><span class="pcw-trade-field-Q">0.050032</span></td>
                                        <td data-title="Price" class="pcw-animate-red"><sup>$</sup><span class="pcw-trade-field-P">5,821.40</span></td>
                                        <td data-title="Total" class="pcw-animate-red"><sup>$</sup><span class="pcw-trade-field-TOTAL">291.26</span></td>
                                    </tr>
                                    <tr class="pcw-trade-type-sell">
                                        <td data-title="Market" class="pcw-animate-red"><span class="pcw-trade-field-M">Exmo</span></td>
                                        <td data-title="Type" class="pcw-animate-red"><span class="pcw-trade-type-indicator-sell"></span><span
                                                class="pcw-trade-field-TYPE">sell</span></td>
                                        <td data-title="Quantity" class="pcw-animate-red"><span class="pcw-trade-field-Q">0.001</span></td>
                                        <td data-title="Price" class="pcw-animate-red"><sup>$</sup><span class="pcw-trade-field-P">5,820.00</span></td>
                                        <td data-title="Total" class="pcw-animate-red"><sup>$</sup><span class="pcw-trade-field-TOTAL">5.82</span></td>
                                    </tr>
                                    <tr class="pcw-trade-type-buy">
                                        <td data-title="Market" class="pcw-animate-green"><span class="pcw-trade-field-M">Coinbase</span></td>
                                        <td data-title="Type" class="pcw-animate-green"><span class="pcw-trade-type-indicator-buy"></span><span
                                                class="pcw-trade-field-TYPE">buy</span></td>
                                        <td data-title="Quantity" class="pcw-animate-green"><span class="pcw-trade-field-Q">0.00594224</span></td>
                                        <td data-title="Price" class="pcw-animate-green"><sup>$</sup><span class="pcw-trade-field-P">5,527.01</span></td>
                                        <td data-title="Total" class="pcw-animate-green"><sup>$</sup><span class="pcw-trade-field-TOTAL">32.84</span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
            <section class="heigth">
                <h1 class="purple">الخطوة الأول :</h1>
                <div class="quotes">
                    <div class="right">
                        <h2>الخطوة الأولى :</h2> <i class="fas fa-file-signature"></i>
                        <p>ابدأ طريقك بإنشاء حساب من (هنا) وتأكد من كتابة رقم هاتفك بشكل صحيح بحيث يكون متاحاً</p>
                    </div>
                    <div class="middle">
                        <h2>الخطوة الثانية :</h2> <i class="fas fa-phone"></i>
                        <p>سيقى متخصص على اتصال بك , يقدم لك النصائح والإرشادات , و يجيب على استفساراتك بشأن إيداعاتك
                            وجميع ما يتعلق بحسابك</p>
                    </div>
                    <div class="left">
                        <h2>الخطوة الثالثة :</h2> <i class="fas fa-hand-holding-usd"></i>
                        <p>تمتع بالتداول والربح وضاعف رأس مالك
                            إبدأ رحلتك بالتداول وضاعف رأس مالك</p>
                    </div>
                </div>
            </section>
            <section class="blue">
                <div class="container">
                    <div class="gold-btn"> <a class="goto" href="#opt-in-form">انضم الآن!</a></div>
                </div>
            </section>
            <section class="tenth">
                <h1 class="purple">كيف تعمل</h1>
                <div class="right">
                    <div class="point">
                        <div class="num">1</div>
                        <div class="txt">
                            <h2>ما نوع النتائج التي يمكن أن أتوقعها؟</h2>
                            <p>عادة ما يحقق أعضاء ثورة البيتكوين الحد الأدنى من ﷼ 4,731.74 يومية.</p>
                        </div>
                    </div>
                    <div class="point">
                        <div class="num">2</div>
                        <div class="txt">
                            <h2>كم عدد الساعات التي أحتاجها للعمل يوميًا؟</h2>
                            <p>يعمل أعضاؤنا بمعدل 20 دقيقة في اليوم أو أقل. ونظرًا لأن البرنامج يقوم بمعالجة التداول
                                فإن مقدار العمل المطلوب يكون ضئيلاً.</p>
                        </div>
                    </div>
                    <div class="point">
                        <div class="num">3</div>
                        <div class="txt">
                            <h2>ما هو الحد الأقصى للمبلغ الذي يمكنني تحقيقه؟</h2>
                            <p>أرباحك غير محدودة مع ثورة البيتكوين. كسب بعض الأعضاء أول مليون خلال 61 يومًا فقط.</p>
                        </div>
                    </div>
                </div>
                <div class="left">
                    <div class="point">
                        <div class="num">4</div>
                        <div class="txt">
                            <h2>كم تكلفة البرنامج؟</h2>
                            <p>يحصل أعضاء ثورة البيتكوين على نسخة من برنامجنا المجاني، ولتصبح عضوًا ما عليك سوى ملء
                                النموذج الموجود على هذه الصفحة.</p>
                        </div>
                    </div>
                    <div class="point">
                        <div class="num">5</div>
                        <div class="txt">
                            <h2>هل هذا يشبه التسويق الشبكي أو التسويق بالعمولة؟</h2>
                            <p>إنه لا يشبه التسويق الشبكي أو التسويق بالعمولة أو أي شيء آخر هناك. فالبرنامج يربح
                                التداولات بدقة 99.4%.</p>
                        </div>
                    </div>
                    <div class="point">
                        <div class="num">6</div>
                        <div class="txt">
                            <h2>هل هناك أي رسوم؟</h2>
                            <p>لا توجد رسوم خفية. ولا رسوم للوسيط أو إكراميات. جميع أموالك ملكك 100% ولديك مطلق الحرية
                                في سحبها في أي وقت تختاره دون تأخير.</p>
                        </div>
                    </div>
                </div>
            </section>
            <section class="blue sec-blue">
                <div class="container">
                    <div class="gold-btn"> <a class="goto" href="#opt-in-form">انضم الآن!</a></div>
                </div>
            </section>
        </main>
    </div>

    <?php //echo GetFooter("AR");?>

    <script src="./index_files/pcwGlobals.js"></script>
    <!-- <script src="/src/jQuery/jquery-3.3.1.min.js"></script>
    <script src="/src/fancybox/jquery.fancybox.min.js"></script>
    <script src="/src/intl-tel-input-master/js/intlTelInput.min.js"></script> -->
    <script src="/src/plugins.js"></script>
    <!--     <script src="//crm.adsguide.info/dist/optindata/js/optin/ar/optingen.min.js"></script>
 -->
    <script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js'></script>
    <script>
        WebFont.load({
            google: {
                families: ['Source Sans Pro']
            }
        });



        $(document).ready(function () {

            $(".goto").mPageScroll2id();


            // var allParams = getUrlVars();
            // if (allParams.t == 0) {
            //     var terms = false;
            // } else {
            //     var terms = true;
            // }


            // $('#opt-in-form').initOpt({
            //     inputFirstName: {
            //         display: true,
            //         placeholder: 'الاسم الشخصي'
            //     },
            //     inputLastName: {
            //         display: true,
            //         placeholder: 'اسم العائله'
            //     },
            //     inputAge: {
            //         display: true,
            //         placeholder: 'العمر'
            //     },
            //     buttonSubmitText: 'امنحني إذن الدخول الآن!',
            //     thankYouURL: 'https://queueads.com/ar/thank-you/',
            //     showTerms: terms
            // });

        });
    </script>
    <script async type="text/javascript" src="./index_files/autoptimize_e4170f9eb9bfb048b85234efa1b53e99.js"></script>
</body>

</html>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="//use.fontawesome.com/releases/v5.5.0/css/all.css">
