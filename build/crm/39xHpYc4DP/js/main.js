(function ($) {
    document.body.oncontextmenu = function (e) {
        return false;
    };

    

    var gogo = {
        init: function () {
            var counter = 9000;
            form = function (t) {
                var minutes = Math.floor(t / 600),
                    seconds = Math.floor((t / 10) % 60);
                minutes = (minutes < 10) ? "0" + minutes.toString() : minutes.toString();
                seconds = (seconds < 10) ? "0" + seconds.toString() : seconds.toString();
                $('.av-warning-timer').text(minutes + ":" + seconds + "." + "0" + Math.floor(t % 10));
            };

            setInterval(function () {
                counter--;
                form(counter);
                if (counter === 0) counter = 9000;
            }, 100);
        }
    };
    gogo.init();

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    var month = "";
    switch (mm) {
        case 1:
            month = "كَانُون ٱلثَّانِي";
            break;
        case 2:
            month = "شُبَاط";
            break;
        case 3:
            month = "آذَار  ";
            break;
        case 4:
            month = "نِيسَان";
            break;
        case 5:
            month = "أَيَّار  ";
            break;
        case 6:
            month = "حَزِيرَان";
            break;
        case 7:
            month = "تَمُّوز";
            break;
        case 8:
            month = "آب ";
            break;
        case 9:
            month = "أَيْلُول ";
            break;
        case 10:
            month = "شْرِين ٱلْأَوَّل";
            break;
        case 11:
            month = "شْرِين ٱلثَّانِي";
            break;
        case 12:
            month = "انُون ٱلْأَوَّل";
            break;
    }
    today = dd + ' ' + month + ' ' + yyyy;
    $(".the-title").text(today);

}(jQuery));