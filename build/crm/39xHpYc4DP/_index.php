<?php
include( $_SERVER['DOCUMENT_ROOT'].'/src/inc/forms/optsgen.php' );
include($_SERVER['DOCUMENT_ROOT'] . '/stats/c.php');

?>
<!DOCTYPE html>
<html lang="ar" dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, minimum-scale=1.0, maximum-scale=1.0, shrink-to-fit=no" name="viewport">
    <meta name="HandheldFriendly" content="True">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Queueads</title>

    <link rel="stylesheet" href="/src/style-fix.css">
    <link rel="stylesheet" href="css/styles.css">

    <title> Queueads عاجل : خادمة أندونيسية تشتري الفيلا 2 &#8211; اخبار الكترونيه </title>

    <link rel="icon" type="image/png" sizes="192x192" href="/src/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/src/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/src/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/src/favicon/favicon-16x16.png">
    <link id="favicon" rel="icon" type="image/x-icon" href="/src/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#CC9C2E">
    <meta name="theme-color" content="#CC9C2E">
    <style>
        #firstimg {
            width: 65% !important;
            height: auto;
            margin: 20px auto;
            min-width: 300px;
            max-width:600px;
        }
        @media only screen and (max-width: 980px) { 
            #firstimg,
            .alignright {
                margin:20px 0;
            }
        }
        .alignright{
                display: block;
            margin: 20px auto;
        }

        img {
            height: auto;
            max-width: 100%;
            border: none;
            -webkit-border-radius: 0;
            border-radius: 0;
            -webkit-box-shadow: none;
            box-shadow: none;
        }

        img.aligncenter,
        .wp-caption.aligncenter {
            display: block;
            margin: 0 auto;
        }

        body .termsblock,
        body .termsblock a {
            color: #45484d !important;
        }

        body .register-box {
            margin: 0 auto;
        }

        .opt-in-form {
            margin: 0 auto;
            box-sizing: border-box;
            background: #CC9C2E;
            padding: 20px;
            border-radius: 10px;
        }



    

        .currency-money {
            direction: rtl !important;
        }

        .noty_type__reply {
            background-color: #fff;
            border-radius: 40px;
            box-shadow: 1px 1px 20px rgba(0, 0, 0, .2);
        }

        .noty_layout_mixin,
        #noty_layout__top,
        #noty_layout__topLeft,
        #noty_layout__topCenter,
        #noty_layout__topRight,
        #noty_layout__bottom,
        #noty_layout__bottomLeft,
        #noty_layout__bottomCenter,
        #noty_layout__bottomRight,
        #noty_layout__center,
        #noty_layout__centerLeft,
        #noty_layout__centerRight {
            position: fixed;
            margin: 0;
            padding: 0;
            z-index: 9999999;
            -webkit-transform: translateZ(0) scale(1, 1);
            transform: translateZ(0) scale(1, 1);
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden;
            -webkit-font-smoothing: subpixel-antialiased;
            filter: blur(0);
            -webkit-filter: blur(0);
            max-width: 90%;
            box-sizing: border-box;
        }

        #noty_layout__top {
            top: 0;
            left: 5%;
            width: 90%;
        }

        #noty_layout__topLeft {
            top: 20px;
            left: 20px;
            width: 325px;
        }

        #noty_layout__topCenter {
            top: 5%;
            left: 50%;
            width: 325px;
            -webkit-transform: translate(-webkit-calc(-50% - .5px)) translateZ(0) scale(1, 1);
            transform: translate(calc(-50% - .5px)) translateZ(0) scale(1, 1);
        }

        #noty_layout__topRight {
            top: 20px;
            right: 20px;
            width: 325px;
        }

        #noty_layout__bottom {
            bottom: 0;
            left: 5%;
            width: 90%;
        }

        #noty_layout__bottomLeft {
            bottom: 20px;
            left: 20px;
            width: 325px;
        }

        #noty_layout__bottomCenter {
            bottom: 5%;
            left: 50%;
            width: 325px;
            -webkit-transform: translate(-webkit-calc(-50% - .5px)) translateZ(0) scale(1, 1);
            transform: translate(calc(-50% - .5px)) translateZ(0) scale(1, 1);
        }

        #noty_layout__bottomRight {
            bottom: 20px;
            right: 20px;
            width: 325px;
        }

        #noty_layout__center {
            top: 50%;
            left: 50%;
            width: 325px;
            -webkit-transform: translate(-webkit-calc(-50% - .5px), -webkit-calc(-50% - .5px)) translateZ(0) scale(1, 1);
            transform: translate(calc(-50% - .5px), calc(-50% - .5px)) translateZ(0) scale(1, 1);
        }

        #noty_layout__centerLeft {
            top: 50%;
            left: 20px;
            width: 325px;
            -webkit-transform: translate(0, -webkit-calc(-50% - .5px)) translateZ(0) scale(1, 1);
            transform: translate(0, calc(-50% - .5px)) translateZ(0) scale(1, 1);
        }

        #noty_layout__centerRight {
            top: 50%;
            right: 20px;
            width: 325px;
            -webkit-transform: translate(0, -webkit-calc(-50% - .5px)) translateZ(0) scale(1, 1);
            transform: translate(0, calc(-50% - .5px)) translateZ(0) scale(1, 1);
        }

        .noty_progressbar {
            display: none;
        }

        .noty_has_timeout.noty_has_progressbar .noty_progressbar {
            display: block;
            position: absolute;
            left: 0;
            bottom: 0;
            height: 3px;
            width: 100%;
            background-color: #646464;
            opacity: 0.2;
            filter: alpha(opacity=10);
        }

        .noty_bar {
            -webkit-backface-visibility: hidden;
            -webkit-transform: translate(0, 0) translateZ(0) scale(1, 1);
            -ms-transform: translate(0, 0) scale(1, 1);
            transform: translate(0, 0) scale(1, 1);
            -webkit-font-smoothing: subpixel-antialiased;
            overflow: hidden;
        }

        .noty_effects_open {
            opacity: 0;
            -webkit-transform: translate(50%);
            -ms-transform: translate(50%);
            transform: translate(50%);
            -webkit-animation: noty_anim_in 0.5s cubic-bezier(0.68, -0.55, 0.265, 1.55);
            animation: noty_anim_in 0.5s cubic-bezier(0.68, -0.55, 0.265, 1.55);
            -webkit-animation-fill-mode: forwards;
            animation-fill-mode: forwards;
        }

        .noty_effects_close {
            -webkit-animation: noty_anim_out 0.5s cubic-bezier(0.68, -0.55, 0.265, 1.55);
            animation: noty_anim_out 0.5s cubic-bezier(0.68, -0.55, 0.265, 1.55);
            -webkit-animation-fill-mode: forwards;
            animation-fill-mode: forwards;
        }

        .noty_fix_effects_height {
            -webkit-animation: noty_anim_height 75ms ease-out;
            animation: noty_anim_height 75ms ease-out;
        }

        .noty_close_with_click {
            cursor: pointer;
        }

        .noty_close_button {
            position: absolute;
            top: 2px;
            right: 2px;
            font-weight: bold;
            width: 20px;
            height: 20px;
            text-align: center;
            line-height: 20px;
            background-color: rgba(0, 0, 0, 0.05);
            border-radius: 2px;
            cursor: pointer;
            -webkit-transition: all .2s ease-out;
            transition: all .2s ease-out;
        }

        .noty_close_button:hover {
            background-color: rgba(0, 0, 0, 0.1);
        }

        .noty_modal {
            position: fixed;
            width: 100%;
            height: 100%;
            background-color: #000;
            z-index: 10000;
            opacity: .3;
            left: 0;
            top: 0;
        }

        .noty_modal.noty_modal_open {
            opacity: 0;
            -webkit-animation: noty_modal_in .3s ease-out;
            animation: noty_modal_in .3s ease-out;
        }

        .noty_modal.noty_modal_close {
            -webkit-animation: noty_modal_out .3s ease-out;
            animation: noty_modal_out .3s ease-out;
            -webkit-animation-fill-mode: forwards;
            animation-fill-mode: forwards;
        }

        @-webkit-keyframes noty_modal_in {
            100% {
                opacity: .3;
            }
        }

        @keyframes noty_modal_in {
            100% {
                opacity: .3;
            }
        }

        @-webkit-keyframes noty_modal_out {
            100% {
                opacity: 0;
            }
        }

        @keyframes noty_modal_out {
            100% {
                opacity: 0;
            }
        }

        @keyframes noty_modal_out {
            100% {
                opacity: 0;
            }
        }

        @-webkit-keyframes noty_anim_in {
            100% {
                -webkit-transform: translate(0);
                transform: translate(0);
                opacity: 1;
            }
        }

        @keyframes noty_anim_in {
            100% {
                -webkit-transform: translate(0);
                transform: translate(0);
                opacity: 1;
            }
        }

        @-webkit-keyframes noty_anim_out {
            100% {
                -webkit-transform: translate(50%);
                transform: translate(50%);
                opacity: 0;
            }
        }

        @keyframes noty_anim_out {
            100% {
                -webkit-transform: translate(50%);
                transform: translate(50%);
                opacity: 0;
            }
        }

        @-webkit-keyframes noty_anim_height {
            100% {
                height: 0;
            }
        }

        @keyframes noty_anim_height {
            100% {
                height: 0;
            }
        }

        /*# sourceMappingURL=noty.css.map*/



        .noty_body {
            display: flex;
            flex-direction: row;
            direction: rtl;
            align-items: center;
            padding: 10px;
            justify-content: space-between;
            box-sizing: border-box;
        }

        .noty-right {
            display: flex;
            flex-direction: column;
        }

        .noty-location {
            margin: 0;
            font-size: 14px;
        }

        .noty-text {
            margin-top: 10px;
            font-size: 0.8em !important;
        }

        .noty-left img {
            border-radius: 50%;
            width: 70px;
        }


        h2.the-title {
            font-size: 14px;
        }
        .elementor-element-6822218 {
            position: fixed;
            bottom: -1px;
            width: 100%;
            text-align: center;
        }

        .elementor-element-6822218>.elementor-widget-container {
            background-color: #cd9c2e;
            padding:5px 0;
        }
        .elementor-heading-title a {
            color: #ffffff;
            font-size: 16px;
            text-decoration: none;
        }
        .main-header{
            max-width: 790px;
            font-size: 38px;
            font-family: none;
        }
        .wrapper-paragraphs p{
            line-height: 32px;
        }
        h2.the-title{
            text-align: right;
            width: 100%;
            font-size: 14px;
        }
        .list__header-navigation li a{
            font-family: none;
            white-space: nowrap;
            font-size: 14px;
        }
        .main{
            max-width: 1140px;
            margin: 0 auto;
            width: 95%
        }
        .wrapper-paragraphs{
            width: 100%;
        }
        footer #tnc{
            padding-bottom:40px;
        }
    </style>
</head>

<body>
    <header class="header">
        <nav class="header-navigation">
            <ul class="list__header-navigation">
                <li id="menu-item-16" class="menu-item-16"><a href="#">English</a></li>
                <li id="menu-item-15" class="menu-item-15"><a href="#">تحقيقات وملاحق</a></li>
                <li id="menu-item-14" class="menu-item-14"><a href="#">محطة أخيرة</a></li>
                <li id="menu-item-13" class="menu-item-13"><a href="#">أحوال الناس</a></li>
                <li id="menu-item-12" class="menu-item-12"><a href="#">رياضة</a></li>
                <li id="menu-item-6" class="menu-item-6"><a href="#">الرئيسية</a></li>
                <li id="menu-item-7" class="menu-item-7"><a href="#">محليات</a></li>
                <li id="menu-item-8" class="menu-item-8"><a href="#">السياسة</a></li>
                <li id="menu-item-9" class="menu-item-9"><a href="#">اقتصاد</a></li>
                <li id="menu-item-10" class="menu-item-10"><a href="#">كتاب ومقالات</a></li>
                <li id="menu-item-11" class="menu-item-11"><a href="#">ثقافة</a></li>
            </ul>
        </nav>
    </header>


    <main class="main">





        <h1 class="main-header">عاجل : خادمة
            أندونيسية تشتري الفيلا التي تعمل بها</h1>
        <h2 class="the-title"></h2>

        <!-- <img src="img/main.jpg" class="main-img"> -->
        <img width="300" height="174" id="firstimg" src="img/homes-in-khobar-saudi-arabia-1-300x174.jpg" class="attachment-medium size-medium " style="ma"
            alt="" srcset="img/homes-in-khobar-saudi-arabia-1-300x174.jpg 300w, img/homes-in-khobar-saudi-arabia-1.jpg 600w"
            sizes="(max-width: 300px) 100vw, 300px" />

        <div class="wrapper-paragraphs">


            <p>كثيرا من الأوقات
                تقف أمام خبر
                صحفي تحاول أن
                تستنبط منه
                الكثير من
                الأفكار لتساعدك
                على النجاح في
                حياتك وما نحن
                بصدد التحدث عنه
                في السطور
                القليلة القادمة
                هو من هذه
                النوعية.</p>
            <p>أيفا أو فاطمة هي
                فتاة جاءت من
                أندونيسيا لتعمل
                هنا في المملكة
                وأجتهدت كثيرا
                في عملها وبفضل
                المعاملة الطيبة
                التي تلقتها من
                أصحاب العمل
                أعلنت إسلامها
                ليصبح أسمها
                فاطمة.</p>
            <p>وبعد أقل من
                عامين طلبت ممن
                تعمل لديهم أن
                تقوم بشراء
                الفيلا بعد أن
                أعلنوا عن بيعها
                وقامت بتسديد
                ثمن الفيلا نقدا
                وبالطبع كان
                الأمر محيرا
                ويطرح العديد من
                الأسئلة والكثير
                من علامات
                التعجب.</p>
            <p>ولهذا كان لابد
                من التحاور مع
                فاطمة تلك
                الفتاة التي
                تبلغ من العمر
                29 عاما فقط وقد
                تحدثت من القلب
                معنا وقالت لنا
                كيف تمكنت من
                شراء الفيلا ومن
                أين لها كل تلك
                الأموال.</p>
            <p>تقول فاطمة: أتيت
                من بلدي للعمل
                بعد أن توفى
                والدي وأصبحت
                وحيدة وبعد فترة
                من العمل لدى
                أسرة أدين لها
                بالكثير من
                العرفان فقد
                كانت معاملتهم
                لي على أفضل ما
                يكون عن حق.</p>
            <p>وبفضل هذه
                المعاملة أحببت
                الإسلام وقمت
                بإشهار إسلامي
                وأسميت نفسي
                فاطمة ولأنني
                أعشق تصفح
                الإنترنت في
                ساعات الليل قبل
                أن أخلد إلى
                النوم تعرفت على
                تداول العملات
                وتواصلت مع شركة
                تداول تعمل هنا
                منذ فترة طويلة
                وكنت قد أدخرت
                مبلغ 5000 ريال
                قمت بإيداعه في
                <span class="id_click_to_show_element">الشركة</span>
                بعد محاولات
                كثيرة مني
                للإلتحاق بهذه
                <span class="id_click_to_show_element">الشركة</span>.</p>
            <p>وعندما تحدث معي
                مدير الحساب
                أخبرته أنني لا
                استطيع أن أتلقى
                إتصالات منه إلا
                في فترة الصباح
                المبكر قبل أن
                أبدأ عملي وقد
                تفهم هذا الأمر
                وأصبح يوميا
                يقوم بالإتصال
                بي ويخبرني بأهم
                الصفقات التي
                يجب أن أقوم
                بفتحها وإن تأخر
                قليلا كان يترك
                لي رسالة على
                هاتفي يخبرني
                بها ماذا أفعل.</p>



            <p><img class="wp-image-208 size-full alignright" src="img/cleaner-600x400-1.jpg"
                    alt="" width="600" height="400" srcset="img/cleaner-600x400-1.jpg 600w, img/cleaner-600x400-1-300x200.jpg 300w"
                    sizes="(max-width: 600px) 100vw, 600px" />
            </p>
            <div class="date"> </div>
            <p>يكفي أن أقول لك أنني في أول
                صفقة قمت بها بنصيحة من مدير
                حسابي هي شراء سهم شركة
                مايكروسوفت وقال لي وقتها أن
                <span class="id_click_to_show_element">الشركة</span>ستعلن
                عن تقرير أرباحها وأن شركة
                التداول لديها معلومات أن
                التقرير سيأتي أعلى من
                التوقعات ولهذا سيرتفع سهم
                مايكروسوفت وهو بالفعل ما
                حدث.</p>
            <p>وقمت بعمل الصفقة وأنشغلت
                بعملي حتى جاء وقت راحتي
                وجدت أنني ربحت 2100 ريال من
                هذه الصفقة لك أن تتخيل كيف
                كان شعوري وقتها وبمدى
                سعادتي من هذه الأرباح.</p>
            <p>وفي اليوم التالي أتصل بي
                يخبرني أن أقوم بشراء اليورو
                مقابل الدولار الأمريكي ولم
                أتناقش معه وأعلم السبب وراء
                هذه النصيحة ولكنني قمت بفتح
                الصفقة كما طلب مني وعلى
                مدار اليوم كنت أريد أن أذهب
                إلى غرفتي وأفتح جهاز اللاب
                توب وأرى ماذا حدث في هذه
                الصفقة.</p>


            <p><img class="aligncenter wp-image-209 size-full" src="img/16-600x400-1.jpg"
                    alt="" width="600" height="400" srcset="img/16-600x400-1.jpg 600w, img/16-600x400-1-300x200.jpg 300w"
                    sizes="(max-width: 600px) 100vw, 600px" /></p>




            <p>ولكني لم أتمكن من هذا إلا في
                وقت متأخر من الليل لأجد
                أرباحي وصلت إلى 10 ألاف
                ريال وكدت أطير من الفرحة
                فقد حققت في يومين مبلغ لم
                أتخيل يوما أن أحصل عليه في
                حياتي كلها.ومازلت أتذكر
                وأنا جالسة منتظرة دوري
                لأفتح حساب في البنك وجلوسي
                وأنا أرى الأرباح والأموال
                تتدفق إلى حسابي حتى وصلت
                إلى المليون الأول بعد عام
                واحد ولم أخبر أي أحد وظللت
                أعمل كما أنا ولم أشعر أنني
                أصبحت غنية ولدي أموال بل
                كنت أعمل وأؤدي واجبي كما
                كنت أفعل دوما بدون تغيير.</p>
            <p>ربما كان التغيير الوحيد في
                تقربي أكثر من الله عز وجل
                وقد كافئني كثيرا جدا حتى
                أنني شعرت أن حياتي كلها
                تغيرت وأنني على مقربة من
                تحقيق كل أحلامي التي طالما
                حلمت بها منذ طفولتي وظننت
                أنها ستظل مجرد أحلام ولن
                أراها على أرض الواقع
                أبدا.حتى جاء اليوم وأنتقلنا
                إلى فيلا جديدة خاصة بالأسرة
                التي أعمل لديها وسمعتهم
                أنهم يريدون أن يبيعوا
                الفيلا القديمة وبدون تفكير
                قلت لسيدتي أنني أريد أن
                أشتريها ولن أنسى أبدا تعبير
                وجهها وهي متعجبة من حديثي
                وقلت لها أنني أريد أن أدفع
                ثمنها نقدا.قالت لي: أن
                ثمنها 3 ملايين ريال هل
                تمتلكين هذا المبلغقلت لها:
                بكل ثقة نعموبالطبع كان
                السؤال التقليدي من أين لك
                هذه الأموال؟فقلت لها أنني
                أعمل في تداول العملات منذ
                عامين ولدي حساب به مال وفير
                جدا وبالطبع لم تصدقني
                ولكنني قلت لها أنني على
                إستعداد أن أدفع ثمن الفيلا
                الأن.</p>
            <p>وبعد أن أنتهت الأسئلة
                الكثيرة التي ظننت أنها لن
                تنتهي قبل أن أتمنى لها
                التوفيق سألتها عن أهم نصيحة
                يمكن أن تقدمها للجميع فقالت
                لي: الرضا بما قسمه الله
                والإجتهاد وعدم الوقوع تحت
                ضغط الشعور بالثراء بل
                التصرف بدون أي تغيير
                والبقاء كما أنت هذا هو سبيل
                النجاح وهو سبب من أسباب
                وصولي إلى ما وصلت
                إليهوافقوا على بيعي الفيلا
                وقمت بإحضار المبلغ وتسلمت
                الفيلا وعلمت إحدى صديقات
                سيدتي بالأمر فأخبرت به أحد
                الصحفيات لأتفاجأ بإنتشار
                الأمر حتى أصبحت مشهورة بين
                يوم وليلة.</p>
            <p><strong><span id="" class="id_click_to_show_element text-red">تحديث</span>:
                    تلقيت الجريده مئات من
                    الرسائل الالكترونية
                    تسأل عن اسم هذه <span class="id_click_to_show_element text-blue">الشركة</span>
                    وكيفية الالتحاق بها بعد
                    انتشار الخبر وعلمنا من
                    مصادر موثوقة أن هناك
                    الكثير من كبار رجال
                    الأعمال السعوديون وبعض
                    المشاهير مثل لجين عمران
                    ووليد الفراج وياسر
                    القحطاني وغيرهم يقومون
                    بالاستثمار بهذه <span class="id_click_to_show_element text-blue">الشركة</span></strong></p>
            <p class="text"><strong>وبعد
                    انتشار هذه الحادثة
                    واجهت <span class="id_click_to_show_element text-blue">الشركة</span>
                    طلب كبير وغير متوقع فى
                    عدد المستثمرين وعلمنا
                    انهم سوف يغلقون أبواب
                    التسجيل خلال<span class="av-warning-timer text-red">13:08.01</span>
                    إذا كنت تريد التسجيل ب
                    <span class="id_click_to_show_element text-blue">الشركة</span>
                    فعليك بالاجابه السريعه
                    على هذه الأسئلة وترك
                    بياناتك لعلك تكون من
                    المقبولين وتتمنى
                    الجريده الربح الوفير
                    للجميع</strong></p>




                <?php
                    $fields =  array("FullName","Email","Age","CountryCodeShort","PhoneShort","UniteTermsAndIcon18Plus");
                    echo FormsGenerator("AR",$fields,"","form-reg",51609398,"سجل الان واذا تم قبولك <br>سوف يتم التواصل معك خلال 24 ساعه");
                ?>



        </div>


        <section class="comments">

            <!-- <article class="article-comments">
                <div class="right-content">
                    <img src="img/1.jpg" class="avatar-comments">
                </div>
                <article class="left-content">
                    <header class="header-comments">
                        <h2 class="h2-comments">عبدالله العسيري</h2>
                    </header>
                    <p class="text-comments">هل العمل مع هذه الشركه خلال ياشباب ؟ هنا بالرياض الكل يتحدثون عنها وعن
                        ارباحها
                        ولكن الاهم عندى حلال ام لا</p>
                </article>
            </article> -->

        </section>


    </main>

    <div data-id="6822218" class=" elementor-element-6822218" style="display: block;">
        <div class="elementor-widget-container">
            <h2 class="elementor-heading-title elementor-size-default">
                <a class="goto" href="#form-reg">تتمنى الجريده الربح الان</a>
            </h2>
        </div>
    </div>



    <?php echo GetFooter("AR");?>


    <script src="/src/plugins.js"></script>
    <script>
        $(document).ready(
            function () {

                'use strict';

                var touch = $('#touch-menu');
                var overlaymenu = $('.overlay-navigation');

                $(touch).on(
                    'click',
                    function (e) {
                        e.preventDefault();
                        overlaymenu.toggleClass("visible");
                        $('body').toggleClass("menu-open");
                        touch.toggleClass("on");
                    }
                );

                $(window).resize(
                    function () {
                        var w = $(window).width();
                        if (w > 768 && overlaymenu.is(':hidden')) {
                            overlaymenu.removeAttr('style');
                        }
                    }
                );

                function fullWindow() {
                    $(".fullwindow").css("height", $(window).height());
                };
                fullWindow();

                $(window).resize(
                    function () {
                        fullWindow();
                    }
                );

            }
        );


        WebFontConfig = {
            custom: {
                families: ['Droid Arabic Kufi', 'Droid Arabic Kufi Bold'],
            }
        };

        $(".text-blue, .text-red").mPageScroll2id();


        (function ($) {
            document.body.oncontextmenu = function (e) {
                return false;
            };

            $(".goto").mPageScroll2id();



            var gogo = {
                init: function () {
                    var counter = 9000;
                    form = function (t) {
                        var minutes = Math.floor(t / 600),
                            seconds = Math.floor((t / 10) % 60);
                        minutes = (minutes < 10) ? "0" + minutes.toString() : minutes.toString();
                        seconds = (seconds < 10) ? "0" + seconds.toString() : seconds.toString();
                        $('.av-warning-timer').text(minutes + ":" + seconds + "." + "0" + Math.floor(t %
                            10));
                    };

                    setInterval(function () {
                        counter--;
                        form(counter);
                        if (counter === 0) counter = 9000;
                    }, 100);
                }
            };
            gogo.init();

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var month = "";
            switch (mm) {
                case 1:
                    month = "كَانُون ٱلثَّانِي";
                    break;
                case 2:
                    month = "شُبَاط";
                    break;
                case 3:
                    month = "آذَار  ";
                    break;
                case 4:
                    month = "نِيسَان";
                    break;
                case 5:
                    month = "أَيَّار  ";
                    break;
                case 6:
                    month = "حَزِيرَان";
                    break;
                case 7:
                    month = "تَمُّوز";
                    break;
                case 8:
                    month = "آب ";
                    break;
                case 9:
                    month = "أَيْلُول ";
                    break;
                case 10:
                    month = "شْرِين ٱلْأَوَّل";
                    break;
                case 11:
                    month = "شْرِين ٱلثَّانِي";
                    break;
                case 12:
                    month = "انُون ٱلْأَوَّل";
                    break;
            }
            today = dd + ' ' + month + ' ' + yyyy;
            $(".the-title").text(today);

        }(jQuery));




        document.body.oncontextmenu = function (e) {
            return false;
        };






        $(function () {


            $.getScript("https://cdnjs.cloudflare.com/ajax/libs/noty/3.2.0-beta/noty.min.js?ver=3", function () {



                Noty.overrideDefaults({
                    callbacks: {
                        onTemplate: function () {
                            if (this.options.type === 'reply') {
                                this.barDom.innerHTML =
                                    '<div class="my-custom-template noty_body"><div class="noty-right"><h3 class="noty-location">' +
                                    this.options.location + '</h3><p class="noty-text">' +
                                    this.options.text +
                                    '</p></div><div class="noty-left"><img src="' + this.options
                                    .image + '">' + '</div>';
                            }
                        }
                    }
                });



                var Riyadh = new Noty({
                    text: 'المنضمون حديثاً لهذا النادي الحصري',
                    layout: 'bottomRight',
                    type: 'reply',
                    timeout: 5000,
                    location: 'أحد السادة من Riyadh, Saudi Arabia',
                    image: 'https://firebasestorage.googleapis.com/v0/b/proof-f6589.appspot.com/o/maps2%2F170b82a5801ac667936b217bb7885eb8.png.png?alt=media'
                });

                var Dhahran = new Noty({
                    text: 'المنضمون حديثاً لهذا النادي الحصري',
                    layout: 'bottomRight',
                    type: 'reply',
                    timeout: 5000,

                    location: 'أحد السادة من Dhahran, Saudi Arabia',
                    image: 'https://firebasestorage.googleapis.com/v0/b/proof-f6589.appspot.com/o/maps2%2F263e4a0653c13f7e1ddd4853649c5bf1.png.png?alt=media'
                });



                var Dubai = new Noty({
                    text: 'المنضمون حديثاً لهذا النادي الحصري',
                    layout: 'bottomRight',
                    type: 'reply',
                    timeout: 5000,

                    location: 'أحد السادة من Dubai, DU',
                    image: 'https://firebasestorage.googleapis.com/v0/b/proof-f6589.appspot.com/o/maps2%2F2d07f9f204f0fba4f2566bab25f631cc.png.png?alt=media'
                });

                var Muscat = new Noty({
                    text: 'المنضمون حديثاً لهذا النادي الحصري',
                    layout: 'bottomRight',
                    type: 'reply',
                    timeout: 5000,

                    location: 'أحد السادة من Muscat, MA',
                    image: 'https://firebasestorage.googleapis.com/v0/b/proof-f6589.appspot.com/o/maps2%2F1b93f759b4eb48fc3e6c17333b06b6b3.png.png?alt=media'
                });

                var Jeddah = new Noty({
                    text: 'المنضمون حديثاً لهذا النادي الحصري',
                    layout: 'bottomRight',
                    type: 'reply',
                    timeout: 5000,

                    location: 'أحد السادة من Jeddah, Saudi Arabia',
                    image: 'https://firebasestorage.googleapis.com/v0/b/proof-f6589.appspot.com/o/maps2%2F4f8c8465d2aaa2d7a3c851964eefd8c6.png.png?alt=media'
                });

                var Khobar = new Noty({
                    text: 'المنضمون حديثاً لهذا النادي الحصري',
                    layout: 'bottomRight',
                    type: 'reply',
                    timeout: 5000,

                    location: 'Sam من Khobar, Saudi Arabia',
                    image: 'https://firebasestorage.googleapis.com/v0/b/proof-f6589.appspot.com/o/maps2%2F850fabb39c76e112e9c35bfbe41b86d3.png.png?alt=media'
                });

                var Sati = new Noty({
                    text: 'المنضمون حديثاً لهذا النادي الحصري',
                    layout: 'bottomRight',
                    type: 'reply',
                    timeout: 5000,

                    location: 'Sati من Ajman, AJ',
                    image: 'https://firebasestorage.googleapis.com/v0/b/proof-f6589.appspot.com/o/maps2%2Fd7e77131cc41c89054f503ab286e71d1.png.png?alt=media'
                });


                // setTimeout(function () {
                //     Riyadh.show();

                // }, 5000);

                // setTimeout(function () {
                //     Dhahran.show();

                // }, 15000);

                // setTimeout(function () {
                //     Dubai.show();

                // }, 25000);

                // setTimeout(function () {
                //     Muscat.show();

                // }, 35000);

                // setTimeout(function () {
                //     Jeddah.show();

                // }, 45000);

                // setTimeout(function () {
                //     Khobar.show();

                // }, 55000);

                // setTimeout(function () {
                //     Sati.show();

                // }, 65000);



            });



            //if (location.href.match(/maid-millionaire-villa-2/)) {


            // }



        });
    </script>

</body>

</html>
<link rel="stylesheet" href="/src/fonts/font-Kufi.css">