/*jQuery*/
//= plugins/jquery-3.3.1.min.js
//= plugins/bootstrap/bootstrap.min.js
//= plugins/swiper.min.js
// plugins/webfontloader.min.js

function addScript(src) {
	var script = document.createElement('script');
	script.src = src;
	script.type = 'text/javascript';
	script.async = false;
	document.head.appendChild(script);
}



$(document).ready(function () {
	// WebFont.load({
	// 	google: {
	// 		families: ['Open Sans']
	// 	},
	// 	custom: {
	// 		families: ['My Font', 'My Other Font:n4,i4,n7'],
	// 		urls: ['/fonts.css']
	// 	}
	// });


	$('.main-header-block .hamburger').on('click',function () {
		//alert('1');
		console.log('menu!')
		$(this).toggleClass('is-active');
		$('.menu').toggleClass('active');
		$('#main').toggleClass('active-menu');
		$('.img-flag').toggleClass('active');
	})

	var swiper = new Swiper('.swiper-container', {
		slidesPerView: 1,
		spaceBetween: 30,
		centeredSlides: true,
		slidesPerGroup: 1,
		loop: true,
		loopFillGroupWithBlank: true,
		pagination: {
			el: '.swiper-pagination',
			clickable: true,
		},
	});

	$('.drag-file input').change(function () {
		//$('.drag-file-text p').text(this.files.length + " file(s) selected");
		$('.drag-file-text p').text("File selected");
	});

	if( window.screen.width <= 991 ){
		$('#descktop-video').remove()
	}else{
		$('#mobile-video').remove()
	}



	//addScript("https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU");


});